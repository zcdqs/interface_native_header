/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OH_NATIVE_DISPLAY_MANAGER_H
#define OH_NATIVE_DISPLAY_MANAGER_H

/**
 * @addtogroup OH_DisplayManager
 * @{
 *
 * @brief Provides the display management capability.
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */

/**
 * @file oh_display_manager.h
 *
 * @brief Declares the functions for basic display management.
 * You can call the functions to obtain various information about the default display and listen for display status
 * changes, such as rotation, folding, and unfolding.
 *
 * @kit ArkUI
 * File to include: <window_manager/oh_display_manager.h>
 * @library libnative_display_manager.so.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */

#include "oh_display_info.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Obtains the ID of the default display.
 *
 * @param displayId Pointer to the display ID. The value is a non-negative integer.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_GetDefaultDisplayId(uint64_t *displayId);

/**
 * @brief Obtains the width of the default display.
 *
 * @param displayWidth Pointer to the width, in px. The value is an integer.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_GetDefaultDisplayWidth(int32_t *displayWidth);

/**
 * @brief Obtains the height of the default display.
 *
 * @param displayHeight Pointer to the height, in px. The value is an integer.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_GetDefaultDisplayHeight(int32_t *displayHeight);

/**
 * @brief Obtains the clockwise rotation angle of the default display.
 *
 * @param displayRotation Pointer to the clockwise rotation angle.
 * For details about the available options, see {@link NativeDisplayManager_Rotation}.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_GetDefaultDisplayRotation(
    NativeDisplayManager_Rotation *displayRotation);

/**
 * @brief Obtains the orientation of the default display.
 *
 * @param displayOrientation Pointer to the orientation.
 * For details about the available options, see {@link NativeDisplayManager_Orientation}.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_GetDefaultDisplayOrientation(
    NativeDisplayManager_Orientation *displayOrientation);

/**
 * @brief Obtains the virtual pixel ratio of the default display.
 *
 * @param virtualPixels Pointer to the virtual pixel ratio. The value is a floating-point number,
 * and it is usually the same as that of <b>densityPixels</b>.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_GetDefaultDisplayVirtualPixelRatio(float *virtualPixels);

/**
 * @brief Obtains the refresh rate of the default display.
 *
 * @param refreshRate Pointer to the refresh rate. The value is an integer, in Hz.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_GetDefaultDisplayRefreshRate(uint32_t *refreshRate);

/**
 * @brief Obtains the physical pixel density of the default display.
 *
 * @param densityDpi Pointer to the physical pixel density, that is, the number of pixels per inch.
 * The value is a floating point number, in px. The actual value depends on the options provided in device settings.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_GetDefaultDisplayDensityDpi(int32_t *densityDpi);

/**
 * @brief Obtains the logical pixel density of the default display.
 *
 * @param densityPixels Pointer to the logical pixel density, which indicates the scaling coefficient of the
 * physical pixels and logical pixels. The value is a floating point number in the range [0.5, 4.0]. Generally, the
 * value is <b>1.0</b> or <b>3.0</b>. The actual value depends on the density DPI provided by the device in use.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_GetDefaultDisplayDensityPixels(float *densityPixels);

/**
 * @brief Obtains the scale factor of the font displayed on the default display.
 *
 * @param scaledDensity Pointer to the scale factor. The value is a floating-point number,
 * and it is usually the same as that of <b>densityPixels</b>.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_GetDefaultDisplayScaledDensity(float *scaledDensity);

/**
 * @brief Obtains the number of physical pixels that are displayed horizontally per inch on the default display.
 *
 * @param xDpi Pointer to the number of physical pixels displayed horizontally per inch.
 * The value is a floating point number.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_GetDefaultDisplayDensityXdpi(float *xDpi);

/**
 * @brief Obtains the number of physical pixels that are displayed vertically per inch on the default display.
 *
 * @param yDpi Pointer to the number of physical pixels displayed vertically per inch.
 * The value is a floating point number.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_GetDefaultDisplayDensityYdpi(float *yDpi);

/**
 * @brief Obtains the cutout information of the default display.
 *
 * @param cutoutInfo Double pointer to the cutout information. For details, see {@link NativeDisplayManager_CutoutInfo}.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_CreateDefaultDisplayCutoutInfo(
    NativeDisplayManager_CutoutInfo **cutoutInfo);

/**
 * @brief Destroys the cutout information of the default display.
 *
 * @param cutoutInfo Pointer to the cutout information object obtained by calling
 * {@link OH_NativeDisplayManager_CreateDefaultDisplayCutoutInfo}.
 * For details, see {@link NativeDisplayManager_CutoutInfo}.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_DestroyDefaultDisplayCutoutInfo(
    NativeDisplayManager_CutoutInfo *cutoutInfo);

/**
 * @brief Checks whether the device is foldable.
 *
 * @return Returns the check result. The value <b>true</b> means that the device is foldable,
 * and <b>false</b> means the opposite.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 */
bool OH_NativeDisplayManager_IsFoldable();

/**
 * @brief Obtains the display mode of the foldable device.
 *
 * @param displayMode Pointer to the display mode.
 * For details about the available options, see {@link NativeDisplayManager_FoldDisplayMode}.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.Window.SessionManager
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_GetFoldDisplayMode(
    NativeDisplayManager_FoldDisplayMode *displayMode);

/**
 * @brief Defines a callback function used to listen for status changes of a display.
 *
 * @param displayId ID of the display.
 * @syscap SystemCapability.Window.SessionManager
 * @since 12
 */
typedef void (*OH_NativeDisplayManager_DisplayChangeCallback)(uint64_t displayId);

/**
 * @brief Registers a listener for status changes (such as rotation, refresh rate, DPI, and resolution changes)
 * of the display.
 *
 * @param displayChangeCallback Callback function triggered when the display status is changed.
 * For details, see {@link OH_NativeDisplayManager_DisplayChangeCallback}.
 * @param listenerIndex Pointer to the index of the listener registered. It is used as an input parameter of
 * {@link OH_NativeDisplayManager_UnregisterDisplayChangeListener}.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_RegisterDisplayChangeListener(
    OH_NativeDisplayManager_DisplayChangeCallback displayChangeCallback, uint32_t *listenerIndex);

/**
 * @brief Cancels the listening for status changes of the display.
 *
 * @param listenerIndex Index of the listener returned when
 * {@link OH_NativeDisplayManager_RegisterDisplayChangeListener} is called.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_UnregisterDisplayChangeListener(uint32_t listenerIndex);

/**
 * @brief Defines a callback function used to listen for folded/unfolded state changes of a display.
 *
 * @param displayMode Folded or unfolded state of the display.
 * For details about the available options, see {@link NativeDisplayManager_FoldDisplayMode}.
 * @syscap SystemCapability.Window.SessionManager
 * @since 12
 */
typedef void (*OH_NativeDisplayManager_FoldDisplayModeChangeCallback)(
    NativeDisplayManager_FoldDisplayMode displayMode);

/**
 * @brief Registers a listener for folded/unfolded state changes of the display.
 *
 * @param displayModeChangeCallback Callback function triggered when the folded/unfolded state of the display
 * is changed. For details, see {@link OH_NativeDisplayManager_FoldDisplayModeChangeCallback}.
 * @param listenerIndex Pointer to the index of the listener registered. It is used as an input parameter of
 * {@link OH_NativeDisplayManager_UnregisterFoldDisplayModeChangeListener}.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.Window.SessionManager
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_RegisterFoldDisplayModeChangeListener(
    OH_NativeDisplayManager_FoldDisplayModeChangeCallback displayModeChangeCallback, uint32_t *listenerIndex);

/**
 * @brief Cancels the listening for folded/unfolded state changes of the display.
 *
 * @param listenerIndex Index of the listener returned when
 * {@link OH_NativeDisplayManager_RegisterFoldDisplayModeChangeListener} is called.
 * @return Returns a status code defined in {@link NativeDisplayManager_ErrorCode}.
 * @syscap SystemCapability.Window.SessionManager
 * @since 12
 */
NativeDisplayManager_ErrorCode OH_NativeDisplayManager_UnregisterFoldDisplayModeChangeListener(uint32_t listenerIndex);

#ifdef __cplusplus
}
#endif
/** @} */
#endif // OH_NATIVE_DISPLAY_MANAGER_H
