/*
 * Copyright (c) 2021-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_PEN_H
#define C_INCLUDE_DRAWING_PEN_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_pen.h
 *
 * @brief Declares the functions related to the pen the drawing module.
 *
 * File to include: native_drawing/drawing_pen.h
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates an <b>OH_Drawing_Pen</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_Pen</b> object created.
 * @since 8
 * @version 1.0
 */
OH_Drawing_Pen* OH_Drawing_PenCreate(void);

/**
 * @brief Copies an existing {@link OH_Drawing_Pen} object to create a new one.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @return Returns the pointer to the {@link OH_Drawing_Pen} object created. If NULL is returned, the creation fails.
 *         The possible failure cause is that no memory is available or <b>pen</b> is NULL.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Pen* OH_Drawing_PenCopy(OH_Drawing_Pen* pen);

/**
 * @brief Destroys an <b>OH_Drawing_Pen</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenDestroy(OH_Drawing_Pen*);

/**
 * @brief Checks whether anti-aliasing is enabled for a pen.
 * Anti-aliasing makes the pixels around the shape edges semi-transparent.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @return Returns <b>true</b> if anti-aliasing is enabled; returns <b>false</b> otherwise.
 * @since 8
 * @version 1.0
 */
bool OH_Drawing_PenIsAntiAlias(const OH_Drawing_Pen*);

/**
 * @brief Enables or disables anti-aliasing for a pen.
 * Anti-aliasing makes the pixels around the shape edges semi-transparent.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @param bool Whether to enable anti-aliasing. The value <b>true</b> means to enable anti-aliasing,
 * and <b>false</b> means the opposite.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetAntiAlias(OH_Drawing_Pen*, bool);

/**
 * @brief Obtains the color of a pen. The color is used by the pen to outline a shape.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @return Returns a 32-bit (ARGB) variable that describes the color.
 * @since 8
 * @version 1.0
 */
uint32_t OH_Drawing_PenGetColor(const OH_Drawing_Pen*);

/**
 * @brief Sets the color for a pen. The color is used by the pen to outline a shape.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @param color Color, which is a 32-bit (ARGB) variable.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetColor(OH_Drawing_Pen*, uint32_t color);

/**
 * @brief Obtains the alpha value of a pen. This value is used by the alpha channel when the pen outlines a shape.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @return Returns an 8-bit variable that describes the alpha value.
 * @since 11
 * @version 1.0
 */
uint8_t OH_Drawing_PenGetAlpha(const OH_Drawing_Pen*);

/**

 * @brief Sets the alpha value for a pen. This value is used by the alpha channel when the pen outlines a shape.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @param alpha Alpha value, which is an 8-bit variable.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_PenSetAlpha(OH_Drawing_Pen*, uint8_t alpha);

/**
 * @brief Obtains the thickness of a pen. This thickness determines the width of the outline of a shape.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @return Returns the thickness.
 * @since 8
 * @version 1.0
 */
float OH_Drawing_PenGetWidth(const OH_Drawing_Pen*);

/**
 * @brief Sets the thickness for a pen. This thickness determines the width of the outline of a shape.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @param width Thickness, which is a variable.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetWidth(OH_Drawing_Pen*, float width);

/**
 * @brief Obtains the stroke miter limit of a polyline drawn by a pen. When the corner type is bevel, a beveled corner
 * is displayed if the miter limit is exceeded, and a mitered corner is displayed if the miter limit is not exceeded.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @return Returns the miter limit.
 * @since 8
 * @version 1.0
 */
float OH_Drawing_PenGetMiterLimit(const OH_Drawing_Pen*);

/**
 * @brief Sets the stroke miter limit for a polyline drawn by a pen. When the corner type is bevel, a beveled corner
 * is displayed if the miter limit is exceeded, and a mitered corner is displayed if the miter limit is not exceeded.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @param miter Stroke miter limit, which is a variable.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetMiterLimit(OH_Drawing_Pen*, float miter);

/**
 * @brief Enumerates the line cap styles of a pen. The line cap style defines the style of both ends of a line segment
 * drawn by the pen.
 *
 * @since 8
 * @version 1.0
 */
typedef enum OH_Drawing_PenLineCapStyle {
    /** There is no cap style. Both ends of the line segment are cut off square. */
    LINE_FLAT_CAP,
    /**
     * Square cap style. Both ends have a square, the height of which is half of the width of the line segment,
     * with the same width.
     */
    LINE_SQUARE_CAP,
    /**
     * Round cap style. Both ends have a semicircle centered, the diameter of which is the same as the width of
     * the line segment.
     */
    LINE_ROUND_CAP
} OH_Drawing_PenLineCapStyle;

/**
 * @brief Obtains the line cap style of a pen.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @return Returns the line cap style.
 * @since 8
 * @version 1.0
 */
OH_Drawing_PenLineCapStyle OH_Drawing_PenGetCap(const OH_Drawing_Pen*);

/**
 * @brief Sets the line cap style for a pen.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>OH_Drawing_PenLineCapStyle</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @param OH_Drawing_PenLineCapStyle Line cap style, which is a variable.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetCap(OH_Drawing_Pen*, OH_Drawing_PenLineCapStyle);

/**
 * @brief Enumerates the line join styles of a pen. The line join style defines the shape of the joints of a polyline
 * segment drawn by the pen.
 *
 * @since 8
 * @version 1.0
 */
typedef enum OH_Drawing_PenLineJoinStyle {
    /**
     * Mitered corner. If the angle of a polyline is small, its miter length may be inappropriate. In this case,
     * you need to use the miter limit to limit the miter length.
     */
    LINE_MITER_JOIN,
    /** Round corner. */
    LINE_ROUND_JOIN,
    /** Beveled corner. */
    LINE_BEVEL_JOIN
} OH_Drawing_PenLineJoinStyle;

/**
 * @brief Obtains the line join style of a pen.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @return Returns the line join style.
 * @since 8
 * @version 1.0
 */
OH_Drawing_PenLineJoinStyle OH_Drawing_PenGetJoin(const OH_Drawing_Pen*);

/**
 * @brief Sets the line join style for a pen.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>OH_Drawing_PenLineJoinStyle</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @param OH_Drawing_PenLineJoinStyle Line join style.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetJoin(OH_Drawing_Pen*, OH_Drawing_PenLineJoinStyle);

/**
 * @brief Sets the shader effect for a pen.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an {@link OH_Drawing_Pen} object.
 * @param OH_Drawing_ShaderEffect Pointer to an {@link OH_Drawing_ShaderEffect} object.
 * If NULL is passed in, the shader effect will be cleared.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_PenSetShaderEffect(OH_Drawing_Pen*, OH_Drawing_ShaderEffect*);

/**
 * @brief Sets the shadow layer for a pen. The shadow layer effect takes effect only when text is drawn.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an {@link OH_Drawing_Pen} object.
 * @param OH_Drawing_ShadowLayer Pointer to an {@link OH_Drawing_ShadowLayer} object.
 * If NULL is passed in, the shadow layer effect will be cleared.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PenSetShadowLayer(OH_Drawing_Pen*, OH_Drawing_ShadowLayer*);

/**
 * @brief Sets the path effect for a pen.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an {@link OH_Drawing_Pen} object.
 * @param OH_Drawing_PathEffect Pointer to an {@link OH_Drawing_PathEffect} object.
 * If NULL is passed in, the path effect will be cleared.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PenSetPathEffect(OH_Drawing_Pen*, OH_Drawing_PathEffect*);

/**
 * @brief Sets a filter for a pen.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an {@link OH_Drawing_Pen} object.
 * @param OH_Drawing_Filter Pointer to an {@link OH_Drawing_Filter} object.
 * If NULL is passed in, the filter will be cleared.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_PenSetFilter(OH_Drawing_Pen*, OH_Drawing_Filter*);

/**
 * @brief Obtains the filter from a pen. The filter is a container that holds a mask filter and color filter.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Pen</b> or <b>OH_Drawing_Filter</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an {@link OH_Drawing_Pen} object.
 * @param OH_Drawing_Filter Pointer to the {@link OH_Drawing_Filter} object obtained.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PenGetFilter(OH_Drawing_Pen*, OH_Drawing_Filter*);

/**
 * @brief Sets a blender for a pen. The blender implements the specified blend mode.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>OH_Drawing_BlendMode</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an {@link OH_Drawing_Pen} object.
 * @param OH_Drawing_BlendMode Blend mode. For details about the available options, see {@link OH_Drawing_BlendMode}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PenSetBlendMode(OH_Drawing_Pen*, OH_Drawing_BlendMode);

/**
 * @brief Obtains the source path outline drawn using a pen and represents it using a destination path.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If any of <b>OH_Drawing_Pen</b>, <b>src</b>, and <b>dst</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an {@link OH_Drawing_Pen} object.
 * @param src Pointer to a source path, which is an {@link OH_Drawing_Path} object.
 * @param dst Pointer to a destination path, which is an {@link OH_Drawing_Path} object.
 * @param OH_Drawing_Rect Pointer to an {@link OH_Drawing_Rect} object. NULL is recommended.
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object. NULL is recommended.
 * The default value is an identity matrix.
 * @return Returns <b>true</b> if the destination path is obtained; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_PenGetFillPath(OH_Drawing_Pen*, const OH_Drawing_Path* src, OH_Drawing_Path* dst,
    const OH_Drawing_Rect*, const OH_Drawing_Matrix*);

/**
 * @brief Resets a pen to the initial state.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Pen</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen Pointer to an {@link OH_Drawing_Pen} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PenReset(OH_Drawing_Pen*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
