/*
 * Copyright (c) 2023-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_TYPEFACE_H
#define C_INCLUDE_DRAWING_TYPEFACE_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_typeface.h
 *
 * @brief Declares the functions related to the typeface in the drawing module.
 * Different platforms have their own default typefaces. You can also parse the .ttf file to obtain the typefaces
 * specified by the third party, such as SimSun and SimHei.
 *
 * File to include: native_drawing/drawing_typeface.h
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_error_code.h"
#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates an <b>OH_Drawing_Typeface</b> object, which defines the default font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_Typeface</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_Typeface* OH_Drawing_TypefaceCreateDefault(void);

/**
 * @brief Creates an <b>OH_Drawing_Typeface</b> object through a file.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>path</b> is NULL, {@link OH_DRAWING_ERROR_INVALID_PARAMETER} is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path Pointer to the file path.
 * @param index File index.
 * @return Returns the pointer to the {@link OH_Drawing_Typeface} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Typeface* OH_Drawing_TypefaceCreateFromFile(const char* path, int index);

/**
 * @brief Creates an <b>OH_Drawing_Typeface</b> object with font arguments through a file.
 * If the <b>OH_Drawing_Typeface</b> object does not support the variation described in the font arguments,
 * this function creates an <b>OH_Drawing_Typeface</b> object with the default font arguments.
 * In this case, this function provides the same capability as {@link OH_Drawing_TypefaceCreateFromFile}.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path Pointer to the file path.
 * @param fontArguments Pointer to an {@link OH_Drawing_FontArguments} object.
 * @return Returns the pointer to the {@link OH_Drawing_Typeface} object created.
 * If a null pointer is returned, the creation fails. Possible causes are that no memory is available,
 * the passed-in <b>path</b> or <b>fontArguments</b> is NULL, or the path is invalid.
 * @since 13
 * @version 1.0
 */
OH_Drawing_Typeface* OH_Drawing_TypefaceCreateFromFileWithArguments(const char* path,
    const OH_Drawing_FontArguments* fontArguments);

/**
 * @brief Creates an <b>OH_Drawing_Typeface</b> object with font arguments based on an existing
 * <b>OH_Drawing_Typeface</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param current Pointer to an existing {@link OH_Drawing_Typeface} object.
 * @param fontArguments Pointer to an {@link OH_Drawing_FontArguments} object.
 * @return Returns the pointer to the {@link OH_Drawing_Typeface} object created.
 * If a null pointer is returned, the creation fails. Possible causes are that no memory is available,
 * the passed-in <b>path</b> or <b>fontArguments</b> is null, or the existing <b>OH_Drawing_FontArguments</b> object
 * does not support the variation described in the font arguments.
 * @since 13
 * @version 1.0
 */
OH_Drawing_Typeface* OH_Drawing_TypefaceCreateFromCurrent(const OH_Drawing_Typeface* current,
    const OH_Drawing_FontArguments* fontArguments);

/**
 * @brief Creates an <b>OH_Drawing_Typeface</b> object through a memory stream.
 * If the memory stream is an invalid font file, a null pointer is returned.
 * After the memory stream is passed in, the ownership is transferred and you cannot release it.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_MemoryStream</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_MemoryStream Pointer to an {@link OH_Drawing_MemoryStream} object.
 * @param index Index of the memory stream.
 * @return Returns the pointer to the {@link OH_Drawing_Typeface} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Typeface* OH_Drawing_TypefaceCreateFromStream(OH_Drawing_MemoryStream*, int32_t index);

/**
 * @brief Destroys an <b>OH_Drawing_Typeface</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typeface Pointer to an <b>OH_Drawing_Typeface</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_TypefaceDestroy(OH_Drawing_Typeface*);

/**
 * @brief Creates an <b>OH_Drawing_FontArguments</b> object.
 * The font arguments are used to create an <b>OH_Drawing_Typeface</b> object with custom attributes.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_FontArguments</b> object created.
 * @since 13
 * @version 1.0
 */
OH_Drawing_FontArguments* OH_Drawing_FontArgumentsCreate(void);

/**
 * @brief Adds a variation to an <b>OH_Drawing_FontArguments</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param fontArguments Pointer to an {@link OH_Drawing_FontArguments} object.
 * @param axis Pointer to the label of the variation. The value must contain four ASCII characters.
 * The supported labels depend on the loaded font file. For example, <b>'wght'</b> is the font weight label.
 * @param value Value of the variation label.
 * @return Returns either of the following result codes:
 *         <b>OH_DRAWING_SUCCESS</b> if the operation is successful.
 *         <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if either <b>fontArguments</b> or <b>axis</b> is NULL
 *         or the length of <b>axis</b> is not 4.
 * @since 13
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_FontArgumentsAddVariation(OH_Drawing_FontArguments* fontArguments,
    const char* axis, float value);

/**
 * @brief Destroys an <b>OH_Drawing_FontArguments</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param fontArguments Pointer to an {@link OH_Drawing_FontArguments} object.
 * @return Returns either of the following result codes:
 *         <b>OH_DRAWING_SUCCESS</b> if the operation is successful.
 *         <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if <b>fontArguments</b> is NULL.
 * @since 13
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_FontArgumentsDestroy(OH_Drawing_FontArguments* fontArguments);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
