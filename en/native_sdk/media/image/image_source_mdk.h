/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup image
 * @{
 *
 * @brief Provides native APIs for image sources.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */

/**
 * @file image_source_mdk.h
 *
 * @brief Declares APIs for decoding an image source into a pixel map.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */

#ifndef INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_SOURCE_MDK_H_
#define INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_SOURCE_MDK_H_
#include <cstdint>
#include "napi/native_api.h"
#include "image_mdk_common.h"
namespace OHOS {
namespace Media {
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines a native image source object for the image source APIs.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct ImageSourceNative_;

/**
 * @brief Defines a native image source object for the image source APIs.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
typedef struct ImageSourceNative_ ImageSourceNative;

/**
 * @brief Defines a pointer to bits per sample, one of the image properties.
 * It is used in {@link OH_ImageSource_GetImageProperty} and {@link OH_ImageSource_ModifyImageProperty}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_BITS_PER_SAMPLE = "BitsPerSample";

/**
 * @brief Defines a pointer to the orientation, one of the image properties.
 * It is used in {@link OH_ImageSource_GetImageProperty} and {@link OH_ImageSource_ModifyImageProperty}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_ORIENTATION = "Orientation";

/**
 * @brief Defines a pointer to the image length, one of the image properties.
 * It is used in {@link OH_ImageSource_GetImageProperty} and {@link OH_ImageSource_ModifyImageProperty}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_IMAGE_LENGTH = "ImageLength";

/**
 * @brief Defines a pointer to the image width, one of the image properties.
 * It is used in {@link OH_ImageSource_GetImageProperty} and {@link OH_ImageSource_ModifyImageProperty}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_IMAGE_WIDTH = "ImageWidth";

/**
 * @brief Defines a pointer to the GPS latitude, one of the image properties.
 * It is used in {@link OH_ImageSource_GetImageProperty} and {@link OH_ImageSource_ModifyImageProperty}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_GPS_LATITUDE = "GPSLatitude";

/**
 * @brief Defines a pointer to the GPS longitude, one of the image properties.
 * It is used in {@link OH_ImageSource_GetImageProperty} and {@link OH_ImageSource_ModifyImageProperty}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_GPS_LONGITUDE = "GPSLongitude";

/**
 * @brief Defines a pointer to the GPS latitude reference information, one of the image properties.
 * It is used in {@link OH_ImageSource_GetImageProperty} and {@link OH_ImageSource_ModifyImageProperty}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_GPS_LATITUDE_REF = "GPSLatitudeRef";

/**
 * @brief Defines a pointer to the GPS longitude reference information, one of the image properties.
 * It is used in {@link OH_ImageSource_GetImageProperty} and {@link OH_ImageSource_ModifyImageProperty}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_GPS_LONGITUDE_REF = "GPSLongitudeRef";

/**
 * @brief Defines a pointer to the created date and time, one of the image properties.
 * It is used in {@link OH_ImageSource_GetImageProperty} and {@link OH_ImageSource_ModifyImageProperty}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_DATE_TIME_ORIGINAL = "DateTimeOriginal";

/**
 * @brief Defines a pointer to the exposure time, one of the image properties.
 * It is used in {@link OH_ImageSource_GetImageProperty} and {@link OH_ImageSource_ModifyImageProperty}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_EXPOSURE_TIME = "ExposureTime";

/**
 * @brief Defines a pointer to the scene type, one of the image properties.
 * It is used in {@link OH_ImageSource_GetImageProperty} and {@link OH_ImageSource_ModifyImageProperty}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_SCENE_TYPE = "SceneType";

/**
 * @brief Defines a pointer to the ISO speed ratings, one of the image properties.
 * It is used in {@link OH_ImageSource_GetImageProperty} and {@link OH_ImageSource_ModifyImageProperty}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_ISO_SPEED_RATINGS = "ISOSpeedRatings";

/**
 * @brief Defines a pointer to the f-number of the image, one of the image properties.
 * It is used in {@link OH_ImageSource_GetImageProperty} and {@link OH_ImageSource_ModifyImageProperty}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_F_NUMBER = "FNumber";

/**
 * @brief Defines a pointer to the compressed bits per pixel, one of the image properties.
 * It is used in {@link OH_ImageSource_GetImageProperty} and {@link OH_ImageSource_ModifyImageProperty}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_COMPRESSED_BITS_PER_PIXEL = "CompressedBitsPerPixel";

/**
 * @brief Defines the region of the image source to decode.
 * It is used in {@link OhosImageDecodingOps}, {@link OH_ImageSource_CreatePixelMap}, and
 * {@link OH_ImageSource_CreatePixelMapList}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageRegion {
    /** X coordinate of the start point, in pixels. */
    int32_t x;
    /** Y coordinate of the start point, in pixels. */
    int32_t y;
    /** Width of the region, in pixels. */
    int32_t width;
    /** Height of the region, in pixels. */
    int32_t height;
};

/**
 * @brief Defines the image source options.
 * It is used in {@link OH_ImageSource_Create} and {@link OH_ImageSource_CreateIncremental}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageSourceOps {
    /** Pixel density of the image source. */
    int32_t density;
    /** Pixel format of the image source. It is usually used to describe the YUV buffer. */
    int32_t pixelFormat;
    /** Pixel width and height of the image source. */
    struct OhosImageSize size;
};

/**
 * @brief Defines the options for decoding the image source.
 * It is used in {@link OH_ImageSource_CreatePixelMap} and {@link OH_ImageSource_CreatePixelMapList}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageDecodingOps {
    /** Whether the output pixel map is editable. */
    int8_t editable;
    /** Pixel format of the output pixel map. */
    int32_t pixelFormat;
    /** Pixel density of the output pixel map. */
    int32_t fitDensity;
    /** Index of the output pixel map. */
    uint32_t index;
    /** Size of the sample. */
    uint32_t sampleSize;
    /** Decoding rotation. */
    uint32_t rotate;
    /** Pixel width and height of the output pixel map. */
    struct OhosImageSize size;
    /** Region of the output pixel map. */
    struct OhosImageRegion region;
};

/**
 * @brief Defines the image source information, which is obtained by calling {@link OH_ImageSource_GetImageInfo}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageSourceInfo {
    /** Pixel format of the image source. It is set in {@link OH_ImageSource_Create}. */
    int32_t pixelFormat;
    /** Color space of the image source. */
    int32_t colorSpace;
    /** Alpha type of the image source. */
    int32_t alphaType;
    /** Image density of the image source. It is set in {@link OH_ImageSource_Create}. */
    int32_t density;
    /** Pixel width and height of the image source. */
    struct OhosImageSize size;
};

/**
 * @brief Defines the input resource of the image source. It is obtained by calling {@link OH_ImageSource_Create}.
 * Only one type of resource is accepted at a time.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageSource {
    /** Pointer to the image source URI. Only a file URI or Base64 URI is accepted. */
    char* uri = nullptr;
    /** Length of the image source URI. */
    size_t uriSize = 0;
    /** Descriptor of the image source. */
    int32_t fd = -1;
    /** Pointer to the image source buffer. Only a formatted packet buffer or Base64 buffer is accepted. */
    uint8_t* buffer = nullptr;
    /** Size of the image source buffer. */
    size_t bufferSize = 0;
};

/**
 * @brief Defines the delay time list of the image source. It is obtained by calling
 * {@link OH_ImageSource_GetDelayTime}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageSourceDelayTimeList {
    /** Pointer to the head of the image source delay time list. */
    int32_t* delayTimeList;
    /** Size of the image source delay time list. */
    size_t size = 0;
};

/**
 * @brief Defines the format string supported by the image source.
 * It is used in {@link OhosImageSourceSupportedFormatList} and {@link OH_ImageSource_GetSupportedFormats}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageSourceSupportedFormat {
    /** Pointer to the head of the format string. */
    char* format = nullptr;
    /** Size of the format string. */
    size_t size = 0;
};

/**
 * @brief Defines the format string list supported by the image source.
 * It is obtained by calling {@link OH_ImageSource_GetSupportedFormats}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageSourceSupportedFormatList {
    /** Double pointer to the head of the format string list. */
    struct OhosImageSourceSupportedFormat** supportedFormatList = nullptr;
    /** Size of the format string list. */
    size_t size = 0;
};

/**
 * @brief Defines the property string (in key-value format) of the image source.
 * It is used in {@link OH_ImageSource_GetImageProperty} and {@link OH_ImageSource_ModifyImageProperty}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageSourceProperty {
    /** Pointer to the head of the property string. */
    char* value = nullptr;
    /** Size of the property string. */
    size_t size = 0;
};

/**
 * @brief Defines the update data of the image source. It is obtained by calling {@link OH_ImageSource_UpdateData}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageSourceUpdateData {
    /** Pointer to the buffer for storing the update data. */
    uint8_t* buffer = nullptr;
    /** Size of the buffer. */
    size_t bufferSize = 0;
    /** Offset of the update data in the buffer. */
    uint32_t offset = 0;
    /** Length of the update data in the buffer. */
    uint32_t updateLength = 0;
    /** Whether the image source data update is completed. */
    int8_t isCompleted = 0;
};

/**
 * @brief Creates an <b>ImageSource</b> object at the JavaScript native layer based on the specified
 * {@link OhosImageSource} and {@link OhosImageSourceOps} structs.
 *
 * @param env Indicates a pointer to the Java Native Interface (JNI) environment.
 * @param src Indicates a pointer to the input resource of the image source. For details, see {@link OhosImageSource}.
 * @param ops Indicates a pointer to the options for creating the image source.
 * For details, see {@link OhosImageSourceOps}.
 * @param res Indicates a pointer to the <b>ImageSource</b> object created at the JavaScript native layer.
 * @return Returns {@link OHOS_IMAGE_RESULT_SUCCESS} if the operation is successful; returns an error code otherwise.
 * @see {@link OhosImageSource}, {@link OhosImageSourceOps}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_Create(napi_env env, struct OhosImageSource* src,
    struct OhosImageSourceOps* ops, napi_value *res);

/**
 * @brief Creates an incremental <b>ImageSource</b> object at the JavaScript native layer based on the specified
 * {@link OhosImageSource} and {@link OhosImageSourceOps} structs.
 * The image source data will be updated through {@link OH_ImageSource_UpdateData}.
 *
 * @param env Indicates a pointer to the JNI environment.
 * @param src Indicates a pointer to the input resource of the image source. Only the buffer type is accepted.
 * For details, see {@link OhosImageSource}.
 * @param ops Indicates a pointer to the options for creating the image source.
 * For details, see {@link OhosImageSourceOps}.
 * @param res Indicates a pointer to the <b>ImageSource</b> object created at the JavaScript native layer.
 * @return Returns {@link OHOS_IMAGE_RESULT_SUCCESS} if the operation is successful; returns an error code otherwise.
 * @see {@link OhosImageSource}, {@link OhosImageSourceOps}, {@link OH_ImageSource_UpdateData}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_CreateIncremental(napi_env env, struct OhosImageSource* source,
    struct OhosImageSourceOps* ops, napi_value *res);

/**
 * @brief Obtains all supported decoding formats.
 *
 * @param res Indicates a pointer to the <b>OhosImageSourceSupportedFormatList</b> struct.
 * When the input <b>supportedFormatList</b> is a null pointer and <b>size</b> is 0, the size of the supported formats
 * is returned through <b>size</b> in <b>res</b>.
 * To obtain all formats, a space larger than <b>size</b> is required.
 * In addition, sufficient space must be reserved for each format supported.
 * @return Returns {@link OHOS_IMAGE_RESULT_SUCCESS} if the operation is successful; returns an error code otherwise.
 * @see {@link OhosImageSourceSupportedFormatList}, {@link OhosImageSourceSupportedFormat}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_GetSupportedFormats(struct OhosImageSourceSupportedFormatList* res);
/**
 * @brief Converts an {@link ImageSource} object at the JavaScript native layer to an <b>ImageSourceNative</b> object
 * at the C++ native layer.
 *
 * @param env Indicates a pointer to the JNI environment.
 * @param source Indicates a pointer to the <b>ImageSource</b> object at the JavaScript native layer.
 * @return Returns a pointer to the {@link ImageSourceNative} object if the operation is successful;
 * returns a null pointer otherwise.
 * @see {@link ImageSourceNative}, {@link OH_ImageSource_Release}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
ImageSourceNative* OH_ImageSource_InitNative(napi_env env, napi_value source);

/**
 * @brief Decodes an <b>ImageSource</b> object to obtain a <b>PixelMap</b> object at the JavaScript native layer
 * based on the specified {@link OhosImageDecodingOps} struct.
 *
 * @param native Indicates a pointer to the {@link ImageSourceNative} object at the C++ native layer.
 * @param ops Indicates a pointer to the options for decoding the image source.
 * For details, see {@link OhosImageDecodingOps}.
 * @param res Indicates a pointer to the <b>PixelMap</b> object obtained at the JavaScript native layer.
 * @return Returns {@link OHOS_IMAGE_RESULT_SUCCESS} if the operation is successful; returns an error code otherwise.
 * @see {@link ImageSourceNative}, {@link OhosImageDecodingOps}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_CreatePixelMap(const ImageSourceNative* native,
    struct OhosImageDecodingOps* ops, napi_value *res);

/**
 * @brief Decodes an <b>ImageSource</b> to obtain all the <b>PixelMap</b> objects at the JavaScript native layer
 * based on the specified {@link OhosImageDecodingOps} struct.
 *
 * @param native Indicates a pointer to the {@link ImageSourceNative} object at the C++ native layer.
 * @param ops Indicates a pointer to the options for decoding the image source.
 * For details, see {@link OhosImageDecodingOps}.
 * @param res Indicates a pointer to the <b>PixelMap</b> objects obtained at the JavaScript native layer.
 * @return Returns {@link OHOS_IMAGE_RESULT_SUCCESS} if the operation is successful; returns an error code otherwise.
 * @see {@link ImageSourceNative}, {@link OhosImageDecodingOps}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_CreatePixelMapList(const ImageSourceNative* native,
    struct OhosImageDecodingOps* ops, napi_value *res);

/**
 * @brief Obtains the delay time list from some <b>ImageSource</b> objects (such as GIF image sources).
 *
 * @param native Indicates a pointer to the {@link ImageSourceNative} object at the C++ native layer.
 * @param res Indicates a pointer to the delay time list obtained.
 * For details, see {@link OhosImageSourceDelayTimeList}. When the input <b>delayTimeList</b> is a null pointer and
 * <b>size</b> is <b>0</b>, the size of the delay time list is returned through <b>size</b> in <b>res</b>.
 * To obtain the complete delay time list, a space greater than <b>size</b> is required.
 * @return Returns {@link OHOS_IMAGE_RESULT_SUCCESS} if the operation is successful; returns an error code otherwise.
 * @see {@link ImageSourceNative}, {@link OhosImageSourceDelayTimeList}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_GetDelayTime(const ImageSourceNative* native,
    struct OhosImageSourceDelayTimeList* res);

/**
 * @brief Obtains the number of frames from an <b>ImageSource</b> object.
 *
 * @param native Indicates a pointer to the {@link ImageSourceNative} object at the C++ native layer.
 * @param res Indicates a pointer to the number of frames obtained.
 * @return Returns {@link OHOS_IMAGE_RESULT_SUCCESS} if the operation is successful; returns an error code otherwise.
 * @see {@link ImageSourceNative}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_GetFrameCount(const ImageSourceNative* native, uint32_t *res);

/**
 * @brief Obtains image source information from an <b>ImageSource</b> object by index.
 *
 * @param native Indicates a pointer to the {@link ImageSourceNative} object at the C++ native layer.
 * @param index Indicates the index of the frame.
 * @param info Indicates a pointer to the image source information obtained.
 * For details, see {@link OhosImageSourceInfo}.
 * @return Returns {@link OHOS_IMAGE_RESULT_SUCCESS} if the operation is successful; returns an error code otherwise.
 * @see {@link ImageSourceNative}, {@link OhosImageSourceInfo}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_GetImageInfo(const ImageSourceNative* native, int32_t index,
    struct OhosImageSourceInfo* info);

/**
 * @brief Obtains the value of an image property from an <b>ImageSource</b> object.
 *
 * @param native Indicates a pointer to the {@link ImageSourceNative} object at the C++ native layer.
 * @param key Indicates a pointer to the property. For details, see {@link OhosImageSourceProperty}.
 * @param value Indicates a pointer to the property value obtained.
 * If the input <b>value</b> is a null pointer and <b>size</b> is <b>0</b>, the size of the property value is returned
 * through <b>size</b> in <b>value</b>.
 * To obtain the complete property value, a space greater than <b>size</b> is required.
 * @return Returns {@link OHOS_IMAGE_RESULT_SUCCESS} if the operation is successful; returns an error code otherwise.
 * @see {@link ImageSourceNative}, {@link OhosImageSourceProperty}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_GetImageProperty(const ImageSourceNative* native,
    struct OhosImageSourceProperty* key, struct OhosImageSourceProperty* value);

/**
 * @brief Modifies the value of an image property of an <b>ImageSource</b> object.
 *
 * @param native Indicates a pointer to the {@link ImageSourceNative} object at the C++ native layer.
 * @param key Indicates a pointer to the property. For details, see {@link OhosImageSourceProperty}.
 * @param value Indicates a pointer to the new value of the property.
 * @return Returns {@link OHOS_IMAGE_RESULT_SUCCESS} if the operation is successful; returns an error code otherwise.
 * @see {@link ImageSourceNative}, {@link OhosImageSourceProperty}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_ModifyImageProperty(const ImageSourceNative* native,
    struct OhosImageSourceProperty* key, struct OhosImageSourceProperty* value);

/**
 * @brief Updates the data of an <b>ImageSource</b> object.
 *
 * @param native Indicates a pointer to the {@link ImageSourceNative} object at the C++ native layer.
 * @param data Indicates a pointer to the update data. For details, see {@link OhosImageSourceUpdateData}.
 * @return Returns {@link OHOS_IMAGE_RESULT_SUCCESS} if the operation is successful; returns an error code otherwise.
 * @see {@link ImageSourceNative}, {@link OhosImageSourceUpdateData}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_UpdateData(const ImageSourceNative* native, struct OhosImageSourceUpdateData* data);


/**
 * @brief Releases an <b>ImageSourceNative</b> object.
 *
 * @param native Indicates a pointer to the {@link ImageSourceNative} object at the C++ native layer.
 * @return Returns {@link OHOS_IMAGE_RESULT_SUCCESS} if the operation is successful; returns an error code otherwise.
 * @see {@link ImageSourceNative}, {@link OH_ImageSource_Create}, {@link OH_ImageSource_CreateIncremental}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_Release(ImageSourceNative* native);
#ifdef __cplusplus
};
#endif
/** @} */
} // namespace Media
} // namespace OHOS
#endif // INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_SOURCE_MDK_H_
