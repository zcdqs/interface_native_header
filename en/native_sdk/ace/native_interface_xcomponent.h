/*
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OH_NativeXComponent Native XComponent
 * @{
 *
 * @brief Describes the surface and touch event held by the ArkUI XComponent, which can be used for the EGL/OpenGL ES
 * and media data input and displayed on the ArkUI XComponent.
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file native_interface_xcomponent.h
 *
 * @brief Declares the APIs used to access <b>NativeXComponent</b>.
 *
 * @library libace_ndk.z.so
 * @since 8
 * @version 1.0
 */

#ifndef _NATIVE_INTERFACE_XCOMPONENT_H_
#define _NATIVE_INTERFACE_XCOMPONENT_H_

#include <stdint.h>
#include <stdbool.h>

#include "arkui/native_type.h"
#include "arkui/ui_input_event.h"

#include "native_interface_accessibility.h"
#include "native_xcomponent_key_event.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the API access states.
 *
 * @since 8
 * @version 1.0
 */
enum {
    /** Success. */
    OH_NATIVEXCOMPONENT_RESULT_SUCCESS = 0,
    /** Failure. */
    OH_NATIVEXCOMPONENT_RESULT_FAILED = -1,
    /** Invalid parameter. */
    OH_NATIVEXCOMPONENT_RESULT_BAD_PARAMETER = -2,
};

/**
 * @brief Enumerates touch event types.
 * @since 8
 */
typedef enum {
    /** The touch event is triggered when a finger is pressed. */
    OH_NATIVEXCOMPONENT_DOWN = 0,
    /** The touch event is triggered when a finger is lifted. */
    OH_NATIVEXCOMPONENT_UP,
    /** The touch event is triggered when a finger is moved on the screen. */
    OH_NATIVEXCOMPONENT_MOVE,
    /** The touch event is triggered when a touch is canceled. */
    OH_NATIVEXCOMPONENT_CANCEL,
    /** Invalid touch type. */
    OH_NATIVEXCOMPONENT_UNKNOWN,
} OH_NativeXComponent_TouchEventType;

/**
 * @brief Enumerates touch point tool types.
 *
 * @since 9
 * @version 1.0
 */
typedef enum {
    /** Unknown tool type. */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_UNKNOWN = 0,
    /** Finger. */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_FINGER,
    /** Stylus. */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_PEN,
    /** Rubber. */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_RUBBER,
    /** Brush. */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_BRUSH,
    /** Pencil. */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_PENCIL,
    /** Air brush. */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_AIRBRUSH,
    /** Mouse. */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_MOUSE,
    /** Lens. */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_LENS,
} OH_NativeXComponent_TouchPointToolType;

/**
 * @brief Enumerates touch event source types.
 *
 * @since 9
 * @version 1.0
 */
typedef enum {
    /** Unknown source type. */
    OH_NATIVEXCOMPONENT_SOURCE_TYPE_UNKNOWN = 0,
    /** Source that generates a mouse multi-click event. */
    OH_NATIVEXCOMPONENT_SOURCE_TYPE_MOUSE,
    /** Source that generates a touchscreen multi-touch event. */
    OH_NATIVEXCOMPONENT_SOURCE_TYPE_TOUCHSCREEN,
    /** Source that generates a touchpad multi-touch event. */
    OH_NATIVEXCOMPONENT_SOURCE_TYPE_TOUCHPAD,
    /** Source that generates a joystick multi-touch event. */
    OH_NATIVEXCOMPONENT_SOURCE_TYPE_JOYSTICK,
    /**
     * @brief Describes the source that generates a key event.
     *
     * @since 10
     * @version 1.0
     */
    OH_NATIVEXCOMPONENT_SOURCE_TYPE_KEYBOARD,
} OH_NativeXComponent_EventSourceType;

/**
 * @brief Enumerates the mouse event actions.
 *
 * @since 9
 * @version 1.0
 */
typedef enum {
    /** Invalid mouse event. */
    OH_NATIVEXCOMPONENT_MOUSE_NONE = 0,
    /** Mouse button press. */
    OH_NATIVEXCOMPONENT_MOUSE_PRESS,
    /** Mouse button release. */
    OH_NATIVEXCOMPONENT_MOUSE_RELEASE,
    /** Mouse movement. */
    OH_NATIVEXCOMPONENT_MOUSE_MOVE,
} OH_NativeXComponent_MouseEventAction;

/**
 * @brief Enumerates the mouse event buttons.
 *
 * @since 9
 * @version 1.0
 */
typedef enum {
    /** No button. */
    OH_NATIVEXCOMPONENT_NONE_BUTTON = 0,
    /** Left mouse button. */
    OH_NATIVEXCOMPONENT_LEFT_BUTTON = 0x01,
    /** Right mouse button. */
    OH_NATIVEXCOMPONENT_RIGHT_BUTTON = 0x02,
    /** Middle mouse button. */
    OH_NATIVEXCOMPONENT_MIDDLE_BUTTON = 0x04,
    /** Back button on the left of the mouse. */
    OH_NATIVEXCOMPONENT_BACK_BUTTON = 0x08,
    /** Forward key on the left of the mouse. */
    OH_NATIVEXCOMPONENT_FORWARD_BUTTON = 0x10,
} OH_NativeXComponent_MouseEventButton;

#define OH_NATIVE_XCOMPONENT_OBJ ("__NATIVE_XCOMPONENT_OBJ__")
const uint32_t OH_XCOMPONENT_ID_LEN_MAX = 128;
const uint32_t OH_MAX_TOUCH_POINTS_NUMBER = 10;

typedef struct {
    /** Unique identifier of the finger. */
    int32_t id = 0;
    /** X coordinate of the touch point relative to the left edge of the application window where the XComponent is
      * located.
      */
    float screenX = 0.0;
    /** Y coordinate of the touch point relative to the left edge of the application window where the XComponent is
      * located.
      */
    float screenY = 0.0;
    /** X coordinate of the touch point relative to the left edge of the XComponent. */
    float x = 0.0;
    /** Y coordinate of the touch point relative to the upper edge of the XComponent. */
    float y = 0.0;
    /** Touch type of the touch point. */
    OH_NativeXComponent_TouchEventType type = OH_NativeXComponent_TouchEventType::OH_NATIVEXCOMPONENT_UNKNOWN;
    /** Contact area between the finger pad and the screen. */
    double size = 0.0;
    /** Pressure of the touch event. */
    float force = 0.0;
    /** Timestamp of the touch point. It is interval between the time when the event is triggered and the time when
     *  the system starts, in nanoseconds. */
    long long timeStamp = 0;
    /** Whether the current point is pressed. */
    bool isPressed = false;
} OH_NativeXComponent_TouchPoint;

/**
 * @brief Describes the touch event.
 *
 * @since 8
 * @version 1.0
 */
typedef struct {
    /** Unique identifier of the finger. */
    int32_t id = 0;
    /** X coordinate of the touch point relative to the upper left corner of the application window where the XComponent
     *  is located. */
    float screenX = 0.0;
    /** Y coordinate of the touch point relative to the upper left corner of the application window where the XComponent
     *  is located. */
    float screenY = 0.0;
    /** X coordinate of the touch point relative to the left edge of the XComponent. */
    float x = 0.0;
    /** Y coordinate of the touch point relative to the upper edge of the XComponent. */
    float y = 0.0;
    /** Touch type of the touch point. */
    OH_NativeXComponent_TouchEventType type = OH_NativeXComponent_TouchEventType::OH_NATIVEXCOMPONENT_UNKNOWN;
    /** Contact area between the finger pad and the screen. */
    double size = 0.0;
    /** Pressure of the touch event. */
    float force = 0.0;
    /** ID of the device where the current touch event is triggered. */
    int64_t deviceId = 0;
    /** Timestamp of the touch point. It is interval between the time when the event is triggered and the time when
     *  the system starts, in nanoseconds. */
    long long timeStamp = 0;
    /** Array of the touch points. */
    OH_NativeXComponent_TouchPoint touchPoints[OH_MAX_TOUCH_POINTS_NUMBER];
    /** Number of current touch points. */
    uint32_t numPoints = 0;
} OH_NativeXComponent_TouchEvent;

/**
 * @brief Describes the mouse event.
 *
 * @since 9
 * @version 1.0
 */
typedef struct {
    /** X coordinate of the clicked point relative to the upper left corner of the component. */
    float x;
    /** Y coordinate of the clicked point relative to the upper left corner of the component. */
    float y;
    /** X coordinate of the clicked point relative to the upper left corner of the application screen where the
     *  XComponent is located. */
    float screenX;
    /** Y coordinate of the clicked point relative to the upper left corner of the application window where the
     *  XComponent is located. */
    float screenY;
    /** Timestamp of the mouse event. It is interval between the time when the event is triggered and the time when
     *  the system starts, in nanoseconds. */
    int64_t timestamp;
    /** Action of the mouse event. */
    OH_NativeXComponent_MouseEventAction action;
    /** Mouse event button. */
    OH_NativeXComponent_MouseEventButton button;
} OH_NativeXComponent_MouseEvent;

/**
 * @brief Provides an encapsulated <b>OH_NativeXComponent</b> instance.
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_NativeXComponent OH_NativeXComponent;

/**
 * @brief Registers callbacks for the surface lifecycle and touch event.
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_NativeXComponent_Callback {
    /** Invoked when a surface is created. */
    void (*OnSurfaceCreated)(OH_NativeXComponent* component, void* window);
    /** Invoked when the surface changes. */
    void (*OnSurfaceChanged)(OH_NativeXComponent* component, void* window);
    /** Invoked when the surface is destroyed. */
    void (*OnSurfaceDestroyed)(OH_NativeXComponent* component, void* window);
    /** Invoked when a touch event is triggered. */
    void (*DispatchTouchEvent)(OH_NativeXComponent* component, void* window);
} OH_NativeXComponent_Callback;

/**
 * @brief Registers callbacks for the mouse event.
 *
 * @since 9
 * @version 1.0
 */
typedef struct OH_NativeXComponent_MouseEvent_Callback {
    /** Invoked when a mouse event is triggered. */
    void (*DispatchMouseEvent)(OH_NativeXComponent* component, void* window);
    /** Invoked when a hover event is triggered. */
    void (*DispatchHoverEvent)(OH_NativeXComponent* component, bool isHover);
} OH_NativeXComponent_MouseEvent_Callback;

struct OH_NativeXComponent_KeyEvent;
/**
 * @brief Provides an encapsulated <b>OH_NativeXComponent_KeyEvent</b> instance.
 *
 * @since 10
 * @version 1.0
 */
typedef struct OH_NativeXComponent_KeyEvent OH_NativeXComponent_KeyEvent;

/**
 * @brief Defines the expected frame rate range.
 *
 * @since 11
 * @version 1.0
 */
typedef struct {
    /** Minimum value of the expected frame rate range. */
    int32_t min;
    /** Maximum value of the expected frame rate range. */
    int32_t max;
    /** Expected frame rate range. */
    int32_t expected;
} OH_NativeXComponent_ExpectedRateRange;

/**
 * @brief Obtains the ID of the ArkUI XComponent.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param id Indicates the pointer to the character buffer for storing the ID of the <b>OH_NativeXComponent</b> instance.
 * Note that null terminators will be attached to the character buffer, so the size of the character buffer should be
 * at least one unit greater than the length of the real ID.
 * The recommended size is [OH_XCOMPONENT_ID_LEN_MAX + 1].
 * @param size Indicates the pointer to the length of the ID.
 * @return Returns the status code of the execution.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetXComponentId(OH_NativeXComponent* component, char* id, uint64_t* size);

/**
 * @brief Obtains the size of the surface held by the ArkUI XComponent.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param window Indicates the handle to the <b>NativeWindow</b> instance.
 * @param width Indicates the pointer to the width of the current surface.
 * @param height Indicates the pointer to the height of the current surface.
 * @return Returns the status code of the execution.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetXComponentSize(
    OH_NativeXComponent* component, const void* window, uint64_t* width, uint64_t* height);

/**
 * @brief Obtains the offset of the surface held by an XComponent relative to the upper left corner of the window.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param window Indicates the handle to the <b>NativeWindow</b> instance.
 * @param x Indicates the pointer to the X coordinate of the current surface relative to the upper left corner of the
 *          XComponent's parent component.
 * @param y Indicates the pointer to the Y coordinate of the current surface relative to the upper left corner of the
 *          XComponent's parent component.
 * @return Returns the status code of the execution.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetXComponentOffset(
    OH_NativeXComponent* component, const void* window, double* x, double* y);

/**
 * @brief Obtains the touch event scheduled by the ArkUI XComponent.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param window Indicates the handle to the <b>NativeWindow</b> instance.
 * @param touchEvent Indicates the pointer to the current touch event.
 * @return Returns the status code of the execution.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchEvent(
    OH_NativeXComponent* component, const void* window, OH_NativeXComponent_TouchEvent* touchEvent);

/**
 * @brief Obtains the ArkUI XComponent touch point tool type.
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param pointIndex Indicates the pointer to the index of the touch point.
 * @param toolType Indicates the pointer to the tool type.
 * @return Returns the status code of the execution.
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchPointToolType(OH_NativeXComponent* component, uint32_t pointIndex,
    OH_NativeXComponent_TouchPointToolType* toolType);

/**
 * @brief Obtains the angle between the ArkUI XComponent touch point and the x-axis.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param pointIndex Indicates the pointer to the index of the touch point.
 * @param tiltX Indicates the pointer to the angle between the touch point and the x-axis.
 * @return Returns the status code of the execution.
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchPointTiltX(OH_NativeXComponent* component, uint32_t pointIndex, float* tiltX);

/**
 * @brief Obtains the angle between the ArkUI XComponent touch point and the y-axis.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param pointIndex Indicates the pointer to the index of the touch point.
 * @param tiltY Indicates the pointer to the angle between the touch point and the y-axis.
 * @return Returns the status code of the execution.
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchPointTiltY(OH_NativeXComponent* component, uint32_t pointIndex, float* tiltY);

/**
 * @brief Obtains the X coordinate of the touch point relative to the upper left corner of the application window where
 *        the ArkUI XComponent is located.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param pointIndex Indicates the pointer to the index of the touch point.
 * @param windowX Indicates the pointer to the X coordinate of the touch point relative to the upper left corner of
 *                the application window.
 * @return Returns the status code of the execution.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchPointWindowX(OH_NativeXComponent* component, uint32_t pointIndex, float* windowX);

/**
 * @brief Obtains the Y coordinate of the touch point relative to the upper left corner of the application window where
 *        the ArkUI XComponent is located.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param pointIndex Indicates the pointer to the index of the touch point.
 * @param windowY Indicates the pointer to the Y coordinate of the touch point relative to the upper left corner of the
 *                application window.
 * @return Returns the status code of the execution.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchPointWindowY(OH_NativeXComponent* component, uint32_t pointIndex, float* windowY);

/**
 * @brief Obtains the X coordinate of the touch point relative to the upper left corner of the screen where the ArkUI
 *        XComponent is located.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param pointIndex Indicates the pointer to the index of the touch point.
 * @param displayX Indicates the pointer to the X coordinate of the touch point relative to the upper left corner of the
 *                 screen.
 * @return Returns the status code of the execution.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchPointDisplayX(OH_NativeXComponent* component, uint32_t pointIndex, float* displayX);

/**
 * @brief Obtains the Y coordinate of the touch point relative to the upper left corner of the screen where the ArkUI
 *        XComponent is located.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param pointIndex Indicates the pointer to the index of the touch point.
 * @param displayY Indicates the pointer to the Y coordinate of the touch point relative to the upper left corner of the
 *                 screen.
 * @return Returns the status code of the execution.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchPointDisplayY(OH_NativeXComponent* component, uint32_t pointIndex, float* displayY);

/**
 * @brief Obtains the mouse event scheduled by ArkUI XComponent.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param window Indicates the handle to the <b>NativeWindow</b> instance.
 * @param mouseEvent Indicates the pointer to the current mouse event.
 * @return Returns the status code of the execution.
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetMouseEvent(
    OH_NativeXComponent* component, const void* window, OH_NativeXComponent_MouseEvent* mouseEvent);

/**
 * @brief Registers a callback for this <b>OH_NativeXComponent</b> instance.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param callback Indicates the pointe to the surface lifecycle and touch event callback.
 * @return Returns the status code of the execution.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterCallback(OH_NativeXComponent* component, OH_NativeXComponent_Callback* callback);

/**
 * @brief Registers a mouse event callback for this <b>OH_NativeXComponent</b> instance.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param callback Indicates the pointer to the mouse event callback.
 * @return Returns the status code of the execution.
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterMouseEventCallback(
    OH_NativeXComponent* component, OH_NativeXComponent_MouseEvent_Callback* callback);

/**
 * @brief Registers a focus event callback for this <b>OH_NativeXComponent</b> instance.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param callback Indicates the pointer to the focus event callback.
 * @return Returns the status code of the execution.
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterFocusEventCallback(
    OH_NativeXComponent* component, void (*callback)(OH_NativeXComponent* component, void* window));

/**
 * @brief Registers a key event callback for this <b>OH_NativeXComponent</b> instance.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param callback Indicates the pointer to the key event callback.
 * @return Returns the status code of the execution.
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterKeyEventCallback(
    OH_NativeXComponent* component, void (*callback)(OH_NativeXComponent* component, void* window));

/**
 * @brief Registers a blur event callback for this <b>OH_NativeXComponent</b> instance.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param callback Indicates the pointer to the blur event callback.
 * @return Returns the status code of the execution.
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterBlurEventCallback(
    OH_NativeXComponent* component, void (*callback)(OH_NativeXComponent* component, void* window));

/**
 * @brief Obtains the key event scheduled by the ArkUI XComponent.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param keyEvent Indicates the pointer to the current key event.
 * @return Returns the status code of the execution.
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetKeyEvent(OH_NativeXComponent* component, OH_NativeXComponent_KeyEvent** keyEvent);

/**
 * @brief Obtains the action of a key event.
 *
 * @param keyEvent Indicates the pointer to the <b>OH_NativeXComponent_KeyEvent</b> instance.
 * @param action Indicates the pointer to the key event action.
 * @return Returns the status code of the execution.
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetKeyEventAction(
    OH_NativeXComponent_KeyEvent* keyEvent, OH_NativeXComponent_KeyAction* action);

/**
 * @brief Obtains the key code of a key event.
 *
 * @param keyEvent Indicates the pointer to the <b>OH_NativeXComponent_KeyEvent</b> instance.
 * @param code Indicates the pointer to the key code of the key event.
 * @return Returns the status code of the execution.
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetKeyEventCode(OH_NativeXComponent_KeyEvent* keyEvent, OH_NativeXComponent_KeyCode* code);

/**
 * @brief Obtains the source type of a key event.
 *
 * @param keyEvent Indicates the pointer to the <b>OH_NativeXComponent_KeyEvent</b> instance.
 * @param sourceType Indicates the pointer to the key event source type.
 * @return Returns the status code of the execution.
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetKeyEventSourceType(
    OH_NativeXComponent_KeyEvent* keyEvent, OH_NativeXComponent_EventSourceType* sourceType);

/**
 * @brief Obtains the device ID of a key event.
 *
 * @param keyEvent Indicates the pointer to the <b>OH_NativeXComponent_KeyEvent</b> instance.
 * @param deviceId Indicates the pointer to the key event device ID.
 * @return Returns the status code of the execution.
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetKeyEventDeviceId(OH_NativeXComponent_KeyEvent* keyEvent, int64_t* deviceId);

/**
 * @brief Obtains the timestamp of a key event.
 *
 * @param keyEvent Indicates the pointer to the <b>OH_NativeXComponent_KeyEvent</b> instance.
 * @param timeStamp Indicates the pointer to the timestamp of the key event.
 * @return Returns the status code of the execution.
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetKeyEventTimeStamp(OH_NativeXComponent_KeyEvent* keyEvent, int64_t* timeStamp);

/**
 * @brief Sets the expected frame rate range.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param range Indicates the pointer to the expected frame rate range.
 * @return Returns the status code of the execution.
 * @since 11
 * @version 1.0
 */
int32_t OH_NativeXComponent_SetExpectedFrameRateRange(
    OH_NativeXComponent* component, OH_NativeXComponent_ExpectedRateRange* range);

/**
 * @brief Registers a display update callback for an <b>OH_NativeXComponent</b> instance and enables the callback
 * for each frame.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param callback Indicates the pointer to the display update callback.
 * @return Returns the status code of the execution.
 * @since 11
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterOnFrameCallback(OH_NativeXComponent* component,
    void (*callback)(OH_NativeXComponent* component, uint64_t timestamp, uint64_t targetTimestamp));

/**
 * @brief Deregisters the display update callback for an <b>OH_NativeXComponent</b> instance and disables the callback
 * for each frame.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @return Returns the status code of the execution.
 * @since 11
 * @version 1.0
 */
int32_t OH_NativeXComponent_UnregisterOnFrameCallback(OH_NativeXComponent* component);

/**
 * @brief Attaches the UI component created through the native API of ArkUI to this <b>OH_NativeXComponent</b> instance.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param root Indicates the pointer to the component instance created by the native API.
 * @return Returns 0 if success.
 *         Returns 401 if a parameter exception occurs.
 *
 * @since 12
 */
int32_t OH_NativeXComponent_AttachNativeRootNode(OH_NativeXComponent* component, ArkUI_NodeHandle root);

/**
 * @brief Detaches the native component of ArkUI from this <b>OH_NativeXComponent</b> instance.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param root Indicates the pointer to the component instance created by the native API.
 * @return Returns 0 if success.
 *         Returns 401 if a parameter exception occurs.
 *
 * @since 12
 */
int32_t OH_NativeXComponent_DetachNativeRootNode(OH_NativeXComponent* component, ArkUI_NodeHandle root);

/**
 * @brief Registers a UI input event callback for an <b>OH_NativeXComponent</b> instance and enables the callback to
 * be invoked when a UI input event is received.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param callback Indicates the pointer to the UI input event callback.
 * @param type Indicates the type of the current UI input event.
 * @return Returns 0 if success.
 *         Returns 401 if a parameter exception occurs.
 *
 * @since 12
 */
int32_t OH_NativeXComponent_RegisterUIInputEventCallback(
    OH_NativeXComponent *component,
    void (*callback)(OH_NativeXComponent *component, ArkUI_UIInputEvent *event,
                     ArkUI_UIInputEvent_Type type),
    ArkUI_UIInputEvent_Type type);

/**
 * @brief Registers a custom event intercept callback for an <b>OH_NativeXComponent</b> and enables the callback
 * during the hit test.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param callback Indicates the pointer to the custom event intercept callback.
 * @return Returns 0 if success.
 *         Returns 401 if a parameter exception occurs.
 * @since 12
 */
int32_t OH_NativeXComponent_RegisterOnTouchInterceptCallback(
    OH_NativeXComponent* component, HitTestMode (*callback)(OH_NativeXComponent* component, ArkUI_UIInputEvent* event));

/**
 * @brief Specifies whether a soft keyboard is required for an <b>OH_NativeXComponent</b> instance.
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param needSoftKeyboard Indicates whether a soft keyboard is required.
 * @return Returns the status code of the execution.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeXComponent_SetNeedSoftKeyboard(OH_NativeXComponent* component, bool needSoftKeyboard);

/**
 * @brief Registers a surface display callback for an <b>OH_NativeXComponent</b> instance.
 * The callback is invoked whenever the application is switched to the foreground.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param callback Indicates the pointer to the surface display callback.
 * @return Returns the status code of the execution.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterSurfaceShowCallback(
    OH_NativeXComponent* component, void (*callback)(OH_NativeXComponent* component, void* window));

/**
 * @brief Registers a surface hiding callback for an <b>OH_NativeXComponent</b> instance.
 * The callback is invoked whenever the application is switched to the background.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param callback Indicates the pointer to the surface hiding callback.
 * @return Returns the status code of the execution.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterSurfaceHideCallback(
    OH_NativeXComponent* component, void (*callback)(OH_NativeXComponent* component, void* window));

/**
 * @brief Obtains the touch event source type of an <b>OH_NativeXComponent</b> instance.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param pointId Indicates the ID of a touch point.
 * @param sourceType Indicates the pointer to the source type.
 * @return Returns <b>OH_NATIVEXCOMPONENT_RESULT_SUCCESS</b> if the operation is successful.
 *         Returns <b>OH_NATIVEXCOMPONENT_RESULT_BAD_PARAMETER</b> if a parameter error occurs.
 *         Returns <b>OH_NATIVEXCOMPONENT_RESULT_FAILED</b> if any other error occurs.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchEventSourceType(
    OH_NativeXComponent* component, int32_t pointId, OH_NativeXComponent_EventSourceType* sourceType);

/**
 * @brief Obtains the pointer to an <b>OH_NativeXComponent</b> instance based on the specified component instance
 * created by the native API.
 *
 * @param node Indicates the pointer to the component instance created by the native API.
 * @return Returns the pointer to the <b>OH_NativeXComponent</b> instance.
 * @since 12
 * @version 1.0
 */
OH_NativeXComponent* OH_NativeXComponent_GetNativeXComponent(ArkUI_NodeHandle node);

/**
 * @brief Obtains the pointer to the <b> ArkUI_AccessibilityProvider</b> instance of this <b>OH_NativeXComponent</b> instance.
 *
 * @param component Indicates the pointer to the <b>OH_NativeXComponent</b> instance.
 * @param provider Indicates the pointer to the <b>ArkUI_AccessibilityProvider</b> instance.
 * @return Returns <b>OH_NATIVEXCOMPONENT_RESULT_SUCCESS</b> if the operation is successful.
 *         Returns <b>OH_NATIVEXCOMPONENT_RESULT_BAD_PARAMETER</b> if a parameter error occurs.
 *         Returns <b>OH_NATIVEXCOMPONENT_RESULT_FAILED</b> if any other error occurs.
 * @since 13
 */
int32_t OH_NativeXComponent_GetNativeAccessibilityProvider(
    OH_NativeXComponent* component, ArkUI_AccessibilityProvider** handle);

#ifdef __cplusplus
};
#endif
#endif // _NATIVE_INTERFACE_XCOMPONENT_H_
