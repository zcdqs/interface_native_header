/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup ArkUI_NativeModule
 * @{
 *
 * @brief Provides UI capabilities of ArkUI on the native side, such as UI component creation and destruction,
 * tree node operations, attribute setting, and event listening.
 *
 * @since 12
 */

/**
 * @file native_type.h
 *
 * @brief Defines the common types for the native module.
 *
 * @library libace_ndk.z.so
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @since 12
 */

#ifndef ARKUI_NATIVE_TYPE_H
#define ARKUI_NATIVE_TYPE_H

#include <stdint.h>
#include "drawable_descriptor.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines the ArkUI native component object.
 *
 * @since 12
 */
struct ArkUI_Node;

/**
 * @brief Defines the custom dialog box controller of ArkUI on the native side.
 *
 * @since 12
 */
struct ArkUI_NativeDialog;

/**
 * @brief Sets the size constraints of a component during component layout.
 *
 * @since 12
 */
typedef struct ArkUI_LayoutConstraint ArkUI_LayoutConstraint;

/**
 * @brief Defines the structure of the component drawing context.
 *
 * @since 12
 */
typedef struct ArkUI_DrawContext ArkUI_DrawContext;

/**
 * @brief Defines the handle to the ArkUI native component object.
 *
 * @since 12
 */
typedef struct ArkUI_Node* ArkUI_NodeHandle;

/**
 * @brief Defines the handle to the custom dialog box controller of ArkUI on the native side.
 *
 * @since 12
 */
typedef struct ArkUI_NativeDialog* ArkUI_NativeDialogHandle;

/**
 * @brief Defines the water flow section configuration.
 *
 * @since 12
 */
typedef struct ArkUI_WaterFlowSectionOption ArkUI_WaterFlowSectionOption;

/**
 * @brief Defines the item configuration for <b>ListItemSwipeActionOption</b>.
 *
 * @since 12
 */
typedef struct ArkUI_ListItemSwipeActionItem ArkUI_ListItemSwipeActionItem;

/**
 * @brief Defines the configuration for <b>ListItemSwipeActionOption</b>.
 *
 * @since 12
 */
typedef struct ArkUI_ListItemSwipeActionOption ArkUI_ListItemSwipeActionOption;

/**
 * @brief Defines the ArkUI native UI context.
 *
 * @since 12
 */
struct ArkUI_Context;

/**
 * @brief Defines the handle to the ArkUI native UI context.
 *
 * @since 12
 */
typedef struct ArkUI_Context* ArkUI_ContextHandle;

/**
 * @brief Defines the handle to the ArkUI NodeContent instance on the native side.
 *
 * @since 12
 */
typedef struct ArkUI_NodeContent* ArkUI_NodeContentHandle;

/**
 * @brief Defines the alignment rule in the relative container.
 *
 * @since 12
 */
typedef struct ArkUI_AlignmentRuleOption ArkUI_AlignmentRuleOption;

/**
 * @brief Defines the ID, direction, and position of a guideline.
 *
 * @since 12
 */
typedef struct ArkUI_GuidelineOption ArkUI_GuidelineOption;

/**
 * @brief Defines the ID, direction, and referenced component of a barrier.
 *
 * @since 12
 */
typedef struct ArkUI_BarrierOption ArkUI_BarrierOption;

/**
 * @brief Defines the image frame information.
 *
 * @since 12
*/
typedef struct ArkUI_ImageAnimatorFrameInfo ArkUI_ImageAnimatorFrameInfo;

/**
 * @brief Defines the <b>ChildrenMainSize</b> class information of the list.
 *
 * @since 12
*/
typedef struct ArkUI_ListChildrenMainSize ArkUI_ListChildrenMainSize;

/**
 * @brief Defines a struct for the custom property information.
 *
 * @since 14
 */
typedef struct ArkUI_CustomProperty;

/**
 * @brief Defines a struct for active child node information.
 *
 * @since 14
 */
typedef struct ArkUI_ActiveChildrenInfo;

/**
 * @brief Defines the event callback type.
 *
 * @since 12
 */
typedef struct {
    /** Custom type. */
    void* userData;
    /** Event callback. */
    void (*callback)(void* userData);
} ArkUI_ContextCallback;

/**
 * @brief Provides the number types of ArkUI in the native code.
 *
 * @since 12
 */
typedef union {
    /** Floating-point type. */
    float f32;
    /** Signed integer. */
    int32_t i32;
    /** Unsigned integer. */
    uint32_t u32;
} ArkUI_NumberValue;

/**
 * @brief Enumerates the alignment modes.
 *
 * @since 12
 */
typedef enum {
    /** Top start. */
    ARKUI_ALIGNMENT_TOP_START = 0,
    /** Top center. */
    ARKUI_ALIGNMENT_TOP,
    /** Top end. */
    ARKUI_ALIGNMENT_TOP_END,
    /** Vertically centered start. */
    ARKUI_ALIGNMENT_START,
    /** Horizontally and vertically centered. */
    ARKUI_ALIGNMENT_CENTER,
    /** Vertically centered end. */
    ARKUI_ALIGNMENT_END,
    /** Bottom start. */
    ARKUI_ALIGNMENT_BOTTOM_START,
    /** Horizontally centered on the bottom. */
    ARKUI_ALIGNMENT_BOTTOM,
    /** Bottom end. */
    ARKUI_ALIGNMENT_BOTTOM_END,
} ArkUI_Alignment;

/**
 * @brief Enumerates the image repeat patterns.
 *
 * @since 12
 */
typedef enum {
    /** The image is not repeatedly drawn. */
    ARKUI_IMAGE_REPEAT_NONE = 0,
    /** The image is repeatedly drawn only along the x-axis. */
    ARKUI_IMAGE_REPEAT_X,
    /** The image is repeatedly drawn only along the y-axis. */
    ARKUI_IMAGE_REPEAT_Y,
    /** The image is repeatedly drawn along both axes. */
    ARKUI_IMAGE_REPEAT_XY,
} ArkUI_ImageRepeat;

/**
 * @brief Enumerates the font styles.
 *
 * @since 12
 */
typedef enum {
    /** Standard font style. */
    ARKUI_FONT_STYLE_NORMAL = 0,
    /** Italic font style. */
    ARKUI_FONT_STYLE_ITALIC
} ArkUI_FontStyle;

/**
 * @brief Enumerates the font weights.
 *
 * @since 12
 */
typedef enum {
    /** 100 */
    ARKUI_FONT_WEIGHT_W100 = 0,
    /** 200 */
    ARKUI_FONT_WEIGHT_W200,
    /** 300 */
    ARKUI_FONT_WEIGHT_W300,
    /** 400 */
    ARKUI_FONT_WEIGHT_W400,
    /** 500 */
    ARKUI_FONT_WEIGHT_W500,
    /** 600 */
    ARKUI_FONT_WEIGHT_W600,
    /** 700 */
    ARKUI_FONT_WEIGHT_W700,
    /** 800 */
    ARKUI_FONT_WEIGHT_W800,
    /** 900 */
    ARKUI_FONT_WEIGHT_W900,
    /** The font weight is bold. */
    ARKUI_FONT_WEIGHT_BOLD,
    /** The font weight is normal. */
    ARKUI_FONT_WEIGHT_NORMAL,
    /** The font weight is bolder. */
    ARKUI_FONT_WEIGHT_BOLDER,
    /** The font weight is lighter. */
    ARKUI_FONT_WEIGHT_LIGHTER,
    /** The font weight is medium. */
    ARKUI_FONT_WEIGHT_MEDIUM,
    /** The font weight is normal. */
    ARKUI_FONT_WEIGHT_REGULAR,
} ArkUI_FontWeight;

/**
 * @brief Enumerates the text alignment mode.
 *
 * @since 12
 */
typedef enum {
    /** Aligned with the start. */
    ARKUI_TEXT_ALIGNMENT_START = 0,
    /** Horizontally centered. */
    ARKUI_TEXT_ALIGNMENT_CENTER,
    /** Aligned with the end. */
    ARKUI_TEXT_ALIGNMENT_END,
    /** Aligned with both margins. */
    ARKUI_TEXT_ALIGNMENT_JUSTIFY,
} ArkUI_TextAlignment;

/**
 * @brief Enumerates the types of the Enter key for a single-line text box.
 *
 * @since 12
 */
typedef enum {
    /** The Enter key is labeled "Go." */
    ARKUI_ENTER_KEY_TYPE_GO = 2,
    /** The Enter key is labeled "Search." */
    ARKUI_ENTER_KEY_TYPE_SEARCH = 3,
    /** The Enter key is labeled "Send." */
    ARKUI_ENTER_KEY_TYPE_SEND,
    /** The Enter key is labeled "Next." */
    ARKUI_ENTER_KEY_TYPE_NEXT,
    /** The Enter key is labeled "Done." */
    ARKUI_ENTER_KEY_TYPE_DONE,
    /** The Enter key is labeled "Previous." */
    ARKUI_ENTER_KEY_TYPE_PREVIOUS,
    /** The Enter key is labeled "New Line." */
    ARKUI_ENTER_KEY_TYPE_NEW_LINE,
} ArkUI_EnterKeyType;

/**
 * @brief Enumerates the text input types.
 *
 * @since 12
 */
typedef enum {
    /** Normal input mode. */
    ARKUI_TEXTINPUT_TYPE_NORMAL = 0,
    /** Number input mode. */
    ARKUI_TEXTINPUT_TYPE_NUMBER = 2,
    /** Phone number input mode. */
    ARKUI_TEXTINPUT_TYPE_PHONE_NUMBER = 3,
    /** Email address input mode. */
    ARKUI_TEXTINPUT_TYPE_EMAIL = 5,
    /** Password input mode. */
    ARKUI_TEXTINPUT_TYPE_PASSWORD = 7,
    /** Numeric password input mode. */
    ARKUI_TEXTINPUT_TYPE_NUMBER_PASSWORD = 8,
    /** Lock screen password input mode. */
    ARKUI_TEXTINPUT_TYPE_SCREEN_LOCK_PASSWORD = 9,
    /** Username input mode. */
    ARKUI_TEXTINPUT_TYPE_USER_NAME = 10,
    /** New password input mode. */
    ARKUI_TEXTINPUT_TYPE_NEW_PASSWORD = 11,
    /** Number input mode with a decimal point. */
    ARKUI_TEXTINPUT_TYPE_NUMBER_DECIMAL = 12,
} ArkUI_TextInputType;

/**
 * @brief Enumerates the text box types.
 *
 * @since 12
 */
typedef enum {
    /** Normal input mode. */
    ARKUI_TEXTAREA_TYPE_NORMAL = 0,
    /** Number input mode. */
    ARKUI_TEXTAREA_TYPE_NUMBER = 2,
    /** Phone number input mode. */
    ARKUI_TEXTAREA_TYPE_PHONE_NUMBER = 3,
    /** Email address input mode. */
    ARKUI_TEXTAREA_TYPE_EMAIL = 5,
} ArkUI_TextAreaType;

/**
 * @brief Enumerates the styles of the Cancel button.
 *
 * @since 12
 */
typedef enum {
    /** The Cancel button is always displayed. */
    ARKUI_CANCELBUTTON_STYLE_CONSTANT = 0,
    /** The Cancel button is always hidden. */
    ARKUI_CANCELBUTTON_STYLE_INVISIBLE,
    /** The Cancel button is displayed when there is text input. */
    ARKUI_CANCELBUTTON_STYLE_INPUT,
} ArkUI_CancelButtonStyle;

/**
 * @brief Enumerates the types of the <b>XComponent</b> component.
 *
 * @since 12
 */
typedef enum {
    /** The custom content of EGL/OpenGL ES and media data is displayed individually on the screen. */
    ARKUI_XCOMPONENT_TYPE_SURFACE = 0,
    /** The custom content of EGL/OpenGL ES and media data is grouped and displayed together with content
     *  of the component.
     */
    ARKUI_XCOMPONENT_TYPE_TEXTURE = 2,
} ArkUI_XComponentType;

/**
 * @brief Enumerates the styles of the progress indicator.
 *
 * @since 12
 */
typedef enum {
    /** Linear style. */
    ARKUI_PROGRESS_TYPE_LINEAR = 0,
    /** Indeterminate ring style. */
    ARKUI_PROGRESS_TYPE_RING,
    /** Eclipse style. */
    ARKUI_PROGRESS_TYPE_ECLIPSE,
    /** Determinate ring style. */
    ARKUI_PROGRESS_TYPE_SCALE_RING,
    /** Capsule style. */
    ARKUI_PROGRESS_TYPE_CAPSULE,
}ArkUI_ProgressType;

/**
 * @brief Enumerates the text decoration types.
 *
 * @since 12
 */
typedef enum {
    /** No text decoration. */
    ARKUI_TEXT_DECORATION_TYPE_NONE = 0,
    /** Line under the text. */
    ARKUI_TEXT_DECORATION_TYPE_UNDERLINE,
    /** Line over the text. */
    ARKUI_TEXT_DECORATION_TYPE_OVERLINE,
    /** Line through the text. */
    ARKUI_TEXT_DECORATION_TYPE_LINE_THROUGH,
} ArkUI_TextDecorationType;

/**
 * @brief Enumerates the text decoration styles.
 *
 * @since 12
 */
typedef enum {
    /** Single solid line. */
    ARKUI_TEXT_DECORATION_STYLE_SOLID = 0,
    /** Double solid line. */
    ARKUI_TEXT_DECORATION_STYLE_DOUBLE,
    /** Dotted line. */
    ARKUI_TEXT_DECORATION_STYLE_DOTTED,
    /** Dashed line. */
    ARKUI_TEXT_DECORATION_STYLE_DASHED,
    /** Wavy line. */
    ARKUI_TEXT_DECORATION_STYLE_WAVY,
} ArkUI_TextDecorationStyle;

/**
 * @brief Enumerates the text cases.
 *
 * @since 12
 */
typedef enum {
    /** The original case of the text is retained. */
    ARKUI_TEXT_CASE_NORMAL = 0,
    /** All letters in the text are in lowercase. */
    ARKUI_TEXT_CASE_LOWER,
    /** All letters in the text are in uppercase. */
    ARKUI_TEXT_CASE_UPPER,
} ArkUI_TextCase;

/**
 * @brief Enumerates the text copy and paste modes.
 *
 * @since 12
 */
typedef enum {
    /** Copy is not allowed. */
    ARKUI_COPY_OPTIONS_NONE = 0,
    /** Intra-application copy is allowed. */
    ARKUI_COPY_OPTIONS_IN_APP,
    /** Intra-device copy is allowed. */
    ARKUI_COPY_OPTIONS_LOCAL_DEVICE,
    /** Cross-device copy is allowed. */
    ARKUI_COPY_OPTIONS_CROSS_DEVICE,
} ArkUI_CopyOptions;

/**
 * @brief Enumerates the shadow types.
 *
 * @since 12
 */
typedef enum {
    /** Color. */
    ARKUI_SHADOW_TYPE_COLOR = 0,
    /** Blur. */
    ARKUI_SHADOW_TYPE_BLUR
} ArkUI_ShadowType;

/**
 * @brief Enumerates the types of the text picker.
 *
 * @since 12
 */
typedef enum {
    /** Single-column text picker. */
    ARKUI_TEXTPICKER_RANGETYPE_SINGLE = 0,
    /** Multi-column text picker. */
    ARKUI_TEXTPICKER_RANGETYPE_MULTI,
    /** Single-column text picker with image resources. */
    ARKUI_TEXTPICKER_RANGETYPE_RANGE_CONTENT,
    /** Interconnected multi-column text picker. */
    ARKUI_TEXTPICKER_RANGETYPE_CASCADE_RANGE_CONTENT,
} ArkUI_TextPickerRangeType;

/**
 * @brief Defines the input structure of the single-column text picker with image resources.
 *
 * @since 12
 */
typedef struct {
    /** Image resource. */
    const char* icon;
    /** Text information. */
    const char* text;
} ARKUI_TextPickerRangeContent;

/**
 * @brief Defines the input structure of the interconnected multi-column text picker.
 *
 * @since 12
 */
typedef struct {
    /** Text information. */
    const char* text;
    /** Interconnected data. */
    const ARKUI_TextPickerRangeContent* children;
    /** Size of the interconnected data array. */
    int32_t size;
} ARKUI_TextPickerCascadeRangeContent;

/**
 * @brief Enumerates the accessibility check box states.
 *
 * @since 12
 */
typedef enum {
    /** The check box is not selected. */
    ARKUI_ACCESSIBILITY_UNCHECKED = 0,
    /** The check box is selected. */
    ARKUI_ACCESSIBILITY_CHECKED,
} ArkUI_AccessibilityCheckedState;

/**
 * @brief Defines the accessibility action types.
 *
 * @since 12
 */
typedef enum {
    /** Tap. */
    ARKUI_ACCESSIBILITY_ACTION_CLICK = 1 << 0,
    /** Long press. */
    ARKUI_ACCESSIBILITY_ACTION_LONG_CLICK = 1 << 1,
    /** Cut. */
    ARKUI_ACCESSIBILITY_ACTION_CUT = 1 << 2,
    /** Copy. */
    ARKUI_ACCESSIBILITY_ACTION_COPY = 1 << 3,
    /** Paste. */
    ARKUI_ACCESSIBILITY_ACTION_PASTE = 1 << 4,
} ArkUI_AccessibilityActionType;

/**
 * @brief Defines the component accessibility state.
 *
 * @since 12
 */
typedef struct ArkUI_AccessibilityState ArkUI_AccessibilityState;

/**
 * @brief Defines the component accessibility value.
 *
 * @since 12
 */
typedef struct ArkUI_AccessibilityValue ArkUI_AccessibilityValue;

/**
 * @brief Enumerates the effects used at the edges of the component when the boundary of the scrollable content is
 *        reached.
 *
 * @since 12
 */
typedef enum {
    /** Spring effect. When at one of the edges, the component can move beyond the bounds based on the initial
     *  speed or through touches, and produces a bounce effect when the user releases their finger. */
    ARKUI_EDGE_EFFECT_SPRING = 0,
    /** Fade effect. When at one of the edges, the component produces a fade effect. */
    ARKUI_EDGE_EFFECT_FADE,
    /** No effect after the scrollbar is moved to the edge. */
    ARKUI_EDGE_EFFECT_NONE,
} ArkUI_EdgeEffect;

/**
 * @brief Enumerates the edges for which the effect takes effect when the boundary of the scrollable content is reached.
 *
 * @since 16
 */
typedef enum {
    /** Start edge. */
    ARKUI_EFFECT_EDGE_START = 1,
    /** End edge. */
    ARKUI_EFFECT_EDGE_END = 2,
} ArkUI_EffectEdge;

/**
 * @brief Enumerates the scroll directions for the <b><Scroll></b> component.
 *
 * @since 12
 */
typedef enum {
    /** Only vertical scrolling is supported. */
    ARKUI_SCROLL_DIRECTION_VERTICAL = 0,
    /** Only horizontal scrolling is supported. */
    ARKUI_SCROLL_DIRECTION_HORIZONTAL,
    /** Scrolling is not allowed. */
    ARKUI_SCROLL_DIRECTION_NONE = 3,
} ArkUI_ScrollDirection;

/**
 * @brief Enumerates the alignment modes of list items when scrolling ends.
 *
 * @since 12
 */
typedef enum {
    /** No alignment. This is the default value. */
    ARKUI_SCROLL_SNAP_ALIGN_NONE = 0,
    /** The first item in the view is aligned at the start of the list. */
    ARKUI_SCROLL_SNAP_ALIGN_START,
    /** The middle items in the view are aligned in the center of the list. */
    ARKUI_SCROLL_SNAP_ALIGN_CENTER,
    /** The last item in the view is aligned at the end of the list. */
    ARKUI_SCROLL_SNAP_ALIGN_END,
} ArkUI_ScrollSnapAlign;

/**
 * @brief Enumerates the scrollbar display modes.
 *
 * @since 12
 */
typedef enum {
    /** Hide. */
    ARKUI_SCROLL_BAR_DISPLAY_MODE_OFF = 0,
    /** Display on demand (displays when the screen is touched and disappears after 2s). */
    ARKUI_SCROLL_BAR_DISPLAY_MODE_AUTO,
    /** Always display. */
    ARKUI_SCROLL_BAR_DISPLAY_MODE_ON,
} ArkUI_ScrollBarDisplayMode;

/**
 * @brief Enumerates the scroll directions.
 *
 * @since 12
 */
typedef enum {
    /** Only vertical scrolling is supported. */
    ARKUI_AXIS_VERTICAL = 0,
    /** Only horizontal scrolling is supported. */
    ARKUI_AXIS_HORIZONTAL,
} ArkUI_Axis;

/**
 * @brief Enumerates the modes for pinning the header to the top or the footer to the bottom.
 *
 * @since 12
 */
typedef enum {
    /** In the list item group, the header is not pinned to the top, and the footer is not pinned to the bottom. */
    ARKUI_STICKY_STYLE_NONE = 0,
    /** In the list item group, the header is pinned to the top, and the footer is not pinned to the bottom. */
    ARKUI_STICKY_STYLE_HEADER = 1,
    /** In the list item group, the footer is pinned to the bottom, and the header is not pinned to the top. */
    ARKUI_STICKY_STYLE_FOOTER = 2,
    /** In the list item group, the footer is pinned to the bottom, and the header is pinned to the top. */
    ARKUI_STICKY_STYLE_BOTH = 3,
} ArkUI_StickyStyle;

/**
 * @brief Enumerates the content clipping modes of scrollable components.
 *
 * @since 16
 */
typedef enum {
    /** Clip to the content area only. */
    ARKUI_CONTENT_CLIP_MODE_CONTENT_ONLY = 0,
    /** Clip to the component's boundary area. */
    ARKUI_CONTENT_CLIP_MODE_BOUNDARY,
    /** Clip to the safe area configured for the component. */
    ARKUI_CONTENT_CLIP_MODE_SAFE_AREA,
} ArkUI_ContentClipMode;

/**
 * @brief Enumerates the layout modes of the <b>WaterFlow</b> component.
 *
 * @since 16
 */
typedef enum {
    /** Layout from top to bottom. In scenarios where column switching occurs, the layout starts from the first water
     *  flow item to the currently displayed water flow item. */
    ARKUI_WATER_FLOW_LAYOUT_MODE_ALWAYS_TOP_DOWN = 0,
    /** Sliding window layout. In scenarios where column switching occurs, only the range of water flow items currently
     * on display is re-laid out. As the user scrolls down with their finger, water flow items that enter the display
     * range from above are subsequently laid out. */
    ARKUI_WATER_FLOW_LAYOUT_MODE_SLIDING_WINDOW,
} ArkUI_WaterFlowLayoutMode;

/**
 * @brief Enumerates the border styles.
 *
 * @since 12
 */
typedef enum {
    /** Solid border. */
    ARKUI_BORDER_STYLE_SOLID = 0,
    /** Dashed border. */
    ARKUI_BORDER_STYLE_DASHED,
    /** Dotted border. */
    ARKUI_BORDER_STYLE_DOTTED,
} ArkUI_BorderStyle;

/**
 * @brief Enumerates the hit test modes.
 *
 * @since 12
 */
typedef enum {
    /** Both the node and its child node respond to the hit test of a touch event, but its sibling node is blocked from
     *  the hit test. */
    ARKUI_HIT_TEST_MODE_DEFAULT = 0,
    /** The node responds to the hit test of a touch event, but its child node and sibling node are blocked from the
     *  hit test. */
    ARKUI_HIT_TEST_MODE_BLOCK,
    /** Both the node and its child node respond to the hit test of a touch event, and its sibling node is also
     * considered during the hit test. */
    ARKUI_HIT_TEST_MODE_TRANSPARENT,
    /** The node does not respond to the hit test of a touch event. */
    ARKUI_HIT_TEST_MODE_NONE
} ArkUI_HitTestMode;

/**
 * @brief Enumerates the shadow styles.
 *
 * @since 12
 */
typedef enum {
    /** Mini shadow. */
    ARKUI_SHADOW_STYLE_OUTER_DEFAULT_XS = 0,
    /** Little shadow. */
    ARKUI_SHADOW_STYLE_OUTER_DEFAULT_SM,
    /** Medium shadow. */
    ARKUI_SHADOW_STYLE_OUTER_DEFAULT_MD,
    /** Large shadow. */
    ARKUI_SHADOW_STYLE_OUTER_DEFAULT_LG,
    /** Floating small shadow. */
    ARKUI_SHADOW_STYLE_OUTER_FLOATING_SM,
    /** Floating medium shadow. */
    ARKUI_SHADOW_STYLE_OUTER_FLOATING_MD,
} ArkUI_ShadowStyle;

/**
 * @brief Enumerates the animation curves.
 *
 * @since 12
 */
typedef enum {
    /** The animation speed keeps unchanged. */
    ARKUI_CURVE_LINEAR = 0,
    /** The animation starts slowly, accelerates, and then slows down towards the end. */
    ARKUI_CURVE_EASE,
    /** The animation starts at a low speed and then picks up speed until the end. */
    ARKUI_CURVE_EASE_IN,
    /** The animation ends at a low speed. */
    ARKUI_CURVE_EASE_OUT,
    /** The animation starts and ends at a low speed. */
    ARKUI_CURVE_EASE_IN_OUT,
    /** The animation uses the standard curve */
    ARKUI_CURVE_FAST_OUT_SLOW_IN,
    /** The animation uses the deceleration curve. */
    ARKUI_CURVE_LINEAR_OUT_SLOW_IN,
    /** The animation uses the acceleration curve. */
    ARKUI_CURVE_FAST_OUT_LINEAR_IN,
    /** The animation uses the extreme deceleration curve. */
    ARKUI_CURVE_EXTREME_DECELERATION,
    /** The animation uses the sharp curve. */
    ARKUI_CURVE_SHARP,
    /** The animation uses the rhythm curve. */
    ARKUI_CURVE_RHYTHM,
    /** The animation uses the smooth curve. */
    ARKUI_CURVE_SMOOTH,
    /** The animation uses the friction curve */
    ARKUI_CURVE_FRICTION,
} ArkUI_AnimationCurve;

/**
 * @brief Enumerates arrow styles of the navigation point indicator.
 *
 * @since 12
 */
typedef enum {
    /** The arrow is not displayed for the navigation point indicator. */
    ARKUI_SWIPER_ARROW_HIDE = 0,
    /** The arrow is displayed for the navigation point indicator. */
    ARKUI_SWIPER_ARROW_SHOW,
    /** The arrow is displayed only when the mouse pointer hovers over the navigation point indicator. */
    ARKUI_SWIPER_ARROW_SHOW_ON_HOVER,
} ArkUI_SwiperArrow;

/**
 * @brief Enumerates the nested scrolling mode of the <b><Swiper></b>component and its parent container.
 *
 * @since 12
 */
typedef enum {
    /** The scrolling is contained within the <b><Swiper></b> component, and no scroll chaining occurs, that is,
     *  the parent container does not scroll when the component scrolling reaches the boundary. */
    ARKUI_SWIPER_NESTED_SRCOLL_SELF_ONLY = 0,
    /** The <b><Swiper></b> component scrolls first, and when it hits the boundary, the parent container scrolls.
     *  When the parent container hits the boundary, its edge effect is displayed. If no edge effect is specified for
     *  the parent container, the edge effect of the child component is displayed instead. */
    ARKUI_SWIPER_NESTED_SRCOLL_SELF_FIRST,
} ArkUI_SwiperNestedScrollMode;

/**
 * @brief Enumerates the page flipping modes using the mouse wheel for the <b>Swiper</b> component.
 *
 * @since 14
 */
typedef enum {
    /** When the mouse wheel is scrolled continuously, multiple pages are flipped, which is determined by the number of
     *  times that mouse events are reported. */
    ARKUI_PAGE_FLIP_MODE_CONTINUOUS = 0,
    /** The system does not respond to other mouse wheel events until the page flipping animation ends. */
    ARKUI_PAGE_FLIP_MODE_SINGLE,
} ArkUI_PageFlipMode;

/**
 * @brief Enumerates the accessibility modes.
 *
 * @since 12
 */
typedef enum {
    /** Whether the component can be identified by the accessibility service is dependent on the component. */
    ARKUI_ACCESSIBILITY_MODE_AUTO = 0,
    /** The component can be identified by the accessibility service. */
    ARKUI_ACCESSIBILITY_MODE_ENABLED,
    /** The component cannot be identified by the accessibility service. */
    ARKUI_ACCESSIBILITY_MODE_DISABLED,
    /** The component and all its child components cannot be identified by the accessibility service. */
    ARKUI_ACCESSIBILITY_MODE_DISABLED_FOR_DESCENDANTS,
} ArkUI_AccessibilityMode;

/**
 * @brief Defines whether copy and paste is allowed for text content.
 *
 * @since 12
 */
typedef enum {
    /** Copy is not allowed. */
    ARKUI_TEXT_COPY_OPTIONS_NONE = 0,
    /** Intra-application copy is allowed. */
    ARKUI_TEXT_COPY_OPTIONS_IN_APP,
    /** Intra-device copy is allowed. */
    ARKUI_TEXT_COPY_OPTIONS_LOCAL_DEVICE,
    /** Cross-device copy is allowed. */
    ARKUI_TEXT_COPY_OPTIONS_CROSS_DEVICE,
} ArkUI_TextCopyOptions;


/**
 * @brief Defines how the adaptive height is determined for the text.
 *
 * @since 12
 */
typedef enum {
    /** Prioritize the <b>maxLines</b> settings. */
    ARKUI_TEXT_HEIGHT_ADAPTIVE_POLICY_MAX_LINES_FIRST = 0,
    /** Prioritize the <b>minFontSize</b> settings. */
    ARKUI_TEXT_HEIGHT_ADAPTIVE_POLICY_MIN_FONT_SIZE_FIRST,
    /** Prioritize the layout constraint settings in terms of height. */
    ARKUI_TEXT_HEIGHT_ADAPTIVE_POLICY_LAYOUT_CONSTRAINT_FIRST,
} ArkUI_TextHeightAdaptivePolicy;


/**
 * @brief Defines nested scrolling options.
 *
 * @since 12
 */
typedef enum {
    /** The scrolling is contained within the component, and no scroll chaining occurs, that is, the parent component
     * does not scroll when the component scrolling reaches the boundary. */
    ARKUI_SCROLL_NESTED_MODE_SELF_ONLY = 0,
    /** The component scrolls first, and when it hits the boundary, the parent component scrolls.
    /** When the parent component hits the boundary, its edge effect is displayed. If no edge
     *  effect is specified for the parent component, the edge effect of the child component is displayed instead. */
    ARKUI_SCROLL_NESTED_MODE_SELF_FIRST,
    /** The parent component scrolls first, and when it hits the boundary, the component scrolls.
     *  When the component hits the boundary, its edge effect is displayed. If no edge effect is specified for the
     *  component, the edge effect of the parent component is displayed instead. */
    ARKUI_SCROLL_NESTED_MODE_PARENT_FIRST,
    /** The component and its parent component scroll at the same time. When both the component and its parent component
     *  hit the boundary, the edge effect of the component is displayed. If no edge effect is specified for the
     *  component, the edge effect of the parent component is displayed instead. */
    ARKUI_SCROLL_NESTED_MODE_PARALLEL,
} ArkUI_ScrollNestedMode;


/**
 * @brief Defines the edge to which the component scrolls.
 *
 * @since 12
 */
typedef enum {
    /** Top edge in the vertical direction. */
    ARKUI_SCROLL_EDGE_TOP = 0,
    /** Bottom edge in the vertical direction. */
    ARKUI_SCROLL_EDGE_BOTTOM,
    /** Start position in the horizontal direction. */
    ARKUI_SCROLL_EDGE_START,
    /** End position in the horizontal direction. */
    ARKUI_SCROLL_EDGE_END,
} ArkUI_ScrollEdge;

/**
 * @brief Defines how the item to scroll to is aligned with the container.
 *
 * @since 12
 */
typedef enum {
    /** The start edge of the item is flush with the start edge of the container. */
    ARKUI_SCROLL_ALIGNMENT_START = 0,
    /** The list item is centered along the main axis of the container. */
    ARKUI_SCROLL_ALIGNMENT_CENTER,
    /** The end edge of the item is flush with the end edge of the container. */
    ARKUI_SCROLL_ALIGNMENT_END,
    /** The item is automatically aligned. If the item is fully contained within the display area, no adjustment is
     *  performed. Otherwise, the item is aligned so that its start or end edge is flush with the start or end edge of
     *  the container, whichever requires a shorter scrolling distance. */
    ARKUI_SCROLL_ALIGNMENT_AUTO,
} ArkUI_ScrollAlignment;

/**
 * @brief Defines the current scrolling state.
 *
 * @since 12
 */
typedef enum {
    /** Idle. The container enters this state when an API in the controller is used to scroll the container or when
     *  the scrollbar is dragged. */
    ARKUI_SCROLL_STATE_IDLE = 0,
    /** Scrolling. The container enters this state when the user drags the container to scroll. */
    ARKUI_SCROLL_STATE_SCROLL,
    /** Inertial scrolling. The container enters this state when inertial scrolling occurs or when the container bounces
     *  back after being released from a fling. */
    ARKUI_SCROLL_STATE_FLING,
} ArkUI_ScrollState;

/**
 * @brief Enumerates the types of the slider in the block direction.
 *
 * @since 12
 */
typedef enum {
    /** Round slider. */
    ARKUI_SLIDER_BLOCK_STYLE_DEFAULT = 0,
    /** Slider with an image background. */
    ARKUI_SLIDER_BLOCK_STYLE_IMAGE,
    /** Slider in a custom shape. */
    ARKUI_SLIDER_BLOCK_STYLE_SHAPE,
} ArkUI_SliderBlockStyle;

/**
 * @brief Enumerates the scroll directions of the slider.
 *
 * @since 12
 */
typedef enum {
    /** Vertical direction. */
    ARKUI_SLIDER_DIRECTION_VERTICAL = 0,
    /** Horizontal direction. */
    ARKUI_SLIDER_DIRECTION_HORIZONTAL,
} ArkUI_SliderDirection;

/**
 * @brief Enumerates the slider styles.
 *
 * @since 12
 */
typedef enum {
    /** The thumb is on the track. */
    ARKUI_SLIDER_STYLE_OUT_SET = 0,
    /** The thumb is in the track. */
    ARKUI_SLIDER_STYLE_IN_SET,
    /** There is no thumb. */
    ARKUI_SLIDER_STYLE_NONE,
} ArkUI_SliderStyle;

/**
 * @brief Enumerates the shapes of the check box.
 *
 * @since 12
 */
typedef enum {
    /** Circle. */
    ArkUI_CHECKBOX_SHAPE_CIRCLE = 0,
    /** Rounded square. */
    ArkUI_CHECKBOX_SHAPE_ROUNDED_SQUARE,
} ArkUI_CheckboxShape;

/**
 * @brief Enumerates the animation playback modes.
 *
 * @since 12
 */
typedef enum {
    /** The animation is played forwards. */
    ARKUI_ANIMATION_PLAY_MODE_NORMAL = 0,
    /** The animation is played reversely. */
    ARKUI_ANIMATION_PLAY_MODE_REVERSE,
    /** The animation is played normally for an odd number of times (1, 3, 5...) and reversely for an even number
     *  of times (2, 4, 6...). */
    ARKUI_ANIMATION_PLAY_MODE_ALTERNATE,
    /** The animation is played reversely for an odd number of times (1, 3, 5...) and normally for an even number
     *  of times (2, 4, 6...). */
    ARKUI_ANIMATION_PLAY_MODE_ALTERNATE_REVERSE,
} ArkUI_AnimationPlayMode;

/**
 * @brief Defines the image size.
 *
 * @since 12
 */
typedef enum {
    /** The original image aspect ratio is retained. */
    ARKUI_IMAGE_SIZE_AUTO = 0,
    /** Default value. The image is scaled with its aspect ratio retained for both sides to be greater than or equal
     *  to the display boundaries. */
    ARKUI_IMAGE_SIZE_COVER,
    /** The image is scaled with its aspect ratio retained for the content to be completely displayed within the display
     *  boundaries. */
    ARKUI_IMAGE_SIZE_CONTAIN,
} ArkUI_ImageSize;

/**
 * @brief Enumerates the adaptive color modes.
 *
 * @since 12
 */
typedef enum {
    /** Adaptive color mode is not used. */
    ARKUI_ADAPTIVE_COLOR_DEFAULT = 0,
    /** Adaptive color mode is used. */
    ARKUI_ADAPTIVE_COLOR_AVERAGE,
} ArkUI_AdaptiveColor;

/**
 * @brief Enumerates the color modes.
 *
 * @since 12
 */
typedef enum {
    /** Following the system color mode. */
    ARKUI_COLOR_MODE_SYSTEM = 0,
    /** Light color mode. */
    ARKUI_COLOR_MODE_LIGHT,
    /** Dark color mode. */
    ARKUI_COLOR_MODE_DARK,
} ArkUI_ColorMode;

/**
 * @brief Enumerates the system color modes.
 *
 * @since 12
 */
typedef enum {
    /** Light mode. */
    ARKUI_SYSTEM_COLOR_MODE_LIGHT = 0,
    /** Dark mode. */
    ARKUI_SYSTEM_COLOR_MODE_DARK,
} ArkUI_SystemColorMode;

/**
 * @brief Enumerates the blur styles.
 *
 * @since 12
 */
typedef enum {
    /** Thin material. */
    ARKUI_BLUR_STYLE_THIN = 0,
    /** Regular material. */
    ARKUI_BLUR_STYLE_REGULAR,
    /** Thick material. */
    ARKUI_BLUR_STYLE_THICK,
    /** Material that creates the minimum depth of field effect. */
    ARKUI_BLUR_STYLE_BACKGROUND_THIN,
    /** Material that creates a medium shallow depth of field effect. */
    ARKUI_BLUR_STYLE_BACKGROUND_REGULAR,
    /** Material that creates a high shallow depth of field effect. */
    ARKUI_BLUR_STYLE_BACKGROUND_THICK,
    /** Material that creates the maximum depth of field effect. */
    ARKUI_BLUR_STYLE_BACKGROUND_ULTRA_THICK,
    /** No blur. */
    ARKUI_BLUR_STYLE_NONE,
    /** Component ultra-thin material. */
    ARKUI_BLUR_STYLE_COMPONENT_ULTRA_THIN,
    /** Component thin material. */
    ARKUI_BLUR_STYLE_COMPONENT_THIN,
    /** Component regular material. */
    ARKUI_BLUR_STYLE_COMPONENT_REGULAR,
    /** Component thick material. */
    ARKUI_BLUR_STYLE_COMPONENT_THICK,
    /** Component ultra-thick material. */
    ARKUI_BLUR_STYLE_COMPONENT_ULTRA_THICK,
} ArkUI_BlurStyle;

/**
 * @brief Enumerates the vertical alignment modes.
 *
 * @since 12
 */
typedef enum {
    /** Top aligned. */
    ARKUI_VERTICAL_ALIGNMENT_TOP = 0,
    /** Center aligned. This is the default alignment mode. */
    ARKUI_VERTICAL_ALIGNMENT_CENTER,
    /** Bottom aligned. */
    ARKUI_VERTICAL_ALIGNMENT_BOTTOM,
} ArkUI_VerticalAlignment;

/**
 * @brief Enumerates the alignment mode in the horizontal direction.
 *
 * @since 12
 */
typedef enum {
    /** Aligned with the start edge in the same direction as the language in use. */
    ARKUI_HORIZONTAL_ALIGNMENT_START = 0,
    /** Center aligned. This is the default alignment mode. */
    ARKUI_HORIZONTAL_ALIGNMENT_CENTER,
    /** Aligned with the end edge in the same direction as the language in use. */
    ARKUI_HORIZONTAL_ALIGNMENT_END,
} ArkUI_HorizontalAlignment;

/**
 * @brief Enumerates the display modes when the text is too long.
 *
 * @since 12
 */
typedef enum {
    /** Extra-long text is not clipped. */
    ARKUI_TEXT_OVERFLOW_NONE = 0,
    /** Extra-long text is clipped. */
    ARKUI_TEXT_OVERFLOW_CLIP,
    /** An ellipsis (...) is used to represent text overflow. */
    ARKUI_TEXT_OVERFLOW_ELLIPSIS,
    /** Text continuously scrolls when text overflow occurs. */
    ARKUI_TEXT_OVERFLOW_MARQUEE,
} ArkUI_TextOverflow;

/**
 * @brief Enumerates the alignment mode of the image with the text.
 *
 * @since 12
 */
typedef enum {
    /** The image is bottom aligned with the text baseline. */
    ARKUI_IMAGE_SPAN_ALIGNMENT_BASELINE = 0,
    /** The image is bottom aligned with the text. */
    ARKUI_IMAGE_SPAN_ALIGNMENT_BOTTOM,
    /** The image is centered aligned with the text. */
    ARKUI_IMAGE_SPAN_ALIGNMENT_CENTER,
    /** The image is top aligned with the text. */
    ARKUI_IMAGE_SPAN_ALIGNMENT_TOP,
} ArkUI_ImageSpanAlignment;

/**
 * @brief Defines how the image is resized to fit its container.
 *ImageSpanAlignment
 * @since 12
 */
typedef enum {
    /** The image is scaled with its aspect ratio retained for the content to be completely displayed within the
     *  display boundaries. */
    ARKUI_OBJECT_FIT_CONTAIN = 0,
    /** The image is scaled with its aspect ratio retained for both sides to be greater than or equal to the
     *  display boundaries. */
    ARKUI_OBJECT_FIT_COVER,
    /** The image is scaled automatically to fit the display area. */
    ARKUI_OBJECT_FIT_AUTO,
    /** The image is scaled to fill the display area, and its aspect ratio is not retained. */
    ARKUI_OBJECT_FIT_FILL,
    /** The image content is displayed with its aspect ratio retained. The size is smaller than or equal to the
     *  original size. */
    ARKUI_OBJECT_FIT_SCALE_DOWN,
    /** The original size is retained. */
    ARKUI_OBJECT_FIT_NONE,
    /** Not resized, the image is aligned with the start edge of the top of the container. */
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_TOP_START,
    /** Not resized, the image is horizontally centered at the top of the container. */
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_TOP,
    /** Not resized, the image is aligned with the end edge at the top of the container. */
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_TOP_END,
    /** Not resized, the image is vertically centered on the start edge of the container. */
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_START,
    /** Not resized, the image is horizontally and vertically centered in the container. */
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_CENTER,
    /** Not resized, the image is vertically centered on the end edge of the container. */
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_END,
    /** Not resized, the image is aligned with the start edge at the bottom of the container. */
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_BOTTOM_START,
    /** Not resized, the image is horizontally centered at the bottom of the container. */
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_BOTTOM,
    /** Not resized, the image is aligned with the end edge at the bottom of the container. */
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_BOTTOM_END,
} ArkUI_ObjectFit;

/**
 * @brief Enumerates the image interpolation effect.
 *
 * @since 12
 */
typedef enum {
    /** No image interpolation. */
    ARKUI_IMAGE_INTERPOLATION_NONE = 0,
    /** Low quality interpolation. */
    ARKUI_IMAGE_INTERPOLATION_LOW,
    /** Medium quality interpolation. */
    ARKUI_IMAGE_INTERPOLATION_MEDIUM,
    /** High quality interpolation. This mode produces scaled images of the highest possible quality. */
    ARKUI_IMAGE_INTERPOLATION_HIGH,
} ArkUI_ImageInterpolation;


/**
 * @brief Enumerates the blend modes.
 *
 * @since 12
 */
typedef enum {
    /** The top image is superimposed on the bottom image without any blending. */
    ARKUI_BLEND_MODE_NONE = 0,
    /** The target pixels covered by the source pixels are erased by being turned to completely transparent. */
    ARKUI_BLEND_MODE_CLEAR,
    /** r = s: Only the source pixels are displayed. */
    ARKUI_BLEND_MODE_SRC,
    /** r = d: Only the target pixels are displayed. */
    ARKUI_BLEND_MODE_DST,
    /** r = s + (1 - sa) * d: The source pixels are blended based on opacity and cover the target pixels. */
    ARKUI_BLEND_MODE_SRC_OVER,
    /** r = d + (1 - da) * s: The target pixels are blended based on opacity and cover on the source pixels. */
    ARKUI_BLEND_MODE_DST_OVER,
    /** r = s * da: Only the part of the source pixels that overlap with the target pixels is displayed. */
    ARKUI_BLEND_MODE_SRC_IN,
    /** r = d * sa: Only the part of the target pixels that overlap with the source pixels is displayed. */
    ARKUI_BLEND_MODE_DST_IN,
    /** r = s * (1 - da): Only the part of the source pixels that do not overlap with the target pixels is displayed. */
    ARKUI_BLEND_MODE_SRC_OUT,
    /** r = d * (1 - sa): Only the part of the target pixels that do not overlap with the source pixels is displayed. */
    ARKUI_BLEND_MODE_DST_OUT,
    /** r = s * da + d * (1 - sa): The part of the source pixels that overlap with the target pixels is displayed and
     *  the part of the target pixels that do not overlap with the source pixels are displayed.
     */
    ARKUI_BLEND_MODE_SRC_ATOP,
    /** r = d * sa + s * (1 - da): The part of the target pixels that overlap with the source pixels and the part of
     *  the source pixels that do not overlap with the target pixels are displayed.
     */
    ARKUI_BLEND_MODE_DST_ATOP,
    /** r = s * (1 - da) + d * (1 - sa): Only the non-overlapping part between the source pixels and the target pixels
     * is displayed. */
    ARKUI_BLEND_MODE_XOR,
    /** r = min(s + d, 1): New pixels resulting from adding the source pixels to the target pixels are displayed. */
    ARKUI_BLEND_MODE_PLUS,
    /** r = s * d: New pixels resulting from multiplying the source pixels with the target pixels are displayed. */
    ARKUI_BLEND_MODE_MODULATE,
    /** r = s + d - s * d: Pixels are blended by adding the source pixels to the target pixels and subtracting the
     *  product of their multiplication. */
    ARKUI_BLEND_MODE_SCREEN,
    /** The MULTIPLY or SCREEN mode is used based on the target pixels. */
    ARKUI_BLEND_MODE_OVERLAY,
    /** rc = s + d - max(s * da, d * sa), ra = kSrcOver: When two colors overlap, whichever is darker is used. */
    ARKUI_BLEND_MODE_DARKEN,
    /** rc = s + d - min(s * da, d * sa), ra =
       kSrcOver: The final pixels are composed of the lightest values of pixels. */
    ARKUI_BLEND_MODE_LIGHTEN,
    /** The colors of the target pixels are lightened to reflect the source pixels. */
    ARKUI_BLEND_MODE_COLOR_DODGE,
    /** The colors of the target pixels are darkened to reflect the source pixels. */
    ARKUI_BLEND_MODE_COLOR_BURN,
    /** The MULTIPLY or SCREEN mode is used, depending on the source pixels. */
    ARKUI_BLEND_MODE_HARD_LIGHT,
    /** The LIGHTEN or DARKEN mode is used, depending on the source pixels. */
    ARKUI_BLEND_MODE_SOFT_LIGHT,
    /** rc = s + d - 2 * (min(s * da, d * sa)), ra =
       kSrcOver: The final pixel is the result of subtracting the darker of the two pixels (source and target) from
       the lighter one. */
    ARKUI_BLEND_MODE_DIFFERENCE,
    /** rc = s + d - two(s * d), ra = kSrcOver: The final pixel is similar to <b>DIFFERENCE</b>, but with less contrast.
     */
    ARKUI_BLEND_MODE_EXCLUSION,
    /** r = s * (1 - da) + d * (1 - sa) + s * d: The final pixel is the result of multiplying the source pixel
     *  by the target pixel.	 */
    ARKUI_BLEND_MODE_MULTIPLY,
    /** The resultant image is created with the luminance and saturation of the source image and the hue of the target
     *  image. */
    ARKUI_BLEND_MODE_HUE,
    /** The resultant image is created with the luminance and hue of the target image and the saturation of the source
     *  image. */
    ARKUI_BLEND_MODE_SATURATION,
    /** The resultant image is created with the saturation and hue of the source image and the luminance of the target
     *  image. */
    ARKUI_BLEND_MODE_COLOR,
    /** The resultant image is created with the saturation and hue of the target image and the luminance of the source
     *  image. */
    ARKUI_BLEND_MODE_LUMINOSITY,
} ArkUI_BlendMode;

/**
 * @brief Enumerates the modes in which components are laid out along the main axis of the container.
 *
 * @since 12
 */
typedef enum {
    /** Components are arranged from left to right. */
    ARKUI_DIRECTION_LTR = 0,
    /** Components are arranged from right to left. */
    ARKUI_DIRECTION_RTL,
    /** The default layout direction is used. */
    ARKUI_DIRECTION_AUTO = 3,
} ArkUI_Direction;

/**
 * @brief Enumerates the modes in which components are laid out along the cross axis of the container.
 *
 * @since 12
 */
typedef enum {
    /** The default configuration in the container is used. */
    ARKUI_ITEM_ALIGNMENT_AUTO = 0,
    /** The items in the container are aligned with the cross-start edge. */
    ARKUI_ITEM_ALIGNMENT_START,
    /** The items in the container are centered along the cross axis. */
    ARKUI_ITEM_ALIGNMENT_CENTER,
    /** The items in the container are aligned with the cross-end edge. */
    ARKUI_ITEM_ALIGNMENT_END,
    /** The items in the container are stretched and padded along the cross axis. */
    ARKUI_ITEM_ALIGNMENT_STRETCH,
    /** The items in the container are aligned in such a manner that their text baselines are aligned along the
     *  cross axis. */
    ARKUI_ITEM_ALIGNMENT_BASELINE,
} ArkUI_ItemAlignment;

/**
 * @brief Enumerates the foreground colors.
 *
 * @since 12
 */
typedef enum {
    /** The foreground colors are the inverse of the component background colors. */
    ARKUI_COLOR_STRATEGY_INVERT = 0,
    /** The shadow colors of the component are the average color obtained from the component background shadow area. */
    ARKUI_COLOR_STRATEGY_AVERAGE,
    /** The shadow colors of the component are the primary color obtained from the component background shadow area. */
    ARKUI_COLOR_STRATEGY_PRIMARY,
} ArkUI_ColorStrategy;

/**
 * @brief Enumerates the vertical alignment modes.
 *
 * @since 12
 */
typedef enum {
    /** The child components are aligned with the start edge of the main axis. */
    ARKUI_FLEX_ALIGNMENT_START = 1,
    /** The child components are aligned in the center of the main axis. */
    ARKUI_FLEX_ALIGNMENT_CENTER = 2,
    /** The child components are aligned with the end edge of the main axis. */
    ARKUI_FLEX_ALIGNMENT_END = 3,
    /** The child components are evenly distributed along the main axis. The space between any two adjacent components
     *  is the same. The first component is aligned with the main-start, and the last component is aligned with
     *  the main-end. */
    ARKUI_FLEX_ALIGNMENT_SPACE_BETWEEN = 6,
    /** The child components are evenly distributed along the main axis. The space between any two adjacent components
     *  is the same. The space between the first component and main-start, and that between the last component and
     *  cross-main are both half the size of the space between two adjacent components. */
    ARKUI_FLEX_ALIGNMENT_SPACE_AROUND = 7,
    /** The child components are evenly distributed along the main axis. The space between the first component
     *  and main-start, the space between the last component and main-end, and the space between any two adjacent
     *  components are the same. */
    ARKUI_FLEX_ALIGNMENT_SPACE_EVENLY = 8,
} ArkUI_FlexAlignment;

/**
 * @brief Enumerates the directions of the main axis in the flex container.
 *
 * @since 12
 */
typedef enum {
    /** The child components are arranged in the same direction as the main axis runs along the rows. */
    ARKUI_FLEX_DIRECTION_ROW = 0,
    /** The child components are arranged in the same direction as the main axis runs down the columns. */
    ARKUI_FLEX_DIRECTION_COLUMN,
    /** The child components are arranged opposite to the <b>ROW</b> direction. */
    ARKUI_FLEX_DIRECTION_ROW_REVERSE,
    /** The child components are arranged opposite to the <b>COLUMN</b> direction. */
    ARKUI_FLEX_DIRECTION_COLUMN_REVERSE,
} ArkUI_FlexDirection;

/**
 * @brief Defines whether the flex container has a single line or multiple lines.
 *
 * @since 12
 */
typedef enum {
    /** The child components in the flex container are arranged in a single line, and they cannot overflow. */
    ARKUI_FLEX_WRAP_NO_WRAP = 0,
    /** The child components in the flex container are arranged in multiple lines, and they may overflow. */
    ARKUI_FLEX_WRAP_WRAP,
    /** The child components in the flex container are reversely arranged in multiple lines, and they may overflow. */
    ARKUI_FLEX_WRAP_WRAP_REVERSE,
} ArkUI_FlexWrap;

/**
 * @brief Enumerates the visibility values.
 *
 * @since 12
 */
typedef enum {
    /** The component is visible. */
    ARKUI_VISIBILITY_VISIBLE = 0,
    /** The component is hidden, and a placeholder is used for it in the layout. */
    ARKUI_VISIBILITY_HIDDEN,
    /** The component is hidden. It is not involved in the layout, and no placeholder is used for it. */
    ARKUI_VISIBILITY_NONE,
} ArkUI_Visibility;

/**
 * @brief Enumerates the alignment modes between the calendar picker and the entry component.
 *
 * @since 12
 */
typedef enum {
    /** Left aligned. */
    ARKUI_CALENDAR_ALIGNMENT_START = 0,
    /** Center aligned. */
    ARKUI_CALENDAR_ALIGNMENT_CENTER,
    /** Right aligned. */
    ARKUI_CALENDAR_ALIGNMENT_END,
} ArkUI_CalendarAlignment;

/**
 * @brief Enumerates the mask types.
 *
 * @since 12
 */
typedef enum {
    /** Rectangle. */
    ARKUI_MASK_TYPE_RECTANGLE = 0,
    /** Circle. */
    ARKUI_MASK_TYPE_CIRCLE,
    /** Ellipse. */
    ARKUI_MASK_TYPE_ELLIPSE,
    /** Path. */
    ARKUI_MASK_TYPE_PATH,
    /** Progress indicator. */
    ARKUI_MASK_TYPE_PROGRESS,
} ArkUI_MaskType;

/**
 * @brief Enumerates the clipping region types.
 *
 * @since 12
 */
typedef enum {
    /** Rectangle. */
    ARKUI_CLIP_TYPE_RECTANGLE = 0,
    /** Circle. */
    ARKUI_CLIP_TYPE_CIRCLE,
    /** Ellipse. */
    ARKUI_CLIP_TYPE_ELLIPSE,
    /** Path. */
    ARKUI_CLIP_TYPE_PATH,
} ArkUI_ClipType;

/**
 * @brief Defines the gradient color stop structure.
 *
 * @since 12
 */
typedef struct {
    /** Color array. */
    const uint32_t* colors;
    /** Position array. */
    float* stops;
    /** Length array. */
    int size;
} ArkUI_ColorStop;

/**
 * @brief Enumerates the custom shapes.
 *
 * @since 12
 */
typedef enum {
    /** Rectangle. */
    ARKUI_SHAPE_TYPE_RECTANGLE = 0,
    /** Circle. */
    ARKUI_SHAPE_TYPE_CIRCLE,
    /** Ellipse. */
    ARKUI_SHAPE_TYPE_ELLIPSE,
    /** Path. */
    ARKUI_SHAPE_TYPE_PATH,
} ArkUI_ShapeType;

/**
 * @brief Enumerates the gradient directions.
 *
 * @since 12
 */
typedef enum {
    /** From right to left. */
    ARKUI_LINEAR_GRADIENT_DIRECTION_LEFT = 0,
    /** From bottom to top. */
    ARKUI_LINEAR_GRADIENT_DIRECTION_TOP,
    /** From left to right. */
    ARKUI_LINEAR_GRADIENT_DIRECTION_RIGHT,
    /** From top to bottom. */
    ARKUI_LINEAR_GRADIENT_DIRECTION_BOTTOM,
    /** From lower right to upper left. */
    ARKUI_LINEAR_GRADIENT_DIRECTION_LEFT_TOP,
    /** From upper right to lower left. */
    ARKUI_LINEAR_GRADIENT_DIRECTION_LEFT_BOTTOM,
    /** From lower left to upper right. */
    ARKUI_LINEAR_GRADIENT_DIRECTION_RIGHT_TOP,
    /** From upper left to lower right. */
    ARKUI_LINEAR_GRADIENT_DIRECTION_RIGHT_BOTTOM,
    /** No gradient. */
    ARKUI_LINEAR_GRADIENT_DIRECTION_NONE,
    /** Custom direction. */
    ARKUI_LINEAR_GRADIENT_DIRECTION_CUSTOM,
} ArkUI_LinearGradientDirection;

/**
 * @brief Enumerates the word break rules.
 *
 * @since 12
 */
typedef enum {
    /** Word breaks can occur between any two characters for Chinese, Japanese, and Korean (CJK) text, but can occur
     *  only at a space character for non-CJK text (such as English). */
    ARKUI_WORD_BREAK_NORMAL = 0,
    /** Word breaks can occur between any two characters for non-CJK text. CJK text behavior is the same as for
     *  <b>NORMAL</b>. */
    ARKUI_WORD_BREAK_BREAK_ALL,
    /** This option has the same effect as <b>BREAK_ALL</b> for non-CJK text, except that if it preferentially wraps
     *  lines at appropriate characters (for example, spaces) whenever possible.
        CJK text behavior is the same as for <b>NORMAL</b>. */
    ARKUI_WORD_BREAK_BREAK_WORD,
}ArkUI_WordBreak;

/**
 * @brief Enumerates the ellipsis positions.
 *
 * @since 12
 */
typedef enum {
    /** An ellipsis is used at the start of the line of text. */
    ARKUI_ELLIPSIS_MODE_START = 0,
    /** An ellipsis is used at the center of the line of text. */
    ARKUI_ELLIPSIS_MODE_CENTER,
    /** An ellipsis is used at the end of the line of text. */
    ARKUI_ELLIPSIS_MODE_END,
}ArkUI_EllipsisMode;

/**
 * @brief Enumerates the image rendering modes.
 *
 * @since 12
 */
typedef enum {
    /** Render image pixels as they are in the original source image. */
    ARKUI_IMAGE_RENDER_MODE_ORIGINAL = 0,
    /** Render image pixels to create a monochrome template image. */
    ARKUI_IMAGE_RENDER_MODE_TEMPLATE,
}ArkUI_ImageRenderMode;

/**
 * @brief Enumerates the slide-in and slide-out positions of the component from the screen edge during transition.
 *
 * @since 12
 */
typedef enum {
    /** Top edge of the window. */
    ARKUI_TRANSITION_EDGE_TOP = 0,
    /** Bottom edge of the window. */
    ARKUI_TRANSITION_EDGE_BOTTOM,
    /** Left edge of the window. */
    ARKUI_TRANSITION_EDGE_START,
    /** Right edge of the window. */
    ARKUI_TRANSITION_EDGE_END,
}ArkUI_TransitionEdge;

/**
 * @brief Enumerates the animation onFinish callback types.
 *
 * @since 12
 */
typedef enum {
    /** The callback is invoked when the entire animation is removed once it has finished. */
    ARKUI_FINISH_CALLBACK_REMOVED = 0,
    /** The callback is invoked when the animation logically enters the falling state, though it may still be in its
      * long tail state. */
    ARKUI_FINISH_CALLBACK_LOGICALLY,
} ArkUI_FinishCallbackType;

/**
 * @brief Enumerates the alignment modes of items along the cross axis.
  *
 * @since 12
 */
typedef enum {
     /** The list items are packed toward the start edge of the list container along the cross axis. */
    ARKUI_LIST_ITEM_ALIGNMENT_START = 0,
    /** The list items are centered in the list container along the cross axis. */
    ARKUI_LIST_ITEM_ALIGNMENT_CENTER,
    /** The list items are packed toward the end edge of the list container along the cross axis. */
    ARKUI_LIST_ITEM_ALIGNMENT_END,
} ArkUI_ListItemAlignment;

/**
 * @brief Defines how the specified blend mode is applied.
 *
 * @since 12
 */
typedef enum {
    /** The content of the view is blended in sequence on the target image. */
    BLEND_APPLY_TYPE_FAST = 0,
    /** The content of the component and its child components are drawn on the offscreen canvas, and then blended with
    /*  the existing content on the canvas. */
    BLEND_APPLY_TYPE_OFFSCREEN,
} ArkUI_BlendApplyType;

/**
 * @brief Defines a mask area.
 *
 * @since 12
 */
typedef struct {
    /** X coordinate of the mask area. */
    float x;
    /** Y coordinate of the mask area. */
    float y;
    /** Width of the mask area. */
    float width;
    /** Height of the mask area. */
    float height;
} ArkUI_Rect;

/**
 * @brief Describes the width and height of a component.
 *
 * @since 12
 */
typedef struct {
    /** Width, in px. */
    int32_t width;
    /** Height, in px. */
    int32_t height;
} ArkUI_IntSize;

/**
 * @brief Describes the position of a component.
 *
 * @since 12
 */
typedef struct {
    /** Horizontal coordinate, in px. */
    int32_t x;
    /** Vertical coordinate, in px. */
    int32_t y;
} ArkUI_IntOffset;

/**
 * @brief Describes the margins of a component.
 *
 * @since 12
 */
typedef struct {
    /** Top margin, in vp. */
    float top;
    /** Right margin, in vp. */
    float right;
    /** Bottom margin, in vp. */
    float bottom;
    /** Left margin, in vp. */
    float left;
} ArkUI_Margin;

/**
 * @brief Enumerates the component units.
 *
 * @since 12
 */
typedef enum {
    /** Default, which is fp for fonts and vp for non-fonts. */
    ARKUI_LENGTH_METRIC_UNIT_DEFAULT = -1,
    /** px. */
    ARKUI_LENGTH_METRIC_UNIT_PX = 0,
    /** vp. */
    ARKUI_LENGTH_METRIC_UNIT_VP,
    /** fp. */
    ARKUI_LENGTH_METRIC_UNIT_FP
} ArkUI_LengthMetricUnit;

/**
 * @brief Enumerates the autofill types.
 *
 * @since 12
 */
typedef enum {
    /** Username. Password Vault, when enabled, can automatically save and fill in usernames. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_USER_NAME = 0,
    /** Password. Password Vault, when enabled, can automatically save and fill in passwords. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_PASSWORD,
    /** New password. Password Vault, when enabled, can automatically generate a new password. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_NEW_PASSWORD,
    /** Full street address. The scenario-based autofill feature, when enabled, can automatically save and fill in full
     *  street addresses. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_FULL_STREET_ADDRESS,
    /** House number. The scenario-based autofill feature, when enabled, can automatically save and fill in house
     *  numbers. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_HOUSE_NUMBER,
    /** District and county. The scenario-based autofill feature, when enabled, can automatically save and fill in
     *  districts and counties. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_DISTRICT_ADDRESS,
    /** City. The scenario-based autofill feature, when enabled, can automatically save and fill in cities. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_CITY_ADDRESS,
    /** Province. The scenario-based autofill feature, when enabled, can automatically save and fill in provinces. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_PROVINCE_ADDRESS,
    /** Country. The scenario-based autofill feature, when enabled, can automatically save and fill in countries. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_COUNTRY_ADDRESS,
    /** Full name. The scenario-based autofill feature, when enabled, can automatically save and fill in full names. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_PERSON_FULL_NAME,
    /** Last name. The scenario-based autofill feature, when enabled, can automatically save and fill in last names. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_PERSON_LAST_NAME,
    /** First name. The scenario-based autofill feature, when enabled, can automatically save and fill in first names.
     */
    ARKUI_TEXTINPUT_CONTENT_TYPE_PERSON_FIRST_NAME,
    /** Phone number. The scenario-based autofill feature, when enabled, can automatically save and fill in phone
     *  numbers. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_PHONE_NUMBER,
    /** Country code. The scenario-based autofill feature, when enabled, can automatically save and fill in country
     *  codes. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_PHONE_COUNTRY_CODE,
    /** Phone number with country code. The scenario-based autofill feature, when enabled, can automatically save and
     *  fill in phone numbers with country codes. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_FULL_PHONE_NUMBER,
    /** Email address. The scenario-based autofill feature, when enabled, can automatically save and fill in email
     *  addresses. */
    ARKUI_TEXTINPUT_CONTENT_EMAIL_ADDRESS,
    /** Bank card number. The scenario-based autofill feature, when enabled, can automatically save and fill in bank
     *  card numbers. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_BANK_CARD_NUMBER,
    /** ID card number. The scenario-based autofill feature, when enabled, can automatically save and fill in ID card
     *  numbers. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_ID_CARD_NUMBER,
    /** Nickname. The scenario-based autofill feature, when enabled, can automatically save and fill in nicknames. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_NICKNAME,
    /** Address information without street address. The scenario-based autofill feature, when enabled, can automatically
     *  save and fill in address information without street addresses. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_DETAIL_INFO_WITHOUT_STREET,
    /** Standard address. The scenario-based autofill feature, when enabled, can automatically save and fill in standard
     *  addresses. */
    ARKUI_TEXTINPUT_CONTENT_TYPE_FORMAT_ADDRESS,
}ArkUI_TextInputContentType;

/**
 * @brief Defines the direction of a barrier.
 *
 * @since 12
 */
typedef enum {
    /** The barrier is on the left side of all the referenced components specified by referencedId. */
    ARKUI_BARRIER_DIRECTION_START = 0,
    /** The barrier is on the right side of all the referenced components specified by referencedId. */
    ARKUI_BARRIER_DIRECTION_END,
    /** The barrier is at the top of all the referenced components specified by referencedId. */
    ARKUI_BARRIER_DIRECTION_TOP,
    /** The barrier is at the bottom of all the referenced components specified by referencedId. */
    ARKUI_BARRIER_DIRECTION_BOTTOM
} ArkUI_BarrierDirection;


/**
 * @brief Defines the chain style.
 *
 * @since 12
 */
typedef enum {
    /** Child components are evenly distributed among constraint anchors. */
    ARKUI_RELATIVE_LAYOUT_CHAIN_STYLE_SPREAD = 0,
    /** All child components except the first and last ones are evenly distributed among constraint anchors. */
    ARKUI_RELATIVE_LAYOUT_CHAIN_STYLE_SPREAD_INSIDE,
    /** There is no gap between child components in the chain. */
    ARKUI_RELATIVE_LAYOUT_CHAIN_STYLE_PACKED,
} ArkUI_RelativeLayoutChainStyle;

/**
 * @brief Defines the text input style.
 *
 * @since 12
 */
typedef enum {
    /** Default style. The caret width is fixed at 1.5 vp, and the caret height is subject to the background height and
     *  font size of the selected text. */
    ARKUI_TEXTINPUT_STYLE_DEFAULT = 0,
    /** Inline input style. The background height of the selected text is the same as the height of the text box. */
    ARKUI_TEXTINPUT_STYLE_INLINE
} ArkUI_TextInputStyle;

/**
 * @brief Enumerates the entity types of text recognition.
 *
 * @since 12
 */
typedef enum {
    /** Phone number. */
    ARKUI_TEXT_DATA_DETECTOR_TYPE_PHONE_NUMBER = 0,
    /** URL. */
    ARKUI_TEXT_DATA_DETECTOR_TYPE_URL,
    /** Email address. */
    ARKUI_TEXT_DATA_DETECTOR_TYPE_EMAIL,
    /** Address. */
    ARKUI_TEXT_DATA_DETECTOR_TYPE_ADDRESS,
} ArkUI_TextDataDetectorType;

/**
 * @brief Enumerates the button types.
 *
 * @since 12
 */
typedef enum {
    /** Normal button (without rounded corners by default). */
    ARKUI_BUTTON_TYPE_NORMAL = 0,
    /** Capsule-type button (the round corner is half of the height by default). */
    ARKUI_BUTTON_TYPE_CAPSULE,
    /** Circle button. */
    ARKUI_BUTTON_TYPE_CIRCLE,
} ArkUI_ButtonType;

typedef enum {
    /** The component's content stays at the final size and always aligned with the center of the component. */
    ARKUI_RENDER_FIT_CENTER = 0,
    /** The component's content stays at the final size and always aligned with the top center of the component. */
    ARKUI_RENDER_FIT_TOP,
    /** The component's content stays at the final size and always aligned with the bottom center of the component. */
    ARKUI_RENDER_FIT_BOTTOM,
    /** The component's content stays at the final size and always aligned with the left of the component. */
    ARKUI_RENDER_FIT_LEFT,
    /** The component's content stays at the final size and always aligned with the right of the component. */
    ARKUI_RENDER_FIT_RIGHT,
    /** The component's content stays at the final size and always aligned with the upper left corner of the component.
     */
    ARKUI_RENDER_FIT_TOP_LEFT,
    /** The component's content stays at the final size and always aligned with the upper right corner of the component.
     */
    ARKUI_RENDER_FIT_TOP_RIGHT,
    /** The component's content stays at the final size and always aligned with the lower left corner of the component.
     */
    ARKUI_RENDER_FIT_BOTTOM_LEFT,
    /** The component's content stays at the final size and always aligned with the lower right corner of the component.
     */
    ARKUI_RENDER_FIT_BOTTOM_RIGHT,
    /** The component's content is always resized to fill the component's content box, without considering its
     * aspect ratio in the final state. */
    ARKUI_RENDER_FIT_RESIZE_FILL,
    /** While maintaining its aspect ratio in the final state, the component's content is scaled to fit within the
     *  component's content box. It is always aligned with the center of the component. */
    ARKUI_RENDER_FIT_RESIZE_CONTAIN,
    /** While maintaining its aspect ratio in the final state, the component's content is scaled to fit within the
     *  component's content box. When there is remaining space in the width direction of the component, the content
     *  is left-aligned with the component. When there is remaining space in the height direction of the component,
     *  the content is top-aligned with the component. */
    ARKUI_RENDER_FIT_RESIZE_CONTAIN_TOP_LEFT,
    /** While maintaining its aspect ratio in the final state, the component's content is scaled to fit within the
     *  component's content box. When there is remaining space in the width direction of the component, the content is
     *  right-aligned with the component. When there is remaining space in the height direction of the component, the
     *  content is bottom-aligned with the component. */
    ARKUI_RENDER_FIT_RESIZE_CONTAIN_BOTTOM_RIGHT,
    /** While maintaining its aspect ratio in the final state, the component's content is scaled to cover the
     *  component's entire content box. It is always aligned with the center of the component, so that its middle
     *  part is displayed. */
    ARKUI_RENDER_FIT_RESIZE_COVER,
    /** While maintaining its aspect ratio in the final state, the component's content is scaled to cover the
     *  component's entire content box. When there is remaining space in the width direction, the content is
     *  left-aligned with the component, so that its left part is displayed. When there is remaining space in the height
     *  direction, the content is top-aligned with the component, so that its top part is displayed. */
    ARKUI_RENDER_FIT_RESIZE_COVER_TOP_LEFT,
    /** While maintaining its aspect ratio in the final state, the component's content is scaled to cover the
     *  component's entire content box. When there is remaining space in the width direction, the content is
     *  right-aligned with the component, so that its right part is displayed. When there is remaining space in the
     *  height direction, the content is bottom-aligned with the component, so that its bottom part is displayed. */
    ARKUI_RENDER_FIT_RESIZE_COVER_BOTTOM_RIGHT
} ArkUI_RenderFit;

typedef enum {
    /** Following the system color mode. */
    ARKUI_THEME_COLOR_MODE_SYSTEM = 0,
    /** Light color mode. */
    ARKUI_THEME_COLOR_MODE_LIGHT,
    /** Dark color mode. */
    ARKUI_THEME_COLOR_MODE_DARK
} ArkUI_ThemeColorMode;

/**
 * @brief Defines the navigation point indicator type of the <b><Swiper></b> component.
 *
 * @since 12
 */
typedef enum {
    /** Dot type. */
    ARKUI_SWIPER_INDICATOR_TYPE_DOT,
    /** Digit type. */
    ARKUI_SWIPER_INDICATOR_TYPE_DIGIT,
} ArkUI_SwiperIndicatorType;


/**
 * @brief Enumerates the animation playback modes.
 *
 * @since 12
 */
typedef enum {
    /** The animation plays in forward loop mode. */
    ARKUI_ANIMATION_DIRECTION_NORMAL = 0,
    /** The animation plays in reverse loop mode. */
    ARKUI_ANIMATION_DIRECTION_REVERSE,
    /** The animation plays in alternating loop mode. When the animation is played for an odd number of times, the
     *  playback is in forward direction. When the animation is played for an even number of times, the playback is in
     *  reverse direction. */
    ARKUI_ANIMATION_DIRECTION_ALTERNATE,
    /** The animation plays in reverse alternating loop mode. When the animation is played for an odd number of times,
     *  the playback is in reverse direction. When the animation is played for an even number of times, the playback is
     *  in forward direction. */
    ARKUI_ANIMATION_DIRECTION_ALTERNATE_REVERSE,
} ArkUI_AnimationDirection;

/**
 * @brief Enumerates the state of the animated target after the animation is executed.
 *
 * @since 12
 */
typedef enum {
    /** No style is applied to the target before or after the animation is executed. */
    ARKUI_ANIMATION_FILL_NONE = 0,
    /** The target keeps the state at the end of the animation (defined in the last key frame) after the animation is
     *  executed. */
    ARKUI_ANIMATION_FILL_FORWARDS,
    /** The animation uses the value defined in the first key frame during the <b>animation-delay</b>. */
    ARKUI_ANIMATION_FILL_BACKWARDS,
    /** The animation follows the <b>forwards</b> and <b>backwards</b> rules. */
    ARKUI_ANIMATION_FILL_BOTH,
} ArkUI_AnimationFill;

/**
 * @brief Enumerates the modes in which elements are displayed along the main axis of the swiper.
 *
 * @since 12
 */
typedef enum {
    /** The slide width of the <b><Swiper></b> component is equal to the width of the component. */
    ARKUI_SWIPER_DISPLAY_MODE_STRETCH,
    /** The slide width of the <b><Swiper></b> component is equal to the width of the leftmost child component in the
     *  viewport. */
    ARKUI_SWIPER_DISPLAY_MODE_AUTO_LINEAR,
} ArkUI_SwiperDisplayModeType;

/**
 * @brief Enumerates the swipe action item states of list items.
 *
 * @since 12
 */
typedef enum {
    /** Collapsed state. When the list item is swiped in the opposite direction of the main axis, the swipe action item
     *  is hidden. */
    ARKUI_LIST_ITEM_SWIPE_ACTION_STATE_COLLAPSED = 0,
    /** Expanded state. When the list item is swiped in the opposite direction of the main axis, the swipe action item
     *  is shown. */
    ARKUI_LIST_ITEM_SWIPE_ACTION_STATE_EXPANDED,
    /** In-action state. The list item is in this state when it enters the delete area. */
    ARKUI_LIST_ITEM_SWIPE_ACTION_STATE_ACTIONING,
} ArkUI_ListItemSwipeActionState;

/**
 * @brief Defines the edge effect of the swipe action item.
 *
 * @since 12
 */
typedef enum {
    /** When the list item scrolls to the edge of the list, it can continue to scroll for a distance. */
    ARKUI_LIST_ITEM_SWIPE_EDGE_EFFECT_SPRING = 0,
    /** The list item cannot scroll beyond the edge of the list. */
    ARKUI_LIST_ITEM_SWIPE_EDGE_EFFECT_NONE,
} ArkUI_ListItemSwipeEdgeEffect;

/**
 * @brief Enumerates the playback states of the frame-by-frame animation.
 *
 * @since 12
*/
typedef enum {
    /** The animation is in the initial state. */
    ARKUI_ANIMATION_STATUS_INITIAL,
    /** The animation is being played. */
    ARKUI_ANIMATION_STATUS_RUNNING,
    /** The animation is paused. */
    ARKUI_ANIMATION_STATUS_PAUSED,
    /** The animation is stopped. */
    ARKUI_ANIMATION_STATUS_STOPPED,
} ArkUI_AnimationStatus;

/**
 * @brief Enumerates the states before and after execution of the frame-by-frame animation.
 *
 * @since 12
*/
typedef enum {
    /** Before execution, the animation does not apply any styles to the target component. After execution, the
     *  animation restores the target component to its default state. */
    ARKUI_ANIMATION_FILL_MODE_NONE,
    /** The target component retains the state set by the last keyframe encountered during execution of the
     *  animation. */
    ARKUI_ANIMATION_FILL_MODE_FORWARDS,
    /** The animation applies the values defined in the first relevant keyframe once it is applied to the target
     *  component, and retains the values during the period set by <b>delay</b>. */
    ARKUI_ANIMATION_FILL_MODE_BACKWARDS,
    /** The animation follows the rules for both <b><b>Forwards</b></b> and <b><b>Backwards</b></b>, extending the
     *  animation attributes in both directions. */
    ARKUI_ANIMATION_FILL_MODE_BOTH,
} ArkUI_AnimationFillMode;

/**
 * @brief Enumerates the error codes.
 *
 * @since 12
*/
typedef enum {
    /** No error. */
    ARKUI_ERROR_CODE_NO_ERROR = 0,
    /** Invalid parameters. */
    ARKUI_ERROR_CODE_PARAM_INVALID = 401,
    /** API initialization error. */
    ARKUI_ERROR_CODE_CAPI_INIT_ERROR = 500,
    /** The component does not support specific attributes or events. */
    ARKUI_ERROR_CODE_ATTRIBUTE_OR_EVENT_NOT_SUPPORTED = 106102,
    /** The specific operation is not allowed on the node created by ArkTS. */
    ARKUI_ERROR_CODE_NOT_SUPPROTED_FOR_ARKTS_NODE = 106103,
    /** The adapter for lazy loading is not bound to the component. */
    ARKUI_ERROR_CODE_NODE_ADAPTER_NONE_HOST = 106104,
    /** The adapter already exists. */
    ARKUI_ERROR_CODE_NODE_ADAPTER_EXIST_IN_HOST = 106105,
    /** Failed to add the adapter because the corresponding node already has a subnode. */
    ARKUI_ERROR_CODE_NODE_ADAPTER_CHILD_NODE_EXIST = 106106,
    /** The parameter length in the parameter event exceeds the limit. */
    ARKUI_ERROR_CODE_NODE_EVENT_PARAM_INDEX_OUT_OF_RANGE = 106107,
    /** The data does not exist in the component event. */
    ARKUI_ERROR_CODE_NODE_EVENT_PARAM_INVALID = 106108,
    /** The component event does not support return values. */
    ARKUI_ERROR_CODE_NODE_EVENT_NO_RETURN = 106109,
    /** Invalid index. */
    ARKUI_ERROR_CODE_NODE_INDEX_INVALID = 106200,
    /** Failed to obtain the route navigation information. */
    ARKUI_ERROR_CODE_GET_INFO_FAILED = 106201,
    /** Buffer size error. */
    ARKUI_ERROR_CODE_BUFFER_SIZE_ERROR = 106202,
    /** The component is not a scroll container. */
    ARKUI_ERROR_CODE_NON_SCROLLABLE_CONTAINER = 180001,
    /** The buffer is not large enough.
    ARKUI_ERROR_CODE_BUFFER_SIZE_NOT_ENOUGH = 180002,
    /** Invalid styled string. */
    ARKUI_ERROR_CODE_INVALID_STYLED_STRING = 180101,
    /** Invalid UIContext object. */
    ARKUI_ERROR_CODE_UI_CONTEXT_INVALID = 190001,
    /** Invalid callback function. */
    ARKUI_ERROR_CODE_CALLBACK_INVALID = 190002,
} ArkUI_ErrorCode;

/**
 * @brief Enumerates the scroll sources.
 *
 * @since 12
*/
typedef enum {
    /** Finger dragging. */
    ARKUI_SCROLL_SOURCE_DRAG = 0,
    /** Inertia scrolling after finger dragging. */
    ARKUI_SCROLL_SOURCE_FLING,
    /** Cross-boundary bouncing */
    ARKUI_SCROLL_SOURCE_EDGE_EFFECT,
    /** User input other than dragging, such as mouse wheel and keyboard events. */
    ARKUI_SCROLL_SOURCE_OTHER_USER_INPUT,
    /** Scrollbar dragging. */
    ARKUI_SCROLL_SOURCE_SCROLL_BAR,
    /** Inertial scrolling after scrollbar dragging. */
    ARKUI_SCROLL_SOURCE_SCROLL_BAR_FLING,
    /** Scrolling by the scroll controller (without animation). */
    ARKUI_SCROLL_SOURCE_SCROLLER,
    /** Scrolling by the scroll controller (with animation). */
    ARKUI_SCROLL_SOURCE_ANIMATION,
} ArkUI_ScrollSource;

/**
 * @brief Enumerates the types of expanded safe areas.
 *
 * @since 12
*/
typedef enum {
    /** Default non-safe area of the system, including the status bar and navigation bar. */
    ARKUI_SAFE_AREA_TYPE_SYSTEM = 1,
    /** Non-safe area of the device, for example, the notch area. */
    ARKUI_SAFE_AREA_TYPE_CUTOUT = 1 << 1,
    /** Soft keyboard area. */
    ARKUI_SAFE_AREA_TYPE_KEYBOARD = 1 << 2,
} ArkUI_SafeAreaType;

/**
 * @brief Enumerates the edges for expanding the safe area.
 *
 * @since 12
*/
typedef enum {
    /** Top edge. */
    ARKUI_SAFE_AREA_EDGE_TOP = 1,
    /** Bottom edge. */
    ARKUI_SAFE_AREA_EDGE_BOTTOM = 1 << 1,
    /** Start edge. */
    ARKUI_SAFE_AREA_EDGE_START = 1 << 2,
    /** End edge. */
    ARKUI_SAFE_AREA_EDGE_END = 1 << 3,
} ArkUI_SafeAreaEdge;

/**
 * @brief Define an enum for the areas of the <b>ListItemGroup</b> component.
 *
 * @since 16
 */
typedef enum {
    /** Outside the area of the <b>ListItemGroup</b> component. */
    ARKUI_LIST_ITEM_GROUP_AREA_OUTSIDE = 0,
    /** Area when the <b>ListItemGroup</b> component does not have the header, footer, or list item. */
    ARKUI_LIST_ITEM_SWIPE_AREA_NONE,
    /** List item area of the <b>ListItemGroup</b> component. */
    ARKUI_LIST_ITEM_SWIPE_AREA_ITEM,
    /** Header area of the <b>ListItemGroup</b> component. */
    ARKUI_LIST_ITEM_SWIPE_AREA_HEADER,
    /** Footer area of the <b>ListItemGroup</b> component. */
    ARKUI_LIST_ITEM_SWIPE_AREA_FOOTER,
} ArkUI_ListItemGroupArea;

/**
 * @brief Defines a system font style event.
 *
 * @since 12
 */
typedef struct ArkUI_SystemFontStyleEvent ArkUI_SystemFontStyleEvent;

/**
 * @brief Defines the translation options for component transition.
 *
 * @since 12
 */
typedef struct {
    /** Translation distance along the x-axis. */
    float x;
    /** Translation distance along the y-axis. */
    float y;
    /** Translation distance along the z-axis. */
    float z;
} ArkUI_TranslationOptions;

/**
 * @brief Defines the scaling options for component transition.
 *
 * @since 12
 */
typedef struct {
    /** Scale ratio along the x-axis. */
    float x;
    /** Scale ratio along the y-axis. */
    float y;
    /** Scale factor along the z-axis (not effective for the current 2D graphics). */
    float z;
    /** X coordinate of the center point. */
    float centerX;
    /** Y coordinate of the center point. */
    float centerY;
} ArkUI_ScaleOptions;

/**
 * @brief Defines the rotation options for component transition.
 *
 * @since 12
 */
typedef struct {
    /** X-component of the rotation vector. */
    float x;
    /** Y-component of the rotation vector. */
    float y;
    /** Z-component of the rotation vector. */
    float z;
    /** Rotation angle. */
    float angle;
    /** X coordinate of the center point. */
    float centerX;
    /** Y coordinate of the center point. */
    float centerY;
    /** Z-axis anchor, that is, the z-component of the 3D rotation center point. */
    float centerZ;
    /** Distance from the user to the z=0 plane. */
    float perspective;
} ArkUI_RotationOptions;

/**
 * @brief Defines a struct for the measurement information of a custom span.
 *
 * @since 12
 */
typedef struct ArkUI_CustomSpanMeasureInfo ArkUI_CustomSpanMeasureInfo;

/**
 * @brief Defines a struct for the measurement metrics of a custom span.
 *
 * @since 12
 */
typedef struct ArkUI_CustomSpanMetrics ArkUI_CustomSpanMetrics;

/**
 * @brief Defines a struct for the drawing information of a custom span.
 *
 * @since 12
 */
typedef struct ArkUI_CustomSpanDrawInfo ArkUI_CustomSpanDrawInfo;

/**
 * @brief Enumerates the states of the <b>NavDestination</b> component.
 *
 * @since 12
*/
typedef enum {
    /** The <b>NavDestination</b> component is displayed. */
    ARKUI_NAV_DESTINATION_STATE_ON_SHOW = 0,
    /** The <b>NavDestination</b> component is hidden. */
    ARKUI_NAV_DESTINATION_STATE_ON_HIDE = 1,
    /** The <b>NavDestination</b> component is mounted to the component tree. */
    ARKUI_NAV_DESTINATION_STATE_ON_APPEAR = 2,
    /** The <b>NavDestination</b> component is unmounted from the component tree. */
    ARKUI_NAV_DESTINATION_STATE_ON_DISAPPEAR = 3,
    /** The <b>NavDestination</b> is about to be displayed. */
    ARKUI_NAV_DESTINATION_STATE_ON_WILL_SHOW = 4,
    /** The <b>NavDestination</b> is about to be hidden. */
    ARKUI_NAV_DESTINATION_STATE_ON_WILL_HIDE = 5,
    /** The <b>NavDestination</b> is about to be mounted to the component tree. */
    ARKUI_NAV_DESTINATION_STATE_ON_WILL_APPEAR = 6,
    /** The <b>NavDestination</b> component is about to be unmounted from the component tree. */
    ARKUI_NAV_DESTINATION_STATE_ON_WILL_DISAPPEAR = 7,
    /** The back button is pressed for the <b>NavDestination</b> component. */
    ARKUI_NAV_DESTINATION_STATE_ON_BACK_PRESS = 100,
} ArkUI_NavDestinationState;

/**
 * @brief Enumerates the states of a page during routing.
 *
 * @since 12
*/
typedef enum {
    /** The page is about to be displayed. */
    ARKUI_ROUTER_PAGE_STATE_ON_WILL_APPEAR = 0,
    /** The page is about to be destroyed. */
    ARKUI_ROUTER_PAGE_STATE_ON_WILL_DISAPPEAR = 1,
    /** The page is displayed. */
    ARKUI_ROUTER_PAGE_STATE_ON_SHOW = 2,
    /** The page is hidden. */
    ARKUI_ROUTER_PAGE_STATE_ON_HIDE = 3,
    /** The back button is pressed for the page. */
    ARKUI_ROUTER_PAGE_STATE_ON_BACK_PRESS = 4,
} ArkUI_RouterPageState;

/**
 * @brief Defines the navigation point indicator style of the <b><Swiper></b> component.
 *
 * @since 12
 */
typedef struct ArkUI_SwiperIndicator ArkUI_SwiperIndicator;

/**
* @brief Defines a styled string information object supported by the text component.
*
* @since 14
*/
typedef struct ArkUI_StyledString_Descriptor ArkUI_StyledString_Descriptor;

/**
* @brief Creates a size constraint.
*
* @since 12
*/
ArkUI_LayoutConstraint* OH_ArkUI_LayoutConstraint_Create();

/**
* @brief Creates a deep copy of a size constraint.
*
* @param Constraint Indicates the pointer to the size constraint.
* @return Returns the pointer to the new size constraint.
* @since 12
*/
ArkUI_LayoutConstraint* OH_ArkUI_LayoutConstraint_Copy(const ArkUI_LayoutConstraint* Constraint);

/**
* @brief Disposes of the pointer to a size constraint.
*
* @param Constraint Indicates the pointer to the size constraint.
* @since 12
*/
void* OH_ArkUI_LayoutConstraint_Dispose(ArkUI_LayoutConstraint* Constraint);

/**
* @brief Obtains the maximum width for a size constraint, in px.
*
* @param Constraint Indicates the pointer to the size constraint.
* @return Returns the maximum width.
* @since 12
*/
int32_t OH_ArkUI_LayoutConstraint_GetMaxWidth(const ArkUI_LayoutConstraint* Constraint);

/**
* @brief Obtains the minimum width for a size constraint, in px.
*
* @param Constraint Indicates the pointer to the size constraint.
* @return Returns the minimum width.
* @since 12
*/
int32_t OH_ArkUI_LayoutConstraint_GetMinWidth(const ArkUI_LayoutConstraint* Constraint);

/**
* @brief Obtains the maximum height for a size constraint, in px.
*
* @param Constraint Indicates the pointer to the size constraint.
* @return Returns the maximum height.
* @since 12
*/
int32_t OH_ArkUI_LayoutConstraint_GetMaxHeight(const ArkUI_LayoutConstraint* Constraint);

/**
* @brief Obtains the minimum height for a size constraint, in px.
*
* @param Constraint Indicates the pointer to the size constraint.
* @return Returns the minimum height.
* @since 12
*/
int32_t OH_ArkUI_LayoutConstraint_GetMinHeight(const ArkUI_LayoutConstraint* Constraint);

/**
* @brief Obtains the width percentage reference for a size constraint, in px.
*
* @param Constraint Indicates the pointer to the size constraint.
* @return Returns the width percentage reference.
* @since 12
*/
int32_t OH_ArkUI_LayoutConstraint_GetPercentReferenceWidth(const ArkUI_LayoutConstraint* Constraint);

/**
* @brief Obtains the height percentage reference for a size constraint, in px.
*
* @param Constraint Indicates the pointer to the size constraint.
* @return Returns the height percentage reference.
* @since 12
*/
int32_t OH_ArkUI_LayoutConstraint_GetPercentReferenceHeight(const ArkUI_LayoutConstraint* Constraint);

/**
* @brief Sets the maximum width.
*
* @param Constraint Indicates the pointer to the size constraint.
* @param value Indicates the maximum width, in px.
* @since 12
*/
void OH_ArkUI_LayoutConstraint_SetMaxWidth(ArkUI_LayoutConstraint* Constraint, int32_t value);

/**
* @brief Sets the minimum width.
*
* @param Constraint Indicates the pointer to the size constraint.
* @param value Indicates the minimum width, in px.
* @since 12
*/
void OH_ArkUI_LayoutConstraint_SetMinWidth(ArkUI_LayoutConstraint* Constraint, int32_t value);

/**
* @brief Sets the maximum height.
*
* @param Constraint Indicates the pointer to the size constraint.
* @param value Indicates the maximum height, in px.
* @since 12
*/
void OH_ArkUI_LayoutConstraint_SetMaxHeight(ArkUI_LayoutConstraint* Constraint, int32_t value);

/**
* @brief Sets the minimum height.
*
* @param Constraint Indicates the pointer to the size constraint.
* @param value Indicates the minimum height, in px.
* @since 12
*/
void OH_ArkUI_LayoutConstraint_SetMinHeight(ArkUI_LayoutConstraint* Constraint, int32_t value);

/**
* @brief Sets the width percentage reference.
*
* @param Constraint Indicates the pointer to the size constraint.
* @param value Indicates the width percentage reference, in px.
* @since 12
*/
void OH_ArkUI_LayoutConstraint_SetPercentReferenceWidth(ArkUI_LayoutConstraint* Constraint, int32_t value);

/**
* @brief Sets the height percentage reference.
*
* @param Constraint Indicates the pointer to the size constraint.
* @param value Indicates the height percentage reference, in px.
* @since 12
*/
void OH_ArkUI_LayoutConstraint_SetPercentReferenceHeight(ArkUI_LayoutConstraint* Constraint, int32_t value);

/**
* @brief Obtains the pointer to a canvas for drawing, which can be converted into the <b>OH_Drawing_Canvas</b> pointer
* in the <b>Drawing</b> module.
*
* @param context Indicates the pointer to the drawing context.
* @return Returns the pointer to the canvas for drawing.
* @since 12
*/
void* OH_ArkUI_DrawContext_GetCanvas(ArkUI_DrawContext* context);

/**
* @brief Obtains the size of a drawing area.
*
* @param context Indicates the pointer to the drawing context.
* @return Returns the size of the drawing area.
* @since 12
*/
ArkUI_IntSize OH_ArkUI_DrawContext_GetSize(ArkUI_DrawContext* context);

/**
* @brief Creates water flow section configuration.
*
* @return Returns the water flow section configuration.
* @since 12
*/
ArkUI_WaterFlowSectionOption* OH_ArkUI_WaterFlowSectionOption_Create();

/**
* @brief Disposes of the pointer to a water flow section configuration.
*
* @param option Indicates the pointer to a water flow section configuration.
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_Dispose(ArkUI_WaterFlowSectionOption* option);

/**
* @brief Sets the array length for a water flow section configuration.
*
* @param option Indicates the pointer to a water flow section configuration.
* @param size Indicates the array length.
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_SetSize(ArkUI_WaterFlowSectionOption* option,
    int32_t size);

/**
* @brief Obtains the array length for a water flow section configuration.
*
* @param option Indicates the pointer to a water flow section configuration.
* @return Returns the array length. If <b>-1</b> is returned, an error code indicating failure is returned.
* The possible cause is that the <b>option</b> parameter is abnormal, for example, a null pointer.
* @since 12
*/
int32_t OH_ArkUI_WaterFlowSectionOption_GetSize(ArkUI_WaterFlowSectionOption* option);

/**
* @brief Sets the number of items in a water flow section.
*
* @param option Indicates the pointer to a water flow section configuration.
* @param index Indicates the index of the target water flow section.
* @param itemCount Indicates the number of items in the water flow section.
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_SetItemCount(ArkUI_WaterFlowSectionOption* option,
    int32_t index, int32_t itemCount);

/**
* @brief Obtains the number of items in the water flow section that matches the specified index.
*
* @param option Indicates the pointer to a water flow section configuration.
* @param index Indicates the index of the target water flow section.
* @return Returns the number of items in the water flow section.
* @since 12
*/
int32_t OH_ArkUI_WaterFlowSectionOption_GetItemCount(ArkUI_WaterFlowSectionOption* option, int32_t index);

/**
* @brief Sets the number of columns (in a vertical layout) or rows (in a horizontal layout) of a water flow.
*
* @param option Indicates the pointer to a water flow section configuration.
* @param index Indicates the index of the target water flow section.
* @param crossCount Indicates the number of columns or rows, depending on the layout direction.
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_SetCrossCount(ArkUI_WaterFlowSectionOption* option,
    int32_t index, int32_t crossCount);

/**
* @brief Obtains the number of columns (in a vertical layout) or rows (in a horizontal layout) in the water flow section
* that matches the specified index.
*
* @param option Indicates the pointer to a water flow section configuration.
* @param index Indicates the index of the target water flow section.
* @return Returns the number of columns or rows.
* @since 12
*/
int32_t OH_ArkUI_WaterFlowSectionOption_GetCrossCount(ArkUI_WaterFlowSectionOption* option, int32_t index);

/**
* @brief Sets the gap between columns in the specified water flow section.
*
* @param option Indicates the pointer to a water flow section configuration.
* @param index Indicates the index of the target water flow section.
* @param columnGap Indicates the gap between columns to set.
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_SetColumnGap(ArkUI_WaterFlowSectionOption*,
    int32_t index, float columnGap);

/**
* @brief Obtains the gap between columns in the water flow section that matches the specified index.
*
* @param option Indicates the pointer to a water flow section configuration.
* @param index Indicates the index of the target water flow section.
* @return Returns the gap between columns.
* @since 12
*/
float OH_ArkUI_WaterFlowSectionOption_GetColumnGap(ArkUI_WaterFlowSectionOption* option, int32_t index);

/**
* @brief Sets the gap between rows in the specified water flow section.
*
* @param option Indicates the pointer to a water flow section configuration.
* @param index Indicates the index of the target water flow section.
* @param rowGap Indicates the gap between rows to set.
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_SetRowGap(ArkUI_WaterFlowSectionOption* option,
    int32_t index, float rowGap);

/**
* @brief Obtains the gap between rows in the water flow section that matches the specified index.
*
* @param option Indicates the pointer to a water flow section configuration.
* @param index Indicates the index of the target water flow section.
* @return Returns the gap between rows.
* @since 12
*/
float OH_ArkUI_WaterFlowSectionOption_GetRowGap(ArkUI_WaterFlowSectionOption* option, int32_t index);

/**
* @brief Sets the margins for the specified water flow section.
*
* @param option Indicates the pointer to a water flow section configuration.
* @param index Indicates the index of the target water flow section.
* @param marginTop Indicates the top margin of the water flow section.
* @param marginRight Indicates the right margin of the water flow section.
* @param marginBottom Indicates the bottom margin of the water flow section.
* @param marginLeft Indicates the left margin of the water flow section.
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_SetMargin(ArkUI_WaterFlowSectionOption* option, int32_t index,
    float marginTop, float marginRight, float marginBottom, float marginLeft);

/**
* @brief Obtains the margins of the water flow section that matches the specified index.
*
* @param option Indicates the pointer to a water flow section configuration.
* @param index Indicates the index of the target water flow section.
* @return Returns the margins.
* @since 12
*/
ArkUI_Margin OH_ArkUI_WaterFlowSectionOption_GetMargin(ArkUI_WaterFlowSectionOption* option, int32_t index);

/**
* @brief Obtains the main axis size of a specified item based on <b>flowItemIndex</b> through a water flow section
* configuration.
*
* @param option Indicates the pointer to a water flow section configuration.
* @param index Indicates the index of the target water flow section.
* @param callback Indicates the callback used to return the result.
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_RegisterGetItemMainSizeCallbackByIndex(ArkUI_WaterFlowSectionOption* option,
    int32_t index, float(*callback)(int32_t itemIndex));

/**
* @brief Obtains the main axis size of a specified item based on <b>flowItemIndex</b> through a water flow section
* configuration.
*
* @param option Indicates the pointer to a water flow section configuration.
* @param index Indicates the index of the target water flow section.
* @param userData Indicates custom data of the water flow item.
* @param callback Indicates the callback used to return the result.
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_RegisterGetItemMainSizeCallbackByIndexWithUserData(
    ArkUI_WaterFlowSectionOption* option, int32_t index, void* userData,
    float (*callback)(int32_t itemIndex, void* userData));

/**
* @brief Creates a guideline configuration for this <b>RelativeContainer</b>.
*
* @param size Indicates the number of guidelines.
* @return Returns the guideline configuration.
* @since 12
*/
ArkUI_GuidelineOption* OH_ArkUI_GuidelineOption_Create(int32_t size);

/**
* @brief Disposes of a guideline configuration.
*
* @param guideline Indicates the pointer to a guideline configuration.
* @since 12
*/
void OH_ArkUI_GuidelineOption_Dispose(ArkUI_GuidelineOption* guideline);

/**
* @brief Sets the ID of a guideline.
*
* @param guideline Indicates the pointer to a guideline configuration.
* @param value Indicates the ID to set, which must be unique and cannot be the same as the name of any component in
* the container.
* @param index Indicates the index of the guideline.
* @since 12
*/
void OH_ArkUI_GuidelineOption_SetId(ArkUI_GuidelineOption* guideline, const char* value, int32_t index);

/**
* @brief Sets the direction of a guideline.
*
* @param guideline Indicates the pointer to a guideline configuration.
* @param value Indicates the direction to set.
* @param index Indicates the index of the guideline.
* @since 12
*/
void OH_ArkUI_GuidelineOption_SetDirection(ArkUI_GuidelineOption* guideline, ArkUI_Axis value, int32_t index);

/**
* @brief Sets the distance between a guideline and the left or top of the container.
*
* @param guideline Indicates the pointer to a guideline configuration.
* @param value Indicates the distance between the guideline and the left or top of the container.
* @param index Indicates the index of the guideline.
* @since 12
*/
void OH_ArkUI_GuidelineOption_SetPositionStart(ArkUI_GuidelineOption* guideline, float value, int32_t index);

/**
* @brief Sets the distance between a guideline and the right or bottom of the container.
*
* @param guideline Indicates the pointer to a guideline configuration.
* @param value Indicates the distance between the guideline and the right or bottom of the container.
* @param index Indicates the index of the guideline.
* @since 12
*/
void OH_ArkUI_GuidelineOption_SetPositionEnd(ArkUI_GuidelineOption* guideline, float value, int32_t index);

/**
* @brief Obtains the ID of a guideline.
*
* @param guideline Indicates the pointer to a guideline configuration.
* @param index Indicates the index of the guideline.
* @return Returns the ID of the guideline.
* @since 12
*/
const char* OH_ArkUI_GuidelineOption_GetId(ArkUI_GuidelineOption* guideline, int32_t index);

/**
* @brief Obtains the direction of a guideline.
*
* @param guideline Indicates the pointer to a guideline configuration.
* @param index Indicates the index of the guideline.
* @return Returns the direction of the guideline.
* @since 12
*/
ArkUI_Axis OH_ArkUI_GuidelineOption_GetDirection(ArkUI_GuidelineOption* guideline, int32_t index);

/**
* @brief Obtains the distance between a guideline and the left or top of the container.
*
* @param guideline Indicates the pointer to a guideline configuration.
* @param index Indicates the index of the guideline.
* @return Returns the distance between the guideline and the left or top of the container.
* @since 12
*/
float OH_ArkUI_GuidelineOption_GetPositionStart(ArkUI_GuidelineOption* guideline, int32_t index);

/**
* @brief Obtains the distance between a guideline and the right or bottom of the container.
*
* @param guideline Indicates the pointer to a guideline configuration.
* @param index Indicates the index of the guideline.
* @return Returns the distance between the guideline and the right or bottom of the container.
* @since 12
*/
float OH_ArkUI_GuidelineOption_GetPositionEnd(ArkUI_GuidelineOption* guideline, int32_t index);

/**
* @brief Creates a barrier configuration for this <b>RelativeContainer</b>.
*
* @param size Indicates the number of barriers.
* @return Returns the barrier configuration.
* @since 12
*/
ArkUI_BarrierOption* OH_ArkUI_BarrierOption_Create(int32_t size);

/**
* @brief Disposes of a barrier configuration.
*
* @param barrierStyle Indicates the pointer to a barrier configuration.
* @since 12
*/
void OH_ArkUI_BarrierOption_Dispose(ArkUI_BarrierOption* barrierStyle);

/**
* @brief Sets the ID of a barrier.
*
* @param barrierStyle Indicates the pointer to a barrier configuration.
* @param value Indicates the ID to set, which must be unique and cannot be the same as the name of any component in
* the container.
* @param index Indicates the index of the barrier.
* @since 12
*/
void OH_ArkUI_BarrierOption_SetId(ArkUI_BarrierOption* barrierStyle, const char* value, int32_t index);

/**
* @brief Sets the direction of a barrier.
*
* @param barrierStyle Indicates the pointer to a barrier configuration.
* @param value Indicates the direction to set.
* @param index Indicates the index of the barrier.
* @since 12
*/
void OH_ArkUI_BarrierOption_SetDirection(ArkUI_BarrierOption* barrierStyle, ArkUI_BarrierDirection value, int32_t index);

/**
* @brief Sets the referenced components of a barrier.
*
* @param barrierStyle Indicates the pointer to a barrier configuration.
* @param value Indicates the referenced component IDs.
* @param index Indicates the index of the barrier.
* @since 12
*/
void OH_ArkUI_BarrierOption_SetReferencedId(ArkUI_BarrierOption* barrierStyle, const char* value, int32_t index);

/**
* @brief Obtains the ID of a barrier.
*
* @param barrierStyle Indicates the barrier style.
* @param index Indicates the index of the barrier.
* @return Returns the ID of the barrier.
* @since 12
*/
const char* OH_ArkUI_BarrierOption_GetId(ArkUI_BarrierOption* barrierStyle, int32_t index);

/**
* @brief Obtains the direction of a barrier.
*
* @param barrierStyle Indicates the barrier style.
* @param index Indicates the index of the barrier.
* @return Returns the direction of the barrier.
* @since 12
*/
ArkUI_BarrierDirection OH_ArkUI_BarrierOption_GetDirection(ArkUI_BarrierOption* barrierStyle, int32_t index);

/**
* @brief Obtains the referenced components of a barrier.
*
* @param barrierStyle Indicates the barrier style.
* @param index Indicates the index of the barrier.
* @param referencedIndex Indicates the referenced component indexes.
* @return Returns the referenced components of the barrier.
* @since 12
*/
const char* OH_ArkUI_BarrierOption_GetReferencedId(ArkUI_BarrierOption* barrierStyle, int32_t index , int32_t referencedIndex);

/**
* @brief Obtains the number of referenced components of a barrier.
*
* @param barrierStyle Indicates the barrier style.
* @param index Indicates the index of the barrier.
* @return Returns the number of referenced components of the barrier.
* @since 12
*/
int32_t OH_ArkUI_BarrierOption_GetReferencedIdSize(ArkUI_BarrierOption* barrierStyle, int32_t index);

/**
* @brief Creates an alignment rule configuration for this <b>RelativeContainer</b>.
*
* @return Returns the alignment rule configuration.
* @since 12
*/
ArkUI_AlignmentRuleOption* OH_ArkUI_AlignmentRuleOption_Create();

/**
* @brief Disposes of an alignment rule configuration of this <b>RelativeContainer</b>.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_Dispose(ArkUI_AlignmentRuleOption* option);

/**
* @brief Sets the left alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @param id Indicates the ID of the component that functions as the anchor point.
* @param value Indicates the alignment mode relative to the anchor component.
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_SetStart(ArkUI_AlignmentRuleOption* option, const char* id, ArkUI_HorizontalAlignment alignment);

/**
* @brief Sets the right alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @param id Indicates the ID of the component that functions as the anchor point.
* @param value Indicates the alignment mode relative to the anchor component.
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_SetEnd(ArkUI_AlignmentRuleOption* option, const char* id, ArkUI_HorizontalAlignment alignment);

/**
* @brief Sets the horizontal center alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @param id Indicates the ID of the component that functions as the anchor point.
* @param value Indicates the alignment mode relative to the anchor component.
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_SetCenterHorizontal(ArkUI_AlignmentRuleOption* option, const char* id, ArkUI_HorizontalAlignment alignment);

/**
* @brief Sets the top alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @param id Indicates the ID of the component that functions as the anchor point.
* @param value Indicates the alignment mode relative to the anchor component.
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_SetTop(ArkUI_AlignmentRuleOption* option, const char* id, ArkUI_VerticalAlignment alignment);

/**
* @brief Sets the bottom alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @param id Indicates the ID of the component that functions as the anchor point.
* @param value Indicates the alignment mode relative to the anchor component.
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_SetBottom(ArkUI_AlignmentRuleOption* option, const char* id, ArkUI_VerticalAlignment alignment);

/**
* @brief Sets the vertical center alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @param id Indicates the ID of the component that functions as the anchor point.
* @param value Indicates the alignment mode relative to the anchor component.
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_SetCenterVertical(ArkUI_AlignmentRuleOption* option, const char* id, ArkUI_VerticalAlignment alignment);

/**
* @brief Sets the bias value of the component in the horizontal direction under the anchor constraints.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @param horizontal Indicates the bias value in the horizontal direction.
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_SetBiasHorizontal(ArkUI_AlignmentRuleOption* option, float horizontal);

/**
* @brief Sets the bias value of the component in the vertical direction under the anchor constraints.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @param horizontal Indicates the bias value in the vertical direction.
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_SetBiasVertical(ArkUI_AlignmentRuleOption* option, float vertical);

/**
* @brief Obtains the ID in the left alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @return Returns the ID of the component that functions as the anchor point.
* @since 12
*/
const char* OH_ArkUI_AlignmentRuleOption_GetStartId(ArkUI_AlignmentRuleOption* option);

/**
* @brief Obtains the alignment mode in the left alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @return Returns the alignment mode.
* @since 12
*/
ArkUI_HorizontalAlignment OH_ArkUI_AlignmentRuleOption_GetStartAlignment(ArkUI_AlignmentRuleOption* option);

/**
* @brief Obtains the ID in the right alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @return Returns the ID of the component that functions as the anchor point.
* @since 12
*/
const char* OH_ArkUI_AlignmentRuleOption_GetEndId(ArkUI_AlignmentRuleOption* option);

/**
* @brief Obtains the alignment mode in the right alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @return Returns the alignment mode.
* @since 12
*/
ArkUI_HorizontalAlignment OH_ArkUI_AlignmentRuleOption_GetEndAlignment(ArkUI_AlignmentRuleOption* option);

/**
* @brief Obtains the ID in the horizontal center alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @return Returns the ID of the component that functions as the anchor point.
* @since 12
*/
const char* OH_ArkUI_AlignmentRuleOption_GetCenterIdHorizontal(ArkUI_AlignmentRuleOption* option);

/**
* @brief Obtains the alignment mode in the horizontal center alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @return Returns the alignment mode.
* @since 12
*/
ArkUI_HorizontalAlignment OH_ArkUI_AlignmentRuleOption_GetCenterAlignmentHorizontal(ArkUI_AlignmentRuleOption* option);

/**
* @brief Obtains the ID in the top alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @return Returns the ID of the component that functions as the anchor point.
* @since 12
*/
const char* OH_ArkUI_AlignmentRuleOption_GetTopId(ArkUI_AlignmentRuleOption* option);

/**
* @brief Obtains the alignment mode in the top alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @return Returns the alignment mode.
* @since 12
*/
ArkUI_VerticalAlignment OH_ArkUI_AlignmentRuleOption_GetTopAlignment(ArkUI_AlignmentRuleOption* option);

/**
* @brief Obtains the ID in the bottom alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @return Returns the ID of the component that functions as the anchor point.
* @since 12
*/
const char* OH_ArkUI_AlignmentRuleOption_GetBottomId(ArkUI_AlignmentRuleOption* option);

/**
* @brief Obtains the alignment mode in the bottom alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @return Returns the alignment mode.
* @since 12
*/
ArkUI_VerticalAlignment OH_ArkUI_AlignmentRuleOption_GetBottomAlignment(ArkUI_AlignmentRuleOption* option);

/**
* @brief Obtains the ID in the vertical center alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @return Returns the ID of the component that functions as the anchor point.
* @since 12
*/
const char* OH_ArkUI_AlignmentRuleOption_GetCenterIdVertical(ArkUI_AlignmentRuleOption* option);

/**
* @brief Obtains the alignment mode in the vertical center alignment parameters.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @return Returns the alignment mode.
* @since 12
*/
ArkUI_VerticalAlignment OH_ArkUI_AlignmentRuleOption_GetCenterAlignmentVertical(ArkUI_AlignmentRuleOption* option);

/**
* @brief Obtains the bias value in the horizontal direction.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @return Returns the bias value in the horizontal direction.
* @since 12
*/
float OH_ArkUI_AlignmentRuleOption_GetBiasHorizontal(ArkUI_AlignmentRuleOption* option);

/**
* @brief Obtains the bias value in the vertical direction.
*
* @param option Indicates the pointer to an alignment rule configuration.
* @return Returns the bias value in the vertical direction.
* @since 12
*/
float OH_ArkUI_AlignmentRuleOption_GetBiasVertical(ArkUI_AlignmentRuleOption* option);
/**
 * @brief Creates a navigation point indicator for this <b><Swiper></b> component.
 *
 * @param type Indicates the type of the navigation point indicator.
 * @return Returns the pointer to the navigation point indicator.
 * @since 12
*/
ArkUI_SwiperIndicator* OH_ArkUI_SwiperIndicator_Create(ArkUI_SwiperIndicatorType type);

/**
 * @brief Disposes of the navigation point indicator of this <b><Swiper></b> component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_Dispose(ArkUI_SwiperIndicator* indicator);

/**
 * @brief Sets the distance between a navigation point indicator and the left edge of the <b><Swiper></b> component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @param value Indicates the distance between the navigation point indicator and the left edge of the <b><Swiper></b>
 * component.
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetStartPosition(ArkUI_SwiperIndicator* indicator, float value);

/**
 * @brief Obtains the distance between a navigation point indicator and the left edge of the <b><Swiper></b> component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @return Returns the distance between the navigation point indicator and the left edge of the <b><Swiper></b>
 * component.
 * @since 12
*/
float OH_ArkUI_SwiperIndicator_GetStartPosition(ArkUI_SwiperIndicator* indicator);

/**
 * @brief Sets the distance between a navigation point indicator and the top edge of the <b><Swiper></b> component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @param value Indicates the distance between the navigation point indicator and the top edge of the <b><Swiper></b>
 * component.
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetTopPosition(ArkUI_SwiperIndicator* indicator, float value);

/**
 * @brief Obtains the distance between a navigation point indicator and the top edge of the <b><Swiper></b> component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @return Returns the distance between a navigation point indicator and the top edge of the <b><Swiper></b> component.
 * @since 12
*/
float OH_ArkUI_SwiperIndicator_GetTopPosition(ArkUI_SwiperIndicator* indicator);

/**
 * @brief Sets the distance between a navigation point indicator and the right edge of the <b><Swiper></b> component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @param value Indicates the distance between the navigation point indicator and the right edge of the <b><Swiper></b>
 * component.
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetEndPosition(ArkUI_SwiperIndicator* indicator, float value);

/**
 * @brief Obtains the distance between a navigation point indicator and the right edge of the <b><Swiper></b> component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @return Returns the distance between the navigation point indicator and the right edge of the <b><Swiper></b>
 * component.
 * @since 12
*/
float OH_ArkUI_SwiperIndicator_GetEndPosition(ArkUI_SwiperIndicator* indicator);

/**
 * @brief Sets the distance between a navigation point indicator and the bottom edge of the <b><Swiper></b> component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @param value Indicates the distance between the navigation point indicator and the bottom edge of the <b><Swiper></b>
 * component.
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetBottomPosition(ArkUI_SwiperIndicator* indicator, float value);

/**
 * @brief Obtains the distance between a navigation point indicator and the bottom edge of the <b><Swiper></b>
 * component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @return Returns the distance between a navigation point indicator and the bottom edge of the <b><Swiper></b>
 * component.
 * @since 12
*/
float OH_ArkUI_SwiperIndicator_GetBottomPosition(ArkUI_SwiperIndicator* indicator);

/**
 * @brief Sets the width of a navigation point indicator of the dot style for the <b><Swiper></b> component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @param value Indicates the width of the navigation point indicator of the dot style.
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetItemWidth(ArkUI_SwiperIndicator* indicator, float value);

/**
 * @brief Obtains the width of a navigation point indicator of the dot style of the <b><Swiper></b> component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @return Returns the width of the navigation point indicator of the dot style.
 * @since 12
*/
float OH_ArkUI_SwiperIndicator_GetItemWidth(ArkUI_SwiperIndicator* indicator);

/**
 * @brief Sets the height of a navigation point indicator of the dot style for the <b><Swiper></b> component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @param value Indicates the height of the navigation point indicator of the dot style.
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetItemHeight(ArkUI_SwiperIndicator* indicator, float value);

/**
 * @brief Obtains the height of a navigation point indicator of the dot style of the <b><Swiper></b> component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @return Returns the height of the navigation point indicator of the dot style.
 * @since 12
*/
float OH_ArkUI_SwiperIndicator_GetItemHeight(ArkUI_SwiperIndicator* indicator);

/**
 * @brief Sets the width of the selected navigation point indicator of the dot style for the <b><Swiper></b> component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @param value Indicates the width of the navigation point indicator of the dot style.
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetSelectedItemWidth(ArkUI_SwiperIndicator* indicator, float value);

/**
 * @brief Obtains the width of the selected navigation point indicator of the dot style of the <b><Swiper></b>
 * component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @return Returns the width of the navigation point indicator of the dot style.
 * @since 12
*/
float OH_ArkUI_SwiperIndicator_GetSelectedItemWidth(ArkUI_SwiperIndicator* indicator);

/**
 * @brief Sets the height of the selected navigation point indicator of the dot style for the <b><Swiper></b> component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @param value Indicates the height of the navigation point indicator of the dot style.
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetSelectedItemHeight(ArkUI_SwiperIndicator* indicator, float value);

/**
 * @brief Obtains the height of the selected navigation point indicator of the dot style of the <b><Swiper></b>
 * component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @return Returns the height of the navigation point indicator of the dot style.
 * @since 12
*/
float OH_ArkUI_SwiperIndicator_GetSelectedItemHeight(ArkUI_SwiperIndicator* indicator);

/**
 * @brief Sets whether to enable the mask for a navigation point indicator of the dot style for the <b><Swiper></b>
 * component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @param mask Indicates whether to enable the mask. The value <b>1</b> means to enable the mask, and <b>0</b> means
 * the opposite.
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetMask(ArkUI_SwiperIndicator* indicator, int32_t mask);

/**
 * @brief Obtains whether the mask is enabled for a navigation point indicator of the dot style of the <b><Swiper></b>
 * component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @return Returns <b>1</b> if the mask is enabled; returns <b>0</b> otherwise.
 * @since 12
*/
int32_t OH_ArkUI_SwiperIndicator_GetMask(ArkUI_SwiperIndicator* indicator);

/**
 * @brief Sets the color of a navigation point indicator of the dot style for the <b><Swiper></b> component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @param color Indicates the color, in 0xARGB format. For example, 0xFFFF0000 indicates red.
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetColor(ArkUI_SwiperIndicator* indicator, uint32_t color);

/**
 * @brief Obtains the color of a navigation point indicator of the dot style of the <b><Swiper></b> component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @return Returns the color, in 0xARGB format. For example, 0xFFFF0000 indicates red.
 * @since 12
*/
uint32_t OH_ArkUI_SwiperIndicator_GetColor(ArkUI_SwiperIndicator* indicator);

/**
 * @brief Sets the color of the selected navigation point indicator of the dot style for the <b><Swiper></b> component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @param selectedColor Indicates the color, in 0xARGB format. For example, 0xFFFF0000 indicates red.
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetSelectedColor(ArkUI_SwiperIndicator* indicator, uint32_t selectedColor);

/**
 * @brief Obtains the color of the selected navigation point indicator of the dot style of the <b><Swiper></b>
 * component.
 *
 * @param indicator Indicates the pointer to the navigation point indicator.
 * @return Returns the color, in 0xARGB format. For example, 0xFFFF0000 indicates red.
 * @since 12
*/
uint32_t OH_ArkUI_SwiperIndicator_GetSelectedColor(ArkUI_SwiperIndicator* indicator);

/**
 * @brief Sets the maximum number of dots for the navigation point indicator of the dot style.
 *
 * @param indicator Indicates the pointer to a navigation point indicator.
 * @param maxDisplayCount Indicates the maximum number of dots. The value ranges from 6 to 9.
 * @return Returns the result code.
 *         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
 *         Returns {@link ARKUI_ERROR_CODE_PARAM_INVALID} if the value of <b>maxDisplayCount</b> is invalid.
 * @since 12
*/
int32_t OH_ArkUI_SwiperIndicator_SetMaxDisplayCount(ArkUI_SwiperIndicator* indicator, int32_t maxDisplayCount);

/**
 * @brief Obtains the maximum number of dots of a navigation point indicator of the dot style.
 *
 * @param indicator Indicates the pointer to a navigation point indicator.
 * @return Returns the maximum number of dots. The value ranges from 6 to 9.
 * @since 12
*/
int32_t OH_ArkUI_SwiperIndicator_GetMaxDisplayCount(ArkUI_SwiperIndicator* indicator);

/**
 * @brief Creates a <b>ListItemSwipeActionItem</b> instance.
 *
 * @return Returns the created <b>ListItemSwipeActionItem</b> instance.
 * @since 12
*/
ArkUI_ListItemSwipeActionItem* OH_ArkUI_ListItemSwipeActionItem_Create();

/**
* @brief Disposes of a <b>ListItemSwipeActionItem</b> instance.
*
* @param item Indicates the <b>ListItemSwipeActionItem</b> instance to dispose of.
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_Dispose(ArkUI_ListItemSwipeActionItem* item);

/**
* @brief Sets the layout content for a <b>ListItemSwipeActionItem</b> instance.
*
* @param item Indicates the target <b>ListItemSwipeActionItem</b> instance.
* @param node Indicates the layout information.
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetContent(ArkUI_ListItemSwipeActionItem* item, ArkUI_NodeHandle node);

/**
* @brief Sets the swipe distance threshold for deleting the list item.
*
* @param item Indicates the target <b>ListItemSwipeActionItem</b> instance.
* @param distance Indicates the swipe distance threshold for deleting the list item.
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetActionAreaDistance(ArkUI_ListItemSwipeActionItem* item, float distance);

/**
* @brief Obtains the swipe distance threshold for deleting the list item.
*
* @param item Indicates the target <b>ListItemSwipeActionItem</b> instance.
* @return Returns the swipe distance threshold for deleting the list item. Return <b>0</b> if an error occurs.
* @since 12
*/
float OH_ArkUI_ListItemSwipeActionItem_GetActionAreaDistance(ArkUI_ListItemSwipeActionItem* item);

/**
* @brief Sets the callback invoked each time the list item enters the delete area.
*
* @param item Indicates the target <b>ListItemSwipeActionItem</b> instance.
* @param callback Indicates the callback to set.
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetOnEnterActionArea(ArkUI_ListItemSwipeActionItem* item, void (*callback)());

/**
* @brief Sets the callback invoked each time the list item enters the delete area.
*
* @param item Indicates the target <b>ListItemSwipeActionItem</b> instance.
* @param userData Indicates the custom data.
* @param callback Indicates the callback to set.
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetOnEnterActionAreaWithUserData(ArkUI_ListItemSwipeActionItem* item,
    void* userData, void (*callback)(void* userData));

/**
* @brief Sets the callback invoked when the list item is deleted while in the delete area.
*
* @param item Indicates the target <b>ListItemSwipeActionItem</b> instance.
* @param callback Indicates the callback to set.
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetOnAction(ArkUI_ListItemSwipeActionItem* item, void (*callback)());

/**
* @brief Sets the callback invoked when the list item is deleted while in the delete area.
*
* @param item Indicates the target <b>ListItemSwipeActionItem</b> instance.
* @param userData Indicates the custom data.
* @param callback Indicates the callback to set.
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetOnActionWithUserData(ArkUI_ListItemSwipeActionItem* item,
    void* userData, void (*callback)(void* userData));

/**
* @brief Sets the callback invoked each time the list item exits the delete area.
*
* @param item Indicates the target <b>ListItemSwipeActionItem</b> instance.
* @param callback Indicates the callback to set.
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetOnExitActionArea(ArkUI_ListItemSwipeActionItem* item, void (*callback)());

/**
* @brief Sets the callback invoked each time the list item exits the delete area.
*
* @param item Indicates the target <b>ListItemSwipeActionItem</b> instance.
* @param userData Indicates the custom data.
* @param callback Indicates the callback to set.
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetOnExitActionAreaWithUserData(ArkUI_ListItemSwipeActionItem* item,
    void* userData, void (*callback)(void* userData));

/**
* @brief Sets the callback invoked when the swipe state of the list item changes.
*
* @param item Indicates the target <b>ListItemSwipeActionItem</b> instance.
* @param callback Indicates the callback to set.
*        swipeActionState Indicates the state after the change.
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetOnStateChange(ArkUI_ListItemSwipeActionItem* item,
    void (*callback)(ArkUI_ListItemSwipeActionState swipeActionState));

/**
* @brief Sets the callback invoked when the swipe state of the list item changes.
*
* @param item Indicates the target <b>ListItemSwipeActionItem</b> instance.
* @param userData Indicates the custom data.
* @param callback Indicates the callback to set.
*        swipeActionState Indicates the state after the change.
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetOnStateChangeWithUserData(ArkUI_ListItemSwipeActionItem* item,
    void* userData, void (*callback)(ArkUI_ListItemSwipeActionState swipeActionState, void* userData));

/**
 * @brief Creates a <b>ListItemSwipeActionOption</b> instance.
 *
 * @return Returns the created <b>ListItemSwipeActionOption</b> instance.
 * @since 12
*/
ArkUI_ListItemSwipeActionOption* OH_ArkUI_ListItemSwipeActionOption_Create();

/**
* @brief Disposes of a <b>ListItemSwipeActionOption</b> instance.
*
* @param option Indicates the <b>ListItemSwipeActionOption</b> instance to dispose of.
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionOption_Dispose(ArkUI_ListItemSwipeActionOption* option);

/**
* @brief Sets the layout content for the left edge (for a vertical layout) or top edge (for a horizontal layout) of a
* <b>ListItemSwipeActionOption</b> instance.
*
* @param option Indicates the target <b>ListItemSwipeActionItem</b> instance.
* @param builder Indicates the layout information.
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionOption_SetStart(ArkUI_ListItemSwipeActionOption* option, ArkUI_ListItemSwipeActionItem* item);

/**
* @brief Sets the layout content for the right edge (for a vertical layout) or bottom edge (for a horizontal layout) of
* a <b>ListItemSwipeActionOption</b> instance.
*
* @param option Indicates the target <b>ListItemSwipeActionItem</b> instance.
* @param builder Indicates the layout information.
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionOption_SetEnd(ArkUI_ListItemSwipeActionOption* option,
    ArkUI_ListItemSwipeActionItem* item);

/**
* @brief Sets the edge effect.
*
* @param option Indicates the target <b>ListItemSwipeActionItem</b> instance.
* @param edgeEffect Indicates the edge effect.
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionOption_SetEdgeEffect(ArkUI_ListItemSwipeActionOption* option,
    ArkUI_ListItemSwipeEdgeEffect edgeEffect);

/**
* @brief Obtains the edge effect.
*
* @param option Indicates the target <b>ListItemSwipeActionItem</b> instance.
* @return Returns the edge effect. Default value to be returned: <b>ARKUI_LIST_ITEM_SWIPE_EDGE_EFFECT_SPRING</b>
* @since 12
*/
int32_t OH_ArkUI_ListItemSwipeActionOption_GetEdgeEffect(ArkUI_ListItemSwipeActionOption* option);

/**
* @brief Sets the callback invoked when the scroll offset changes.
*
* @param option Indicates the target <b>ListItemSwipeActionItem</b> instance.
* @param callback Indicates the callback to set.
*        offset Indicates the scroll offset.
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionOption_SetOnOffsetChange(ArkUI_ListItemSwipeActionOption* option,
    void (*callback)(float offset));

/**
 * @brief Sets the callback invoked when the scroll offset changes.
 *
 * @param option Indicates the target <b>ListItemSwipeActionItem</b> instance.
 * @param userData Indicates the custom data.
 * @param callback Indicates the callback to set.
 *        offset  Indicates the scroll offset.
 * @since 12
 */
void OH_ArkUI_ListItemSwipeActionOption_SetOnOffsetChangeWithUserData(ArkUI_ListItemSwipeActionOption* option,
    void* userData, void (*callback)(float offset, void* userData));

/**
 * @brief Creates an accessibility state.
 *
 * @return Returns the pointer to the created accessibility state object. If a null pointer is returned, the creation
 *         fails. A possible cause is that the application address space is full.
 * @since 12
 */
ArkUI_AccessibilityState* OH_ArkUI_AccessibilityState_Create(void);

/**
 * @brief Disposes of the pointer to an accessibility state.
 *
 * @param state Indicates the pointer to an accessibility state object.
 * @since 12
 */
void OH_ArkUI_AccessibilityState_Dispose(ArkUI_AccessibilityState* state);

/**
 * @brief Sets whether an accessibility state is disabled.
 *
 * @param state Indicates the pointer to an accessibility state object.
 * @param isDisabled Indicates whether the accessibility state is disabled. The value <b>1</b> means that the
 * accessibility state is disabled, and <b>0</b> means the opposite. The default value is <b>0</b>.
 * @since 12
*/
void OH_ArkUI_AccessibilityState_SetDisabled(ArkUI_AccessibilityState* state, int32_t isDisabled);

/**
 * @brief Obtains whether an accessibility state is disabled.
 *
 * @param state Indicates the pointer to an accessibility state object.
 * @return Returns whether the accessibility state is disabled. The value <b>1</b> means that the accessibility state
 * is disabled, and <b>0</b> means the opposite. The default value is <b>0</b>.
 *         If the value of <b>state</b> is empty, the default value is returned.
 * @since 12
*/
int32_t OH_ArkUI_AccessibilityState_IsDisabled(ArkUI_AccessibilityState* state);

/**
 * @brief Sets whether an accessibility state is selected.
 *
 * @param state Indicates the pointer to an accessibility state object.
 * @param isSelected Indicates whether the accessibility state is selected. The value <b>1</b> means that the
 * accessibility state is selected, and <b>0</b> means the opposite. The default value is <b>0</b>.
 * @since 12
*/
void OH_ArkUI_AccessibilityState_SetSelected(ArkUI_AccessibilityState* state, int32_t isSelected);

/**
 * @brief Obtains whether an accessibility state is selected.
 *
 * @param state Indicates the pointer to an accessibility state object.
 * @return Returns whether the accessibility state is selected. The value <b>1</b> means that the accessibility state
 * is selected, and <b>0</b> means the opposite. The default value is <b>0</b>.
 *         If the value of <b>state</b> is empty, the default value is returned.
 * @since 12
*/
int32_t OH_ArkUI_AccessibilityState_IsSelected(ArkUI_AccessibilityState* state);

/**
 * @brief Sets the check box state of an accessibility state.
 *
 * @param state Indicates the pointer to an accessibility state object.
 * @param checkedState Indicates the check box state. The parameter type is {@link ArkUI_AccessibilityCheckedState}.
 * The default value is <b><b>ARKUI_ACCESSIBILITY_UNCHECKED</b>.
 * @since 12
*/
void OH_ArkUI_AccessibilityState_SetCheckedState(ArkUI_AccessibilityState* state, int32_t checkedState);

/**
 * @brief Obtains the check box state of an accessibility state.
 *
 * @param state Indicates the pointer to an accessibility state object.
 * @return Returns the check box state. The parameter type is {@link ArkUI_AccessibilityCheckedState}. The default value
 * is <b>ARKUI_ACCESSIBILITY_UNCHECKED</b>.
 *         If a parameter error occurs, the default value is returned.
 * @since 12
*/
int32_t OH_ArkUI_AccessibilityState_GetCheckedState(ArkUI_AccessibilityState* state);

/**
 * @brief Creates an <b>AccessibilityValue</b> instance.
 *
 * @return Returns the pointer to the created <b>AccessibilityValue</b> instance.
 * @since 12
*/
ArkUI_AccessibilityValue* OH_ArkUI_AccessibilityValue_Create(void);

/**
* @brief Disposes of the pointer to an <b>AccessibilityValue</b> instance.
*
* @param state Indicates the pointer to an <b>AccessibilityValue</b> instance.
* @since 12
*/
void OH_ArkUI_AccessibilityValue_Dispose(ArkUI_AccessibilityValue* value);

/**
 * @brief Sets the minimum accessibility value.
 *
 * @param value Indicates the pointer to an <b>AccessibilityValue</b> instance.
 * @param min Indicates the minimum value based on the range component. The default value is <b>-1</b>.
 * @since 12
*/
void OH_ArkUI_AccessibilityValue_SetMin(ArkUI_AccessibilityValue* value, int32_t min);

/**
 * @brief Obtains the minimum accessibility value.
 *
 * @param value Indicates the pointer to an <b>AccessibilityValue</b> instance.
 * @return Returns the minimum value based on the range component. The default value is <b>-1</b>.
 *         If a parameter error occurs, <b>-1</b> is returned.
 * @since 12
*/
int32_t OH_ArkUI_AccessibilityValue_GetMin(ArkUI_AccessibilityValue* value);

/**
 * @brief Sets the maximum accessibility value.
 *
 * @param value Indicates the pointer to an <b>AccessibilityValue</b> instance.
 * @param max Indicates the maximum value based on the range component. The default value is <b>-1</b>.
 * @since 12
*/
void OH_ArkUI_AccessibilityValue_SetMax(ArkUI_AccessibilityValue* value, int32_t max);

/**
 * @brief Obtains the maximum accessibility value.
 *
 * @param value Indicates the pointer to an <b>AccessibilityValue</b> instance.
 * @return Returns the maximum value based on the range component. The default value is <b>-1</b>.
 *         If a parameter error occurs, <b>-1</b> is returned.
 * @since 12
*/
int32_t OH_ArkUI_AccessibilityValue_GetMax(ArkUI_AccessibilityValue* value);

/**
 * @brief Sets the current accessibility value.
 *
 * @param value Indicates the pointer to an <b>AccessibilityValue</b> instance.
 * @param current Indicates the current value based on the range component. The default value is <b>-1</b>.
 * @since 12
 */
void OH_ArkUI_AccessibilityValue_SetCurrent(ArkUI_AccessibilityValue* value, int32_t current);

/**
 * @brief Obtains the current accessibility value.
 *
 * 
 * @param value Indicates the pointer to an <b>AccessibilityValue</b> instance.
 * @return Returns the current value based on the range component. The default value is <b>-1</b>.
 *         If a parameter error occurs, <b>-1</b> is returned.
 * @since 12
 */
int32_t OH_ArkUI_AccessibilityValue_GetCurrent(ArkUI_AccessibilityValue* value);

/**
 * @brief Sets the text description of an <b>AccessibilityValue</b> instance.
 *
 * @param value Indicates the pointer to an <b>AccessibilityValue</b> instance.
 * @param text Indicates the text description. The default value is an empty string.
 * @since 12
 */
void OH_ArkUI_AccessibilityValue_SetText(ArkUI_AccessibilityValue* value, const char* text);

/**
 * @brief Obtains the text description of an <b>AccessibilityValue</b> instance.
 *
 * 
 * @param value Indicates the pointer to an <b>AccessibilityValue</b> instance.
 * @return Returns the text description. The default value is an empty string.
 *         If a parameter error occurs, a null pointer is returned.
 * @since 12
 */
const char* OH_ArkUI_AccessibilityValue_GetText(ArkUI_AccessibilityValue* value);

/**
 * @brief Creates an image frame information object based on an image path, with the image format being SVG, PNG, or
 * JPG.
 *
 * @param src Indicates the image path.
 * @return Returns the pointer to the created image frame information object.
 * @since 12
 */
ArkUI_ImageAnimatorFrameInfo* OH_ArkUI_ImageAnimatorFrameInfo_CreateFromString(char* src);

/**
 * @brief Creates an image frame information object based on a <b>DrawableDescriptor</b> object, with the image format
 * being Resource or PixelMap.
 *
 * @param drawable Indicates the pointer to an <b>ArkUI_DrawableDescriptor</b> object created using Resource or
 * PixelMap.
 * @return Returns the pointer to the created image frame information object.
 * @since 12
 */
ArkUI_ImageAnimatorFrameInfo* OH_ArkUI_ImageAnimatorFrameInfo_CreateFromDrawableDescriptor(
    ArkUI_DrawableDescriptor* drawable);

/**
 * @brief Disposes of the pointer to an image frame information object.
 *
 * @param imageInfo Indicates the pointer to an image frame information object.
 * @since 12
 */
void OH_ArkUI_ImageAnimatorFrameInfo_Dispose(ArkUI_ImageAnimatorFrameInfo* imageInfo);

/**
  * @brief Sets the width for an image.
 *
 * @param imageInfo Indicates the pointer to an image frame information object.
 * @param width Indicates the image width, in PX.
 * @since 12
 */
void OH_ArkUI_ImageAnimatorFrameInfo_SetWidth(ArkUI_ImageAnimatorFrameInfo* imageInfo, int32_t width);

/**
  * @brief Obtains the width of an image.
 *
 * @param imageInfo Indicates the pointer to an image frame information object.
 * @return Returns the image width, in PX. If <b>imageInfo</b> is a null pointer, <b>0</b> is returned.
 * @since 12
 */
int32_t OH_ArkUI_ImageAnimatorFrameInfo_GetWidth(ArkUI_ImageAnimatorFrameInfo* imageInfo);

/**
  * @brief Sets the height for an image.
 *
 * @param imageInfo Indicates the pointer to an image frame information object.
 * @param height Indicates the image height, in PX.
 * @since 12
 */
void OH_ArkUI_ImageAnimatorFrameInfo_SetHeight(ArkUI_ImageAnimatorFrameInfo* imageInfo, int32_t height);

/**
  * @brief Obtains the height of an image.
 *
 * @param imageInfo Indicates the pointer to an image frame information object.
 * @return Returns the image height, in PX. If <b>imageInfo</b> is a null pointer, <b>0</b> is returned.
 * @since 12
 */
int32_t OH_ArkUI_ImageAnimatorFrameInfo_GetHeight(ArkUI_ImageAnimatorFrameInfo* imageInfo);

/**
 * @brief Sets the vertical coordinate of an image relative to the upper left corner of the component.
 *
 * @param imageInfo Indicates the pointer to an image frame information object.
 * @param top Indicates the vertical coordinate of the image relative to the upper left corner of the component, in PX.
 * @since 12
 */
void OH_ArkUI_ImageAnimatorFrameInfo_SetTop(ArkUI_ImageAnimatorFrameInfo* imageInfo, int32_t top);

/**
 * @brief Obtains the vertical coordinate of an image relative to the upper left corner of the component.
 *
 * @param imageInfo Indicates the pointer to an image frame information object.
 * @return Returns the vertical coordinate of the image relative to the upper left corner of the component, in PX.
 * If <b>imageInfo</b> is a null pointer, <b>0</b> is returned.
 * @since 12
 */
int32_t OH_ArkUI_ImageAnimatorFrameInfo_GetTop(ArkUI_ImageAnimatorFrameInfo* imageInfo);

/**
 * @brief Sets the horizontal coordinate of an image relative to the upper left corner of the component.
 *
 * @param imageInfo Indicates the pointer to an image frame information object.
 * @param left Indicates the horizontal coordinate of the image relative to the upper left corner of the component,
 * in PX.
 * @since 12
 */
void OH_ArkUI_ImageAnimatorFrameInfo_SetLeft(ArkUI_ImageAnimatorFrameInfo* imageInfo, int32_t left);

/**
 * @brief Obtains the horizontal coordinate of an image relative to the upper left corner of the component.
 *
 * @param imageInfo Indicates the pointer to an image frame information object.
 * @return Returns the horizontal coordinate of the image relative to the upper left corner of the component, in PX.
 * If <b>imageInfo</b> is a null pointer, <b>0</b> is returned.
 * @since 12
 */
int32_t OH_ArkUI_ImageAnimatorFrameInfo_GetLeft(ArkUI_ImageAnimatorFrameInfo* imageInfo);

/**
 * @brief Sets the playback duration of an image.
 *
 * @param imageInfo Indicates the pointer to an image frame information object.
 * @param duration Indicates the playback duration of an image, in milliseconds.
 * @since 12
 */
void OH_ArkUI_ImageAnimatorFrameInfo_SetDuration(ArkUI_ImageAnimatorFrameInfo* imageInfo, int32_t duration);

/**
 * @brief Obtains the playback duration of an image.
 *
 * @param imageInfo Indicates the pointer to an image frame information object.
 * @return Returns the playback duration of the image, in milliseconds. If <b>imageInfo</b> is a null pointer,
 * <b>0</b> is returned.
 * @since 12
 */
int32_t OH_ArkUI_ImageAnimatorFrameInfo_GetDuration(ArkUI_ImageAnimatorFrameInfo* imageInfo);

/**
 * @brief Creates a <b>ListChildrenMainSize</b> instance.
 *
 * @return Returns the created <b>ListChildrenMainSize</b> instance.
 * @since 12
 */
ArkUI_ListChildrenMainSize* OH_ArkUI_ListChildrenMainSizeOption_Create();

/**
 * @brief Disposes of a <b>ListChildrenMainSize</b> instance.
 *
 * @param option Indicates the <b>ListChildrenMainSize</b> instance to dispose of.
 * @since 12
 */
void OH_ArkUI_ListChildrenMainSizeOption_Dispose(ArkUI_ListChildrenMainSize* option);

/**
 * @brief Sets the default size in a <b>ListChildrenMainSize</b> instance.
 *
 * @param option Indicates the target <b>ListChildrenMainSize</b> instance.
 * @param defaultMainSize Indicates the default size to set, in vp.
 * @return Returns the result code.
 *         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
 *         Returns {@link ARKUI_ERROR_CODE_PARAM_INVALID} if a parameter error occurs.
 * @since 12
 */
int32_t OH_ArkUI_ListChildrenMainSizeOption_SetDefaultMainSize(ArkUI_ListChildrenMainSize* option, float defaultMainSize);

/**
 * @brief Obtains the default size in a <b>ListChildrenMainSize</b> instance.
 *
 * @param option Indicates the target <b>ListChildrenMainSize</b> instance.
 * @return Returns the default size, in vp. The default value is <b>0</b>. If <b>option</b> is a null pointer,
 *         <b>-1</b> is returned.
 * @since 12
*/
float OH_ArkUI_ListChildrenMainSizeOption_GetDefaultMainSize(ArkUI_ListChildrenMainSize* option);

/**
 * @brief Resets the array size in a <b>ListChildrenMainSize</b> instance.
 *
 * @param option Indicates the target <b>ListChildrenMainSize</b> instance.
 * @param totalSize Indicates the array size.
 * @since 12
*/
void OH_ArkUI_ListChildrenMainSizeOption_Resize(ArkUI_ListChildrenMainSize* option, int32_t totalSize);

/**
 * @brief Changes the content of a <b>ChildrenMainSizeOption</b> array.
 *
 * @param option Indicates the target <b>ListChildrenMainSize</b> instance.
 * @param index Indicates the position starting from which you want to change the array.
 * @param deleteCount Indicates the number of elements in the array to remove from the position specified by
 * <b>index</b>.
 * @param deleteCount Indicates the number of elements to add to the array from the position specified by <b>index</b>.
 * @return Returns the result code.
 *         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
 *         Returns {@link ARKUI_ERROR_CODE_PARAM_INVALID} if a parameter error occurs.
 * @since 12
*/
int32_t OH_ArkUI_ListChildrenMainSizeOption_Splice(ArkUI_ListChildrenMainSize* option, int32_t index, int32_t deleteCount, int32_t addCount);

/**
 * @brief Updates the values of a <b>ChildrenMainSizeOption</b> array.
 *
 * @param option Indicates the target <b>ListChildrenMainSize</b> instance.
 * @param index Indicates the position starting from which you want to change the array.
 * @param mainSize Indicates the values to use.
 * @return Returns the result code.
 *         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
 *         Returns {@link ARKUI_ERROR_CODE_PARAM_INVALID} if a parameter error occurs.
 * @since 12
*/
int32_t OH_ArkUI_ListChildrenMainSizeOption_UpdateSize(ArkUI_ListChildrenMainSize* option, int32_t index, float mainSize);

/**
 * @brief Obtains the values of a <b>ChildrenMainSizeOption</b> array.
 *
 * @param option Indicates the target <b>ListChildrenMainSize</b> instance.
 * @param index Indicates the position of the value to be obtained.
 * @return Returns the value at the specified position. If a parameter error occurs, <b>-1</b> is returned.
 * @since 12
*/
float OH_ArkUI_ListChildrenMainSizeOption_GetMainSize(ArkUI_ListChildrenMainSize* option, int32_t index);

/**
 * @brief Creates measurement information for this custom span.
 *
 * @return Returns a <b>CustomSpanMeasureInfo</b> instance.
 * If a null pointer is returned, the memory may be insufficient.
 * @since 12
*/
ArkUI_CustomSpanMeasureInfo* OH_ArkUI_CustomSpanMeasureInfo_Create(void);

/**
 * @brief Disposes of measurement information of this custom span.
 *
 * @param info Indicates the pointer to the measurement information of a custom span.
 * @since 12
*/
void OH_ArkUI_CustomSpanMeasureInfo_Dispose(ArkUI_CustomSpanMeasureInfo* info);

/**
 * @brief Obtains the font size of the parent text node of a custom span.
 *
 * @param info Indicates the pointer to the measurement information of a custom span.
 * @return Returns the font size of the parent text node. If a parameter error occurs, <b>0.0f</b> is returned.
 * The return value of <b>0.0f</b> indicates a failure in parameter validation; the parameter must not be null.
 * @since 12
*/
float OH_ArkUI_CustomSpanMeasureInfo_GetFontSize(ArkUI_CustomSpanMeasureInfo* info);

/**
 * @brief Creates measurement metrics for this custom span.
 *
 * @return Returns a <b>CustomSpanMetrics</b> instance.
 * If a null pointer is returned, the memory may be insufficient.
 * @since 12
*/
ArkUI_CustomSpanMetrics* OH_ArkUI_CustomSpanMetrics_Create(void);

/**
 * @brief Disposes of measurement metrics of this custom span.
 *
 * @param metrics Indicates the pointer to a <b>CustomSpanMetrics</b> instance.
 * @since 12
*/
void OH_ArkUI_CustomSpanMetrics_Dispose(ArkUI_CustomSpanMetrics* metrics);

/**
 * @brief Sets the width for a custom span.
 *
 * @param metrics Indicates the pointer to a <b>CustomSpanMetrics</b> instance.
 * @param width Indicates the width, in vp.
 * @return Returns the result code.
 *         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
 *         Returns {@link ARKUI_ERROR_CODE_PARAM_INVALID} if a parameter error occurs. The parameter must not be null.
 * @since 12
*/
int32_t OH_ArkUI_CustomSpanMetrics_SetWidth(ArkUI_CustomSpanMetrics* metrics, float width);

/**
 * @brief Sets the height for a custom span.
 *
 * @param metrics Indicates the pointer to a <b>CustomSpanMetrics</b> instance.
 * @param height Indicates the height, in vp.
 * @return Returns the result code.
 *         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
 *         Returns {@link ARKUI_ERROR_CODE_PARAM_INVALID} if a parameter error occurs. The parameter must not be null.
 * @since 12
*/
int32_t OH_ArkUI_CustomSpanMetrics_SetHeight(ArkUI_CustomSpanMetrics* metrics, float height);

/**
 * @brief Creates drawing information for this custom span.
 *
 * @return Returns a <b>CustomSpanDrawInfo</b> instance.
 * If a null pointer is returned, the memory may be insufficient.
 * @since 12
*/
ArkUI_CustomSpanDrawInfo* OH_ArkUI_CustomSpanDrawInfo_Create(void);

/**
 * @brief Disposes of drawing information for this custom span.
 *
 * @param info Indicates the pointer to the drawing information of a custom span.
 * @since 12
*/
void OH_ArkUI_CustomSpanDrawInfo_Dispose(ArkUI_CustomSpanDrawInfo* info);

/**
 * @brief Obtains the x-axis offset of the custom span relative to the mounted component.
 *
 * @param info Indicates the pointer to the drawing information of a custom span.
 * @return Returns the x-axis offset. If a parameter error occurs, <b>0.0f</b> is returned.
 * The return value of <b>0.0f</b> indicates a failure in parameter validation; the parameter must not be null.
 * @since 12
*/
float OH_ArkUI_CustomSpanDrawInfo_GetXOffset(ArkUI_CustomSpanDrawInfo* info);

/**
 * @brief Obtains the top margin of the custom span relative to the mounted component.
 *
 * @param info Indicates the pointer to the drawing information of a custom span.
 * @return Returns the top margin. If a parameter error occurs, <b>0.0f</b> is returned.
 *         The return value of <b>0.0f</b> indicates a failure in parameter validation; the parameter must not be null.
 * @since 12
*/
float OH_ArkUI_CustomSpanDrawInfo_GetLineTop(ArkUI_CustomSpanDrawInfo* info);

/**
 * @brief Obtains the bottom margin of the custom span relative to the mounted component.
 *
 * @param info Indicates the pointer to the drawing information of a custom span.
 * @return Returns the bottom margin. If a parameter error occurs, <b>0.0f</b> is returned.
 *         The return value of <b>0.0f</b> indicates a failure in parameter validation; the parameter must not be null.
 * @since 12
*/
float OH_ArkUI_CustomSpanDrawInfo_GetLineBottom(ArkUI_CustomSpanDrawInfo* info);

/**
 * @brief Obtains the baseline offset of the custom span relative to the mounted component.
 *
 * @param info Indicates the pointer to the drawing information of a custom span.
 * @return Returns the baseline offset. If a parameter error occurs, <b>0.0f</b> is returned.
 *         The return value of <b>0.0f</b> indicates a failure in parameter validation; the parameter must not be null.
 * @since 12
*/
float OH_ArkUI_CustomSpanDrawInfo_GetBaseline(ArkUI_CustomSpanDrawInfo* info);

/**
 * @brief Destroys a <b>CustomProperty</b> instance.
 *
 * @param handle Indicates the <b>CustomProperty</b> instance to be destroyed.
 * @since 14
 */
void OH_ArkUI_CustomProperty_Destroy(ArkUI_CustomProperty* handle);

/**
 * @brief Obtains the value of a custom property.
 *
 * 
 * @param handle Indicates the pointer to the custom property object.
 * @return Returns the value of the custom property.
 * @since 14
 */
const char* OH_ArkUI_CustomProperty_GetStringValue(ArkUI_CustomProperty* handle);

/**
 * @brief Destroys an <b>ActiveChildrenInfo</b> instance.
 *
 * @param handle Indicates the <b>ActiveChildrenInfo</b> instance to be destroyed.
 * @since 14
 */
void OH_ArkUI_ActiveChildrenInfo_Destroy(ArkUI_ActiveChildrenInfo* handle);

/**
 * @brief Obtains the child node at the specified index in the specified <b>ActiveChildrenInfo</b> instance.
 *
 * @param handle Indicates the <b>ActiveChildrenInfo</b> instance from which to obtain the information.
 * @since 14
 */
ArkUI_NodeHandle OH_ArkUI_ActiveChildrenInfo_GetNodeByIndex(ArkUI_ActiveChildrenInfo* handle, int32_t index);

/**
 * @brief Obtains the number of nodes in the specified <b>ActiveChildrenInfo</b> instance.
 *
 * @param handle Indicates the <b>ActiveChildrenInfo</b> instance from which to obtain the information.
 * @since 14
 */
int32_t OH_ArkUI_ActiveChildrenInfo_GetCount(ArkUI_ActiveChildrenInfo* handle);
#ifdef __cplusplus
};
#endif

#endif // ARKUI_NATIVE_TYPE_H
/** @} */
