/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdiInput
 * @{
 *
 * @brief Provides APIs for the upper-layer input service.
 *
 * Using the APIs provided by the Input module, input service developers can implement the following functions: enabling or disabling input devices, obtaining input events, querying device information, registering callback functions, and managing the feature status.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file InputTypes.idl
 *
 * @brief Defines input device-specific data types.
 *
 * This module defines the structures used by the driver APIs for the input service, including the device information, device attributes, and device capabilities.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the Input module APIs.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.input.v1_0;

/**
 * @brief Describes the input device.
 */
struct DevDesc {
    /** Device index */
    unsigned int devIndex;

    /** Device type */
    unsigned int devType;
};

/**
 * @brief Describes identification information of the input device.
 */
struct DevIdentify {
    /** Bus type */
    unsigned short busType;

    /** Vendor ID */
    unsigned short vendor;

    /** Product ID */
    unsigned short product;

    /** Version */
    unsigned short version;
};

/**
 * @brief Describes dimension information of the input device.
 */
struct DimensionInfo {
    /** Axis */
    int axis;

    /** Minimum value of each coordinate */
    int min;

    /** Maximum value of each coordinate */
    int max;

    /** Resolution of each coordinate */
    int fuzz;

    /** Reference value of each coordinate */
    int flat;

    /** Range */
    int range;
};

/**
 * @brief Describes input device attributes.
 */
struct DevAttr {
    /** Device name */
    String devName;

    /** Device identification information */
    struct DevIdentify id;

    /** Device dimension information */
    struct DimensionInfo[] axisInfo;
};

/**
 * @brief Describes the input device ability for storing bitmaps that record supported event types.
 *
 * A bit is used to indicate the type of events that can be reported by the input device.
 *
 */
struct DevAbility {
    /** Device properties */
    unsigned long[] devProp;

    /** Bitmap for recording the supported event types */
    unsigned long[] eventType;

    /** Bitmap for recording the supported absolute coordinates */
    unsigned long[] absCode;

    /** Bitmap for recording the supported relative coordinates */
    unsigned long[] relCode;

    /** Bitmap for recording the supported keycodes */
    unsigned long[] keyCode;

    /** Bitmap for recording the supported indicators */
    unsigned long[] ledCode;

    /** Bitmap for recording other supported functions */
    unsigned long[] miscCode;

    /** Bitmap for recording supported sounds or alerts */
    unsigned long[] soundCode;

    /** Bitmap for recording the supported force functions */
    unsigned long[] forceCode;

    /** Bitmap for recording the supported switch functions */
    unsigned long[] switchCode;

    /** Bitmap of the key type */
    unsigned long[] keyType;

    /** Bitmap of the LED type */
    unsigned long[] ledType;

    /** Bitmap of the sound type */
    unsigned long[] soundType;

    /** Bitmap of the switch type */
    unsigned long[] switchType;
};

/**
 * @brief Describes basic device information of the input device.
 */
struct DeviceInfo {
    /** Device index */
    unsigned int devIndex;

    /** Device type */
    unsigned int devType;

    /** Driver chip information */
    String chipInfo;

    /** Module vendor name */
    String vendorName;

    /** Driver chip model */
    String chipName;

    /** Device attributes */
    struct DevAttr attrSet;

    /** Device abilities */
    struct DevAbility abilitySet;
};

/**
 * @brief Defines the data structure of the extra command.
 */
struct ExtraCmd {
    /** Command code */
    String cmdCode;

    /** Data transmitted by the command */
    String cmdValue;
};

/**
 * @brief Defines the data packet structure for hot plug events.
 */
struct HotPlugEvent {
    /** Device index */
    unsigned int devIndex;

    /** Device type */
    unsigned int devType;

    /** Device status
     *
     * 1: offline
     * 0: online
     *
     */
    unsigned int status;
};

/**
 * @brief Defines the data packet structure for input events.
 */
struct EventPackage {
    /** Input event type */
    unsigned int type;

    /** Input event code */
    unsigned int code;

    /** Value corresponding to the input event code */
    int value;

    /** Timestamp corresponding to the input event */
    unsigned long timestamp;
};
/** @} */
