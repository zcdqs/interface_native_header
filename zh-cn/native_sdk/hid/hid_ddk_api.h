/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef HID_DDK_API_H
#define HID_DDK_API_H

/**
 * @addtogroup HidDdk
 * @{
 *
 * @brief 提供HID DDK接口，包括创建设备、发送事件、销毁设备。
 *
 * @syscap SystemCapability.Driver.HID.Extension
 * @since 11
 * @version 1.0
 */

/**
 * @file hid_ddk_api.h
 *
 * @brief 声明主机侧访问输入设备的HID DDK接口。
 *
 * 引用文件：<hid/hid_ddk_api.h>
 * @since 11
 * @version 1.0
 */

#include <stdint.h>
#include "hid_ddk_types.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @brief 创建设备。
 *
 * @permission ohos.permission.ACCESS_DDK_HID
 * @param hidDevice 创建设备需要的基本信息，包括设备名、厂商ID、产品ID等。
 * @param hidEventProperties 创建的设备关注的事件，包括事件类型、按键事件属性、绝对坐标事件属性、相对坐标事件属性等。
 * @return deviceID (一个非负的数字) 调用接口成功。
 *         {@link HID_DDK_NO_PERM} 权限校验失败。
 *         {@link HID_DDK_INVALID_OPERATION} 连接hid_ddk服务失败。
 *         {@link HID_DDK_INVALID_PARAMETER} 参数校验失败。可能的原因：1. 入参hidDevice为空指针;\n
 *         2. 入参hidEventProperties为空指针; 3. properties的长度超过7; 4. hidEventTypes的长度超过5;\n
 *         5. hidKeys的长度超过100; 6. hidAbs的长度超过26; 7.hidRelBits的长度超过13;\n
 *         8. hidMiscellaneous的长度超过6。
 *         {@link HID_DDK_FAILURE} 设备数量达到最大值200。
 * @since 11
 * @version 1.03
 */
int32_t OH_Hid_CreateDevice(Hid_Device *hidDevice, Hid_EventProperties *hidEventProperties);

/**
 * @brief 向指定设备发送事件列表。
 *
 * @permission ohos.permission.ACCESS_DDK_HID
 * @param deviceId 设备ID。
 * @param items 发送的事件列表，事件包括类型（取值来源事件类型Hid_EventType）、编码（取值来源同步事件编码Hid_SynEvent、键值编码Hid_KeyCode、按钮编码HidBtnCode、
 * 绝对坐标编码Hid_AbsAxes、相对坐标编码Hid_RelAxes、其它类型的输入事件编码Hid_MscEvent）、值（根据实际设备输入决定）。
 * @param length 发送事件列表长度（一次发送事件个数）。
 * @return {@link HID_DDK_SUCCESS} 调用接口成功。
 *         {@link HID_DDK_NO_PERM} 权限校验失败。
 *         {@link HID_DDK_INVALID_OPERATION} 连接hid_ddk服务失败或者调用方不是设备的创建者。
 *         {@link HID_DDK_INVALID_PARAMETER} 参数校验失败。可能的原因: 1.设备ID小于0;\n
 *         2.入参length长度超过7; 3.入参items为空指针。
 *         {@link HID_DDK_NULL_PTR} 对应设备的注入为空。
 *         {@link HID_DDK_FAILURE} 对应设备不存在。
 * @since 11
 * @version 1.0
 */
int32_t OH_Hid_EmitEvent(int32_t deviceId, const Hid_EmitItem items[], uint16_t length);

/**
 * @brief 销毁设备。
 *
 * @permission ohos.permission.ACCESS_DDK_HID
 * @param deviceId 设备ID。
 * @return {@link HID_DDK_SUCCESS} 调用接口成功。
 *         {@link HID_DDK_NO_PERM} 权限校验失败。
 *         {@link HID_DDK_INVALID_OPERATION} 连接hid_ddk服务失败或者调用方不是设备的创建者。
 *         {@link HID_DDK_FAILURE} 对应设备不存在。
 * @since 11
 * @version 1.0
 */
int32_t OH_Hid_DestroyDevice(int32_t deviceId);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif // HID_DDK_API_H