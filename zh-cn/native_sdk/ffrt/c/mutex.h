/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 /**
 * @addtogroup FFRT
 * @{
 *
 * @brief FFRT（Function Flow运行时）是支持Function Flow编程模型的软件运行时库，用于调度执行开发者基于Function Flow编程模型开发的应用。
 *
 *
 * @syscap SystemCapability.Resourceschedule.Ffrt.Core
 *
 * @since 10
 */

 /**
 * @file mutex.h
 *
 * @brief 声明mutex提供的C接口。
 *
 * @since 10
 */
#ifndef FFRT_API_C_MUTEX_H
#define FFRT_API_C_MUTEX_H
#include "type_def.h"

/**
 * @brief 初始化mutex。
 *
 * @param mutex mutex指针。
 * @param attr mutex属性。
 * @return 初始化mutex成功返回ffrt_success，
           初始化mutex失败返回ffrt_error或ffrt_error_inval。
 * @since 10
 */
FFRT_C_API int ffrt_mutex_init(ffrt_mutex_t* mutex, const ffrt_mutexattr_t* attr);

/**
 * @brief 获取mutex。
 *
 * @param mutex mutex指针。
 * @return 获取mutex成功返回ffrt_success，
           获取mutex失败返回ffrt_error_inval， 或者阻塞当前任务。
 * @since 10
 */
FFRT_C_API int ffrt_mutex_lock(ffrt_mutex_t* mutex);

/**
 * @brief 释放mutex。
 *
 * @param mutex mutex指针。
 * @return 释放mutex成功返回ffrt_success，
           释放mutex失败返回ffrt_error_inval。
 * @since 10
 */
FFRT_C_API int ffrt_mutex_unlock(ffrt_mutex_t* mutex);

/**
 * @brief 尝试获取mutex。
 *
 * @param mutex mutex指针。
 * @return 获取mutex成功返回ffrt_success，
           获取mutex失败返回ffrt_error_inval或ffrt_error_busy。
 * @since 10
 */
FFRT_C_API int ffrt_mutex_trylock(ffrt_mutex_t* mutex);

/**
 * @brief 销毁mutex。
 *
 * @param mutex mutex指针。
 * @return 销毁mutex成功返回ffrt_success，
           销毁mutex失败返回ffrt_error_inval。
 * @since 10
 */
FFRT_C_API int ffrt_mutex_destroy(ffrt_mutex_t* mutex);
/** @} */
#endif
