/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Core
 * @{
 *
 * @brief Core模块提供用于播放框架的基础骨干能力，包含内存、错误码、格式载体等相关函数。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 *
 * @since 9
 * @version 1.0
 */

/**
 * @file native_avmemory.h
 *
 * @brief 声明了AVMemory的函数接口。
 *
 * @since 9
 * @version 1.0
 */

#ifndef NATIVE_AVMEMORY_H
#define NATIVE_AVMEMORY_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct OH_AVMemory OH_AVMemory;

/**
 * @brief 获取入参的内存虚拟地址。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param mem 指向OH_AVMemory实例的指针
 * @return 如果内存有效，返回内存的虚拟地址；如果内存无效，返回nullptr。\n
 * 可能的失败原因：1.输入mem为空指针；2.输入mem参数格式校验失败；3.输入mem中内存为空指针。
 * @since 9
 * @version 1.0
 */
uint8_t *OH_AVMemory_GetAddr(struct OH_AVMemory *mem);

/**
 * @brief 获取入参的内存长度。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param mem 指向OH_AVMemory实例的指针
 * @return 如果内存有效，返回内存长度；如果内存无效，返回-1。\n
 * 可能的失败原因：1.输入mem为空指针；2.输入mem参数格式校验失败；3.输入mem中内存为空指针。
 * @since 9
 * @version 1.0
 */
int32_t OH_AVMemory_GetSize(struct OH_AVMemory *mem);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVMEMORY_H