/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup VideoDecoder
 * @{
 * 
 * @brief VideoDecoder模块提供用于视频解码的函数。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @since 9
 * @version 1.0
 */


/**
 * @file native_avcodec_videodecoder.h
 * 
 * @brief 声明用于视频解码的Native API。
 * 
 * @library libnative_media_vdec.so
 * @since 9
 * @version 1.0
 */

#ifndef NATIVE_AVCODEC_VIDEODECODER_H
#define NATIVE_AVCODEC_VIDEODECODER_H

#include <stdint.h>
#include <stdio.h>
#include "native_avcodec_base.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 根据MIME类型创建视频解码器实例，大多数情况下建议使用。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param mime MIME类型描述字符串，请参阅{@link AVCODEC_MIME_TYPE}。
 * @return 返回一个指向视频解码实例的指针。
 * 当传入的解码器类型不支持或者内存资源耗尽时，返回NULL。
 * @since 9
 * @version 1.0
 */
OH_AVCodec *OH_VideoDecoder_CreateByMime(const char *mime);

/**
 * @brief 根据视频解码器名称创建视频解码器实例。使用此接口的前提是知道解码器的确切名称，解码器的名称可以通过能力查询获取。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param name 视频解码器名称。
 * @return 返回指向视频解码实例的指针。
 * 当输入的解码器名称不支持或者内存资源耗尽时，返回NULL。
 * @since 9
 * @version 1.0
 */
OH_AVCodec *OH_VideoDecoder_CreateByName(const char *name);

/**
 * @brief 清理解码器内部资源，销毁解码器实例。不能重复销毁。
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非解码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@link AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。
 * @since 9
 * @version 1.0
 */

OH_AVErrCode OH_VideoDecoder_Destroy(OH_AVCodec *codec);

/**
 * @brief 设置异步回调函数，让应用可以响应视频解码器生成的事件。在调用OH_VideoDecoder_Prepare接口之前，必须调用此接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param callback 所有回调函数的集合，请参阅{@link OH_AVCodecAsyncCallback}。
 * @param userData 调用者执行回调所依赖的数据。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非解码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@link AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。\n
 *         {@link AV_ERR_INVALID_STATE}本接口必须在OH_VideoDecoder_Prepare接口前调用，如果在其他状态时调用，则返回此错误码。
 * @deprecated since 11
 * @useinstead OH_VideoDecoder_RegisterCallback
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_SetCallback(OH_AVCodec *codec, OH_AVCodecAsyncCallback callback, void *userData);

/**
 * @brief 注册异步回调函数，让应用可以响应视频解码器生成的事件。在调用OH_VideoDecoder_Prepare接口之前，必须调用此接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码器实例的指针。
 * @param callback 所有回调函数的集合，请参见{@link OH_AVCodecCallback}。
 * @param userData 调用者执行回调所依赖的数据。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非解码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@link AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。\n
 *         {@link AV_ERR_INVALID_STATE}本接口必须在OH_VideoDecoder_Prepare接口前调用，如果在其他状态时调用，则返回此错误码。
 * @since 11
 */
OH_AVErrCode OH_VideoDecoder_RegisterCallback(OH_AVCodec *codec, OH_AVCodecCallback callback, void *userData);

/**
 * @brief 设置输出surface以提供视频解码输出。在初始化阶段，必须在调用OH_VideoDecoder_Prepare接口之前调用此接口。在Running状态可以直接调用该接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param window 指向OHNativeWindow实例的指针，请参阅{@link OHNativeWindow}。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_OPERATE_NOT_PERMIT}本接口仅支持在Surface模式下调用, 如果在Buffer模式调用, 则返回此错误码。\n
 *         {@link AV_ERR_INVALID_VAL}1. 输入的codec指针为非解码器实例，或者为空指针；2. window为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@link AV_ERR_INVALID_STATE}解码器状态不支持调用本接口时调用。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_SetSurface(OH_AVCodec *codec, OHNativeWindow *window);

/**
 * @brief 配置视频解码器，通常需要配置解码视频的描述信息，这些信息可以从OH_AVSource中提取。在调用OH_VideoDecoder_Prepare接口之前，必须调用此接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param format 指向OH_AVFormat的指针，用于给出要解码的视频轨道的描述。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_INVALID_VAL}1. 输入的codec指针为非解码器实例，或者为空指针；2. 输入format参数不支持。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。\n
 *         {@link AV_ERR_INVALID_STATE}本接口必须在OH_VideoDecoder_Prepare接口前调用，如果在其他状态时调用，则返回此错误码。\n
 *         {@link AV_ERR_VIDEO_UNSUPPORTED_COLOR_SPACE_CONVERSION}不支持色彩空间转换功能。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_Configure(OH_AVCodec *codec, OH_AVFormat *format);

/**
 * @brief 准备解码器的内部资源，在调用该接口之前，必须调用OH_VideoDecoder_Configure接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非解码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@link AV_ERR_INVALID_STATE}解码器状态不支持调用本接口时调用。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}1. 内部执行错误；2. 配置了色彩空间转换功能，但解码器处于Buffer模式。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_Prepare(OH_AVCodec *codec);

/**
 * @brief 调用OH_VideoDecoder_Prepare接口成功后调用此接口启动解码器。成功启动后，解码器将开始报告注册的回调事件。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非解码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@link AV_ERR_INVALID_STATE}解码器状态不支持调用本接口时调用。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}1. 内部执行错误；2. 视频色彩空间转换功能已配置，但是没有调用OH_VideoDecoder_Prepare接口。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_Start(OH_AVCodec *codec);

/**
 * @brief 停止解码器，释放输入输出buffer。停止后，可以通过调用OH_VideoDecoder_Start接口重新进入Executing状态。
 * 
 * 但需要注意的是，如果编解码器特定数据以前已输入到解码器，则需要再次输入。
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非解码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@link AV_ERR_INVALID_STATE}解码器状态不支持调用本接口时调用。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_Stop(OH_AVCodec *codec);

/**
 * @brief  清除解码器中缓存的输入和输出数据及参数集如H264格式的PPS/SPS。
 * 调用此接口后，以前通过异步回调上报的所有缓冲区index都将失效，请确保不要访问这些index对应的缓冲区。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非解码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@link AV_ERR_INVALID_STATE}解码器状态不支持调用本接口时调用。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_Flush(OH_AVCodec *codec);

/**
 * @brief 重置解码器，解码器回到初始化状态。如果要继续解码，需要再次调用OH_VideoDecoder_Configure接口配置解码器实例。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非解码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_Reset(OH_AVCodec *codec);

/**
 * @brief 获取解码器输出数据的OH_AVFormat信息，请参阅{@link OH_AVFormat。
 * 需要注意的是，返回值指向的OH_AVFormat实例的生命周期需要调用者通过调用接口OH_AVFormat_Destroy释放。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @return 返回指向OH_AVFormat实例的指针。
 * 当输入的codec指针非解码器实例，或者为空指针，则返回NULL。
 * @since 9
 * @version 1.0
 */
OH_AVFormat *OH_VideoDecoder_GetOutputDescription(OH_AVCodec *codec);

/**
 * @brief 设置解码器的动态参数。注意，该接口只能在解码器启动后调用。同时，参数配置错误可能会导致解码失败。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param format 指向OH_AVFormat实例的指针。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_INVALID_VAL}1. 输入的codec指针为非解码器实例，或者为空指针；2. 输入format参数不支持。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@link AV_ERR_INVALID_STATE}解码器状态不支持调用本接口时调用。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_SetParameter(OH_AVCodec *codec, OH_AVFormat *format);

/**
 * @brief 将填充数据的输入缓冲区提交给视频解码器。
 * 输入回调将报告可用的输入缓冲区和相应的index值，请参阅{@OH_AVCodecOnNeedInputData}。
 * 一旦具有指定index的缓冲区提交到视频解码器，则无法再次访问缓冲区，直到再次收到输入回调，报告具有相同index的缓冲区可用。
 * 此外，对于某些解码器，需要在开始时向解码器输入编解码特定数据，以初始化解码器的解码过程，如H264格式的PPS/SPS数据。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param index 输入Buffer对应的索引值。由{@link OH_AVCodecOnNeedInputData}给出。
 * @param attr 	描述缓冲区中包含的数据的信息。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非解码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@link AV_ERR_INVALID_STATE}解码器状态不支持调用本接口时调用。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。
 * @deprecated since 11
 * @useinstead OH_VideoDecoder_PushInputBuffer
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_PushInputData(OH_AVCodec *codec, uint32_t index, OH_AVCodecBufferAttr attr);

/**
 * @brief 将处理后的输出buffer返回给解码器，并通知解码器完成在输出surface上渲染Buffer中包含的解码数据。
 * 如果之前没有配置输出surface，则调用此接口仅将指定index对应的输出缓冲区返回给解码器。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param index 输出Buffer对应的索引值。由{@link OH_AVCodecOnNewOutputData}给出。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非解码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@link AV_ERR_INVALID_STATE}解码器状态不支持调用本接口时调用。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。
 * @deprecated since 11
 * @useinstead OH_VideoDecoder_RenderOutputBuffer
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_RenderOutputData(OH_AVCodec *codec, uint32_t index);

/**
 * @brief 将处理后的输出缓冲区返回到解码器。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param index 输出buffer对应的索引值。由{@link OH_AVCodecOnNewOutputData}给出。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非解码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@link AV_ERR_INVALID_STATE}解码器状态不支持调用本接口时调用。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。
 * @deprecated since 11
 * @useinstead OH_VideoDecoder_FreeOutputBuffer
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_FreeOutputData(OH_AVCodec *codec, uint32_t index);

/**
 * @brief 通知视频解码器已对index对应的缓冲区完成输入数据的填充。
 * 输入回调将报告可用的输入缓冲区和相应的index值，请参阅{@OH_AVCodecOnNeedInputBuffer}。
 * 一旦具有指定index的缓冲区提交到视频解码器，则无法再次访问缓冲区，直到再次收到输入回调，报告具有相同index的缓冲区可用。
 * 此外，对于某些解码器，需要在开始时向解码器输入编解码特定数据，以初始化解码器的解码过程，如H264格式的PPS/SPS数据。
 * 调用者可以使用该接口把解码需要的参数集如H264格式的PPS/SPS传递给解码器，该参数集可以单独送入解码器也可以和要解码的数据一起传入。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param index 输入buffer对应的索引值。由{@link OH_AVCodecOnNeedInputBuffer}给出。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非解码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@link AV_ERR_INVALID_STATE}解码器状态不支持调用本接口时调用。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。
 * @since 11
 */
OH_AVErrCode OH_VideoDecoder_PushInputBuffer(OH_AVCodec *codec, uint32_t index);

/**
 * @brief 将index对应的输出缓冲返回给解码器，缓冲中携带解码输出数据，并通知解码器完成在输出surface上渲染，输出缓冲包含解码数据。
 * 如果之前没有配置输出surface，则调用此接口仅将指定index对应的输出缓冲区返回给解码器。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param index 输出buffer对应的索引值。由{@link OH_AVCodecOnNewOutputBuffer}给出。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非解码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@link AV_ERR_INVALID_STATE}解码器状态不支持调用本接口时调用。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。
 * @since 11
 */
OH_AVErrCode OH_VideoDecoder_RenderOutputBuffer(OH_AVCodec *codec, uint32_t index);

/**
 * @brief 将index对应的输出缓冲返回给解码器，缓冲中携带解码输出数据，并通知解码器在调用者指定的时间内完成在输出surface上渲染，输出缓冲包含解码数据。
 * 如果之前没有配置输出surface，则调用此接口仅将指定index对应的输出缓冲区返回给解码器。
 * 调用者可以使用时间戳在特定时间（在VSYNC或者缓冲区时间戳之后）渲染缓冲区。若要在指定的时间戳显示，时间戳需要合理接近系统时间，有几点需要注意：
 * 1. 缓冲区是按照顺序处理的，因此可能会阻塞后续缓冲区在surface上的显示，如果想要对用户的一些行为做出反应，例如停止或者快进快退视频，这一点很重要。
 * 2. 如果多个缓冲区被发送到surface要在同一个VSYNC上渲染，那么最后一个将会被显示，其他的将被丢弃。
 * 3. 如果时间戳与当前的系统时间不是“合理接近”，surface将会忽略时间戳，并在可行的最早时间里显示buffer。在此模式下不会丢弃帧。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param index 输出buffer对应的索引值。由{@link OH_AVCodecOnNewOutputBuffer}给出。
 * @param renderTimestampNs 输出buffer被发送到surface的时间戳，单位是纳秒。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非解码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@link AV_ERR_INVALID_STATE}解码器状态不支持调用本接口时调用。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。
 * @since 12
 */
OH_AVErrCode OH_VideoDecoder_RenderOutputBufferAtTime(OH_AVCodec *codec, uint32_t index, int64_t renderTimestampNs);

/**
 * @brief 将处理后的输出缓冲区返回到解码器。用户使用完需要及时调用此接口释放输出缓存区，否则会阻塞解码流程。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针
 * @param index 输出buffer对应的索引值。由{@link OH_AVCodecOnNewOutputBuffer}给出。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。\n
 *         {@link AV_ERR_INVALID_VAL}1. 输入的codec指针为非解码器实例，或者为空指针；2. index非法或者连续给同一个index，该错误不影响后续解码流程。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@link AV_ERR_INVALID_STATE}解码器状态不支持调用本接口时调用。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。
 * @since 11
 */
OH_AVErrCode OH_VideoDecoder_FreeOutputBuffer(OH_AVCodec *codec, uint32_t index);

/**
 * @brief 检查当前解码实例是否有效。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param isValid 输出参数。指向布尔类型的指针，如果解码器实例有效，则为true，如果解码器实例无效，则为false。建议调用者将isValid初始化为false。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非解码器实例，或者为空指针。
 * @since 10
 */
OH_AVErrCode OH_VideoDecoder_IsValid(OH_AVCodec *codec, bool *isValid);

/**
 * @brief 设置解密配置。在调用OH_VideoDecoder_Prepare接口之前，可选择调用此接口。
 *
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param mediaKeySession 指向带有解密功能的DRM会话实例的指针，请参阅{@link MediaKeySession}。
 * @param secureVideoPath 安全视频通路。指定安全视频通路为true，非安全视频通路为false。
 * 在Surface模式下，既支持安全视频通路，也支持非安全视频通路。在Buffer模式下，仅支持非安全视频通路。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}1. 内部执行错误；2. 解码服务进程异常；3. 媒体密钥会话服务处于错误状态。\n
 *         {@link AV_ERR_INVALID_VAL}1. 输入的codec指针为非解码器实例或为空指针；2. mediaKeySession为nullptr或无效。\n
 *         {@link AV_ERR_NO_MEMORY}输入的解码器实例已经销毁。
 * @since 11
 * @version 1.0
*/
OH_AVErrCode OH_VideoDecoder_SetDecryptionConfig(OH_AVCodec *codec, MediaKeySession *mediaKeySession,
    bool secureVideoPath);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVCODEC_VIDEODECODER_H
/** @} */