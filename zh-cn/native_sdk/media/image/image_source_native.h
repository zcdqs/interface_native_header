/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Image_NativeModule
 * @{
 *
 * @brief 提供图片编解码能力。
 *
 * @since 12
 */

/**
 * @file image_source_native.h
 *
 * @brief 图片解码API。
 *
 * @library libimage_source.so
 * @syscap SystemCapability.Multimedia.Image.ImageSource
 * @since 12
 */

#ifndef INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_IMAGE_SOURCE_NATIVE_H_
#define INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_IMAGE_SOURCE_NATIVE_H_
#include "image_common.h"

#include "pixelmap_native.h"
#include "rawfile/raw_file.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief ImageSource结构体类型，用于执行ImageSource相关操作。
 *
 * @since 12
 */
struct OH_ImageSourceNative;
typedef struct OH_ImageSourceNative OH_ImageSourceNative;

/**
 * @brief 图片源信息结构体
 * {@link OH_ImageSourceInfo_Create}.
 *
 * @since 12
 */
struct OH_ImageSource_Info;
typedef struct OH_ImageSource_Info OH_ImageSource_Info;

/**
 * @brief 解码指定期望动态范围。
 *
 * @since 12
 */
typedef enum {
    /*
    * 根据图片自适应处理。
    */
    IMAGE_DYNAMIC_RANGE_AUTO = 0,
    /*
    * 标准动态范围。
    */
    IMAGE_DYNAMIC_RANGE_SDR = 1,
    /*
    * 高动态范围。
    */
    IMAGE_DYNAMIC_RANGE_HDR = 2,
} IMAGE_DYNAMIC_RANGE;

/**
 * @brief 创建OH_ImageSource_Info指针。
 *
 * @param info 被操作的OH_ImageSource_Info指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageSourceInfo_Create(OH_ImageSource_Info **info);

/**
 * @brief 获取图片的宽。
 *
 * @param info 被操作的OH_ImageSource_Info指针。
 * @param width 图片的宽，单位：像素。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageSourceInfo_GetWidth(OH_ImageSource_Info *info, uint32_t *width);

/**
 * @brief 获取图片的高。
 *
 * @param info 被操作的OH_ImageSource_Info指针。
 * @param height 图片的高，单位：像素高
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageSourceInfo_GetHeight(OH_ImageSource_Info *info, uint32_t *height);

/**
 * @brief 获取图片是否为高动态范围的信息。
 *
 * @param info 被操作的OH_ImageSource_Info指针。
 * @param isHdr 是否为hdr的布尔值。
 * @return 如果操作成功返回IMAGE_SUCCESS，参数校验错误返回IMAGE_BAD_PARAMETER。具体请参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageSourceInfo_GetDynamicRange(OH_ImageSource_Info *info, bool *isHdr);

/**
 * @brief 释放OH_ImageSource_Info指针。
 *
 * @param info 被操作的OH_ImageSource_Info指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageSourceInfo_Release(OH_ImageSource_Info *info);

/**
 * @brief 编码选项参数结构体,被用于{@link OH_ImageSourceNative_CreatePixelmap}。
 *
 * @since 12
 */
struct OH_DecodingOptions;
typedef struct OH_DecodingOptions OH_DecodingOptions;

/**
 * @brief 创建OH_DecodingOptions指针。
 *
 * @param  options 被操作的OH_DecodingOptions指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_DecodingOptions_Create(OH_DecodingOptions **options);

/**
 * @brief 获取pixel格式。
 *
 * @param  options 被操作的OH_DecodingOptions指针。
 * @param pixelFormat pixel格式{@link PIXEL_FORMAT}。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_DecodingOptions_GetPixelFormat(OH_DecodingOptions *options,
    int32_t *pixelFormat);

/**
 * @brief 设置pixel格式。
 *
 * @param  options 被操作的OH_DecodingOptions指针。
 * @param pixelFormat pixel格式{@link PIXEL_FORMAT}。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_DecodingOptions_SetPixelFormat(OH_DecodingOptions *options,
    int32_t pixelFormat);

/**
 * @brief 获取解码图片序号。
 *
 * @param  options 被操作的OH_DecodingOptions指针。
 * @param index 解码图片序号。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_DecodingOptions_GetIndex(OH_DecodingOptions *options, uint32_t *index);

/**
 * @brief 设置解码图片序号。
 *
 * @param  options 被操作的OH_DecodingOptions指针。
 * @param index 解码图片序号。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_DecodingOptions_SetIndex(OH_DecodingOptions *options, uint32_t index);

/**
 * @brief 获取旋转角度。
 *
 * @param  options 被操作的OH_DecodingOptions指针。
 * @param rotate 旋转角度，单位为deg。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_DecodingOptions_GetRotate(OH_DecodingOptions *options, float *rotate);

/**
 * @brief 设置旋转角度。
 *
 * @param  options 被操作的OH_DecodingOptions指针。
 * @param rotate 旋转角度，单位为deg。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_DecodingOptions_SetRotate(OH_DecodingOptions *options, float rotate);

/**
 * @brief 获取期望输出大小。
 *
 * @param  options 被操作的OH_DecodingOptions指针。
 * @param desiredSize 期望输出大小。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_DecodingOptions_GetDesiredSize(OH_DecodingOptions *options,
    Image_Size *desiredSize);

/**
 * @brief 设置期望输出大小。
 *
 * @param  options 被操作的OH_DecodingOptions指针。
 * @param desiredSize 期望输出大小。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_DecodingOptions_SetDesiredSize(OH_DecodingOptions *options,
    Image_Size *desiredSize);

/**
 * @brief 设置解码区域。
 *
 * @param  options 被操作的OH_DecodingOptions指针。
 * @param desiredRegion 解码区域。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_DecodingOptions_GetDesiredRegion(OH_DecodingOptions *options,
    Image_Region *desiredRegion);

/**
 * @brief 设置解码区域。
 *
 * @param  options 被操作的OH_DecodingOptions指针。
 * @param desiredRegion 解码区域。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_DecodingOptions_SetDesiredRegion(OH_DecodingOptions *options,
    Image_Region *desiredRegion);

/**
 * @brief 获取解码时设置的期望动态范围。
 *
 * @param options 被操作的OH_DecodingOptions指针。
 * @param desiredDynamicRange 期望的动态范围值 {@link IMAGE_DYNAMIC_RANGE}。
 * @return 如果操作成功返回IMAGE_SUCCESS，参数校验错误返回IMAGE_BAD_PARAMETER。具体请参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_DecodingOptions_GetDesiredDynamicRange(OH_DecodingOptions *options,
    int32_t *desiredDynamicRange);

/**
 * @brief 设置解码时的期望动态范围。
 *
 * @param options 被操作的OH_DecodingOptions指针。
 * @param desiredDynamicRange 期望的动态范围值 {@link IMAGE_DYNAMIC_RANGE}。
 * @return 如果操作成功返回IMAGE_SUCCESS，参数校验错误返回IMAGE_BAD_PARAMETER。具体请参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_DecodingOptions_SetDesiredDynamicRange(OH_DecodingOptions *options,
    int32_t desiredDynamicRange);

/**
 * @brief 释放OH_DecodingOptions指针。
 *
 * @param  options 被操作的OH_DecodingOptions指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_DecodingOptions_Release(OH_DecodingOptions *options);

/**
 * @brief 通过uri创建OH_ImageSourceNative指针。
 *
 * @param uri 指向图像源URI的指针。只接受文件URI或Base64 URI。
 * @param uriSize URI长度。
 * @param res 指向c++本地层创建的OH_ImageSourceNative对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果解码数据源异常返回 IMAGE_BAD_SOURCE，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageSourceNative_CreateFromUri(char *uri, size_t uriSize, OH_ImageSourceNative **res);

/**
 * @brief 通过fd创建OH_ImageSourceNative指针。
 *
 * @param fd 文件描述符fd。
 * @param res 指向c++本地层创建的OH_ImageSourceNative对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageSourceNative_CreateFromFd(int32_t fd, OH_ImageSourceNative **res);

/**
 * @brief 通过缓冲区数据创建OH_ImageSourceNative指针。
 *
 * @param data 图像缓冲区数据。
 * @param dataSize 图像缓冲区数据长度。
 * @param res 指向c++本地层创建的OH_ImageSourceNative对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果解码数据源异常返回 IMAGE_BAD_SOURCE，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageSourceNative_CreateFromData(uint8_t *data, size_t dataSize, OH_ImageSourceNative **res);

/**
 * @brief 通过图像资源文件的RawFileDescriptor创建OH_ImageSourceNative指针
 *
 * @param rawFile 指示raw文件的文件描述符。
 * @param res 指向c++本地层创建的OH_ImageSourceNative对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageSourceNative_CreateFromRawFile(RawFileDescriptor *rawFile, OH_ImageSourceNative **res);

/**
 * @brief 通过图片解码参数创建OH_PixelmapNative指针
 *
 * @param source 被操作的OH_ImageSourceNative指针。
 * @param  options 解码参数。
 * @param resPixMap 指向c++本地层创建的OH_PixelmapNative对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageSourceNative_CreatePixelmap(OH_ImageSourceNative *source, OH_DecodingOptions *options,
    OH_PixelmapNative **pixelmap);

/**
 * @brief 通过图片解码参数创建OH_PixelmapNative数组
 *
 * @param source 被操作的OH_ImageSourceNative指针。
 * @param  options 解码参数。
 * @param resVecPixMap 指向c++本地层创建的OH_PixelmapNative对象的指针数组。
 * @param size 数组长度。 用户可以使用{@link OH_ImageSourceNative_GetFrameCount}获取。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果不支持的操作返回 IMAGE_UNSUPPORTED_OPERATION，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageSourceNative_CreatePixelmapList(OH_ImageSourceNative *source, OH_DecodingOptions *options,
    OH_PixelmapNative *resVecPixMap[], size_t size);

/**
 * @brief 通过图片解码创建OH_PictureNative指针。
 *
 * @param source 被操作的OH_ImageSourceNative指针。
 * @param options 解码参数。
 * @param picture 指向c++本地层创建的OH_PictureNative对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 解码失败返回 IMAGE_DECODE_FAILED，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_ImageSourceNative_CreatePicture(OH_ImageSourceNative *source, OH_DecodingOptionsForPicture *options,
    OH_PictureNative **picture);

/**
 * @brief 获取图像延迟时间数组
 *
 * @param source 被操作的OH_ImageSourceNative指针。
 * @param delayTimeList 指向获得的延迟时间列表的指针。它不能是空指针。
 * @param size delayTimeList的大小。用户可以从{@link OH_ImageSourceNative_GetFrameCount}获得大小。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageSourceNative_GetDelayTimeList(OH_ImageSourceNative *source, int32_t *delayTimeList, size_t size);

/**
 * @brief 获取指定序号的图片信息。
 *
 * @param source 被操作的OH_ImageSourceNative指针。
 * @param index 图片序号。对GIF图片可传入[0,N-1],N表示GIF的帧数。对只有一帧数据的图片格式，可传入0。
 * @param info 指向获取的图像源信息的OH_ImageSource_Info指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageSourceNative_GetImageInfo(OH_ImageSourceNative *source, int32_t index,
    OH_ImageSource_Info *info);

/**
 * @brief 获取图片指定属性键的值。
 *
 * @param source 被操作的OH_ImageSourceNative指针。
 * @param key 指示指向属性的指针，详情请参见{@link Image_String}，key的取值范围参考OHOS_IMAGE_PROPERTY_XXX定义。
 * 使用ImageSource后释放，参见{@link OH_ImageSourceNative_Release}。
 * @param value 指向获取的值的指针。用户可以传入一个空指针和零大小，
 * 我们将分配内存，但用户必须在使用后释放内存。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageSourceNative_GetImageProperty(OH_ImageSourceNative *source, Image_String *key,
    Image_String *value);

/**
 * @brief 通过指定的键修改图片属性的值。
 * @param source 被操作的OH_ImageSourceNative指针。
 * @param key 指向属性键的指针，详情请参见{@link Image_String}，key是一个exif常数。
 * 使用ImageSource后释放，参见{@link OH_ImageSourceNative_Release}。
 * @param value 需要修改的属性值。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageSourceNative_ModifyImageProperty(OH_ImageSourceNative *source, Image_String *key,
    Image_String *value);

/**
 * @brief 获取图像帧数。
 *
 * @param source 被操作的OH_ImageSourceNative指针。
 * @param frameCount 图像帧数。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageSourceNative_GetFrameCount(OH_ImageSourceNative *source, uint32_t *frameCount);

/**
 * @brief 释放OH_ImageSourceNative指针。
 *
 * @param source 要释放的OH_ImageSourceNative指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageSourceNative_Release(OH_ImageSourceNative *source);

/**
 * @brief 创建OH_DecodingOptionsForPicture指针。
 *
 * @param options 被操作的OH_DecodingOptionsForPicture指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_DecodingOptionsForPicture_Create(OH_DecodingOptionsForPicture **options);

/**
 * @brief 获取解码时设置的期望辅助图（期望解码出的picture包含的辅助图）。
 *
 * @param options 被操作的OH_DecodingOptionsForPicture指针。
 * @param desiredAuxiliaryPictures 解码选项中的期望辅助图。
 * @param length 期望辅助图长度。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_DecodingOptionsForPicture_GetDesiredAuxiliaryPictures(OH_DecodingOptionsForPicture *options,
    Image_AuxiliaryPictureType **desiredAuxiliaryPictures, size_t *length);

/**
 * @brief 设置解码选项中的期望辅助图。
 *
 * @param options 被操作的OH_DecodingOptionsForPicture指针
 * @param desiredAuxiliaryPictures 将要设置的期望辅助图。
 * @param length 期望辅助图长度。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_DecodingOptionsForPicture_SetDesiredAuxiliaryPictures(OH_DecodingOptionsForPicture *options,
    Image_AuxiliaryPictureType *desiredAuxiliaryPictures, size_t length);

/**
 * @brief 释放OH_DecodingOptionsForPicture指针。
 *
 * @param options 要释放的OH_DecodingOptionsForPicture指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_DecodingOptionsForPicture_Release(OH_DecodingOptionsForPicture *options);
#ifdef __cplusplus
};
#endif
/** @} */
#endif // INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_IMAGE_SOURCE_NATIVE_H_