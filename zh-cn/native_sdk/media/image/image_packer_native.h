/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Image_NativeModule
 * @{
 *
 * @brief 提供图片编解码能力。
 *
 * @since 12
 */

/**
 * @file image_packer_native.h
 *
 * @brief 图片编码API。
 *
 * @library libimage_packer.so
 * @syscap SystemCapability.Multimedia.Image.ImagePacker
 * @since 12
 */

#ifndef INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_IMAGE_PACKER_NATIVE_H_
#define INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_IMAGE_PACKER_NATIVE_H_
#include "image_common.h"
#include "image_source_native.h"
#include "pixelmap_native.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief ImagePacker结构体类型，用于执行ImagePacker相关操作。
 *
 * @since 12
 */
struct OH_ImagePackerNative;
typedef struct OH_ImagePackerNative OH_ImagePackerNative;

/**
 * @brief 图像编码选项。
 *
 * @since 12
 */
struct OH_PackingOptions;
typedef struct OH_PackingOptions OH_PackingOptions;

/**
 * @brief 图像序列编码选项。
 *
 * @since 12
 */
struct OH_PackingOptionsForSequence;

/**
 * @brief 图像序列编码选项。
 *
 * @since 12
 */
typedef struct OH_PackingOptionsForSequence OH_PackingOptionsForSequence;

/**
 * @brief 编码指定动态范围。
 *
 * @since 12
 */
typedef enum {
    /*
    * 编码动态范围根据图像信息自适应。
    */
    IMAGE_PACKER_DYNAMIC_RANGE_AUTO = 0,
    /*
    * 编码图片为标准动态范围。
    */
    IMAGE_PACKER_DYNAMIC_RANGE_SDR = 1,
} IMAGE_PACKER_DYNAMIC_RANGE;

/**
 * @brief 创建PackingOptions结构体的指针。
 *
 * @param options 用于操作的PackingOptions指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PackingOptions_Create(OH_PackingOptions **options);

/**
 * @brief 获取mime type。
 *
 * @param options 被操作的OH_PackingOptions指针。
 * @param format 图像格式。可传入一个空指针和零大小，系统将分配内存，但必须在使用后释放内存。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PackingOptions_GetMimeType(OH_PackingOptions *options,
    Image_MimeType *format);

/**
 * @brief 设置mime type。
 *
 * @param options 被操作的OH_PackingOptions指针。
 * @param format 图像格式。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PackingOptions_SetMimeType(OH_PackingOptions *options,
    Image_MimeType *format);

/**
 * @brief 获取编码质量。
 *
 * @param options 被操作的OH_PackingOptions指针。
 * @param quality 编码质量。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PackingOptions_GetQuality(OH_PackingOptions *options,
    uint32_t *quality);

/**
 * @brief 设置编码质量。
 *
 * @param options 被操作的OH_PackingOptions指针。
 * @param quality 编码质量。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PackingOptions_SetQuality(OH_PackingOptions *options,
    uint32_t quality);

/**
 * @brief 获取编码时期望的图片动态范围。
 *
 * @param options 被操作的OH_PackingOptions指针。
 * @param desiredDynamicRange 期望的动态范围 {@link IMAGE_PACKER_DYNAMIC_RANGE}。
 * @return 如果操作成功返回IMAGE_SUCCESS，参数校验错误返回IMAGE_BAD_PARAMETER。具体请参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PackingOptions_GetDesiredDynamicRange(OH_PackingOptions *options, int32_t* desiredDynamicRange);

/**
 * @brief 设置编码时期望的图片动态范围。
 *
 * @param options 被操作的OH_PackingOptions指针。
 * @param desiredDynamicRange 期望的动态范围 {@link IMAGE_PACKER_DYNAMIC_RANGE}。
 * @return 如果操作成功返回IMAGE_SUCCESS，参数校验错误返回IMAGE_BAD_PARAMETER。具体请参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PackingOptions_SetDesiredDynamicRange(OH_PackingOptions *options, int32_t desiredDynamicRange);

/**
 * @brief 释放OH_PackingOptions指针。
 *
 * @param options 被操作的OH_PackingOptions指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PackingOptions_Release(OH_PackingOptions *options);

/**
 * @brief 创建OH_PackingOptionsForSequence结构体的指针。
 *
 * @param options 用于操作的OH_PackingOptionsForSequence指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER。具体请参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PackingOptionsForSequence_Create(OH_PackingOptionsForSequence **options);

/**
 * @brief 设置编码时指定的帧数。
 *
 * @param options 用于操作的OH_PackingOptionsForSequence指针。
 * @param frameCount 图片的帧数。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER。具体请参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PackingOptionsForSequence_SetFrameCount(OH_PackingOptionsForSequence *options,
    uint32_t frameCount);

/**
 * @brief 获取编码时指定的帧数。
 *
 * @param options 用于操作的OH_PackingOptionsForSequence指针。
 * @param frameCount 图片的帧数。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER。具体请参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PackingOptionsForSequence_GetFrameCount(OH_PackingOptionsForSequence *options,
    uint32_t *frameCount);

/**
 * @brief 设定编码时图片的延迟时间数组。
 *
 * @param options 用于操作的OH_PackingOptionsForSequence指针。
 * @param delayTimeList 图片延迟时间数组的指针。
 * @param delayTimeListLength 图片延迟时间数组的长度。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER。具体请参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PackingOptionsForSequence_SetDelayTimeList(OH_PackingOptionsForSequence *options,
    int32_t *delayTimeList, size_t delayTimeListLength);

/**
 * @brief 获取编码时图片的延迟时间数组。
 *
 * @param options 用于操作的OH_PackingOptionsForSequence指针。
 * @param delayTimeList 图片延迟时间数组的指针。
 * @param delayTimeListLength 图片延迟时间数组的长度。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER。具体请参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PackingOptionsForSequence_GetDelayTimeList(OH_PackingOptionsForSequence *options,
    int32_t *delayTimeList, size_t delayTimeListLength);

/**
 * @brief 设定编码时图片的过渡帧模式数组。
 *
 * @param options 用于操作的OH_PackingOptionsForSequence指针。
 * @param disposalTypes 图片过渡帧模式数组的指针。
 * @param disposalTypesLength 图片过渡帧模式数组的长度。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER。具体请参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PackingOptionsForSequence_SetDisposalTypes(OH_PackingOptionsForSequence *options,
    uint32_t *disposalTypes, size_t disposalTypesLength);

/**
 * @brief 获取编码时图片的过渡帧模式数组。
 *
 * @param options 用于操作的OH_PackingOptionsForSequence指针。
 * @param disposalTypes 图片过渡帧模式数组的指针。
 * @param disposalTypesLength 图片过渡帧模式数组的长度。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER。具体请参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PackingOptionsForSequence_GetDisposalTypes(OH_PackingOptionsForSequence *options,
    uint32_t *disposalTypes, size_t disposalTypesLength);

/**
 * @brief 设定编码时图片循环播放次数。
 *
 * @param options 用于操作的OH_PackingOptionsForSequence指针。
 * @param loopCount 图片循环播放次数。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER。具体请参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PackingOptionsForSequence_SetLoopCount(OH_PackingOptionsForSequence *options, uint32_t loopCount);

/**
 * @brief 获取编码时图片循环播放次数。
 *
 * @param options 用于操作的OH_PackingOptionsForSequence指针。
 * @param loopCount 图片循环播放次数。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER。具体请参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PackingOptionsForSequence_GetLoopCount(OH_PackingOptionsForSequence *options, uint32_t *loopCount);

/**
 * @brief 释放OH_PackingOptionsForSequence指针。
 *
 * @param options 用于操作的OH_PackingOptionsForSequence指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER。具体请参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PackingOptionsForSequence_Release(OH_PackingOptionsForSequence *options);

/**
 * @brief 创建OH_ImagePackerNative指针。
 *
 * @param options 被操作的OH_ImagePackerNative指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImagePackerNative_Create(OH_ImagePackerNative **imagePacker);

/**
 * @brief 将ImageSource编码为指定格式的数据。
 *
 * @param imagePacker 被操作的OH_ImagePackerNative指针。
 * @param options 打包选项参数 {@link OH_PackingOptions}。
 * @param imageSource 用于编码的image source指针。
 * @param outData 用于存储打包图像输出数据的缓冲区。
 * @param size 用于存储打包图像输出数据的缓冲区大小。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果解码失败返回 IMAGE_DECODE_FAILED，如果申请内存失败返回 IMAGE_ALLOC_FAILED，
 * 如果数据或图片过大返回 IMAGE_TOO_LARGE，如果未知错误返回 IMAGE_UNKNOWN_ERROR，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImagePackerNative_PackToDataFromImageSource(OH_ImagePackerNative *imagePacker,
    OH_PackingOptions *options, OH_ImageSourceNative *imageSource, uint8_t *outData, size_t *size);

/**
 * @brief 将Pixelmap编码为指定格式的数据。
 *
 * @param imagePacker 被操作的OH_ImagePackerNative指针。
 * @param options 打包选项参数 {@link OH_PackingOptions}。
 * @param pixelmap 用于编码的Pixelmap指针。
 * @param outData 用于存储打包图像输出数据的缓冲区。
 * @param size 用于存储打包图像输出数据的缓冲区大小。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果解码失败返回 IMAGE_DECODE_FAILED，如果申请内存失败返回 IMAGE_ALLOC_FAILED，
 * 如果数据或图片过大返回 IMAGE_TOO_LARGE，如果未知错误返回 IMAGE_UNKNOWN_ERROR，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImagePackerNative_PackToDataFromPixelmap(OH_ImagePackerNative *imagePacker,
    OH_PackingOptions *options, OH_PixelmapNative *pixelmap, uint8_t *outData, size_t *size);

/**
 * @brief 将Picture编码为指定格式的数据。
 *
 * @param imagePacker 被操作的OH_ImagePackerNative指针。
 * @param options 打包选项参数 {@link OH_PackingOptions}。
 * @param picture 用于编码的Picture指针。
 * @param outData 用于存储打包图像输出数据的缓冲区。
 * @param size 用于存储打包图像输出数据的缓冲区大小。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果解码失败返回 IMAGE_DECODE_FAILED，具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_ImagePackerNative_PackToDataFromPicture(OH_ImagePackerNative *imagePacker,
    OH_PackingOptions *options, OH_PictureNative *picture, uint8_t *outData, size_t *size);

/**
 * @brief 将Pixelmap序列编码为数据。
 *
 * @param imagePacker 被操作的OH_ImagePackerNative指针。
 * @param options 编码选项参数 {@link OH_PackingOptionsForSequence}。
 * @param pixelmapSequence 用于编码的Pixelmap序列指针。
 * @param sequenceLength 用于编码的Pixelmap序列长度。
 * @param outData 用于存储编码后图像输出数据的缓冲区。
 * @param outDataSize 用于存储编码后图像输出数据的缓冲区大小。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，如果解码失败返回 IMAGE_DECODE_FAILED，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_ImagePackerNative_PackToDataFromPixelmapSequence(OH_ImagePackerNative *imagePacker,
    OH_PackingOptionsForSequence *options, OH_PixelmapNative **pixelmapSequence,
    size_t sequenceLength, uint8_t *outData, size_t *outDataSize);

/**
 * @brief 将一个ImageSource编码到文件中。
 *
 * @param imagePacker 被操作的OH_ImagePackerNative指针。
 * @param options 打包选项参数 {@link OH_PackingOptions}。
 * @param imageSource 用于编码的image source指针。
 * @param fd 可写的文件描述符。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果解码失败返回 IMAGE_DECODE_FAILED，如果未知错误返回 IMAGE_UNKNOWN_ERROR，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImagePackerNative_PackToFileFromImageSource(OH_ImagePackerNative *imagePacker,
    OH_PackingOptions *options, OH_ImageSourceNative *imageSource, int32_t fd);

/**
 * @brief 将一个Pixelmap编码到文件中。
 *
 * @param imagePacker 被操作的OH_ImagePackerNative指针。
 * @param options 打包选项参数 {@link OH_PackingOptions}。
 * @param pixelmap 用于编码的pixelmap指针。
 * @param fd 可写的文件描述符。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果解码失败返回 IMAGE_DECODE_FAILED，如果未知错误返回 IMAGE_UNKNOWN_ERROR，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImagePackerNative_PackToFileFromPixelmap(OH_ImagePackerNative *imagePacker,
    OH_PackingOptions *options, OH_PixelmapNative *pixelmap, int32_t fd);

/**
 * @brief 将一个Picture编码到文件中。
 *
 * @param imagePacker 被操作的OH_ImagePackerNative指针。
 * @param options 打包选项参数 {@link OH_PackingOptions}。
 * @param picture 用于编码的picture指针。
 * @param fd 可写的文件描述符。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果解码失败返回 IMAGE_DECODE_FAILED，如果未知错误返回 IMAGE_UNKNOWN_ERROR，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_ImagePackerNative_PackToFileFromPicture(OH_ImagePackerNative *imagePacker,
    OH_PackingOptions *options, OH_PictureNative *picture, int32_t fd);

/**
  * @brief 将一个Pixelmap序列编码到文件中。
  *
  * @param imagePacker 被操作的OH_ImagePackerNative指针。
  * @param options 编码选项参数 {@link OH_PackingOptionsForSequence}。
  * @param pixelmapSequence 用于编码的Pixelmap序列指针。
  * @param sequenceLength 用于编码的Pixelmap序列长度。
  * @param fd 可写的文件描述符。
  * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，如果解码失败返回 IMAGE_DECODE_FAILED，
  * 具体请参考 {@link Image_ErrorCode}。
  * @since 13
 */
Image_ErrorCode OH_ImagePackerNative_PackToFileFromPixelmapSequence(OH_ImagePackerNative *imagePacker,
    OH_PackingOptionsForSequence *options, OH_PixelmapNative **pixelmapSequence, size_t sequenceLength, int32_t fd);

/**
 * @brief 释放OH_ImagePackerNative指针。
 *
 * @param imagePacker 被操作的OH_ImagePackerNative指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImagePackerNative_Release(OH_ImagePackerNative *imagePacker);

#ifdef __cplusplus
};
#endif
/* *@} */
#endif // INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_IMAGE_PACKER_NATIVE_H_