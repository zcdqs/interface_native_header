/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup VideoProcessing
 * @{
 *
 * @brief 提供视频处理能力，包括颜色空间转换、元数据生成和视频缩放。
 *
 * @since 12
 */

/**
 * @file video_processing_types.h
 *
 * @brief 视频处理类型定义。
 *
 * @library libvideo_processing.so
 * @syscap SystemCapability.Multimedia.VideoProcessingEngine
 * @kit MediaKit
 * @since 12
 */

#ifndef VIDEO_PROCESSING_ENGINE_C_API_VIDEO_PROCESSING_TYPES_H
#define VIDEO_PROCESSING_ENGINE_C_API_VIDEO_PROCESSING_TYPES_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义视频处理对象。
 *
 * 定义一个OH_VideoProcessing空指针，调用{@link OH_VideoProcessing_Create}创建视频处理实例，该指针在创建实例之前必须为空。
 * 用户可以对不同的处理类型创建不同的视频处理实例。
 *
 * @since 12
 */
typedef struct OH_VideoProcessing OH_VideoProcessing;

/**
 * @brief 定义NativeWindow对象。
 *
 * @since 12
 */
typedef struct NativeWindow OHNativeWindow;

/**
 * @brief 定义OH_AVFormat对象。
 *
 * @since 12
 */
typedef struct OH_AVFormat OH_AVFormat;

/**
 * @brief 表示创建颜色空间转换视频处理实例。
 *
 * 调用{@link OH_VideoProcessing_Create}创建颜色空间转换视频处理实例，如果不支持该能力返回{@link VIDEO_PROCESSING_ERROR_UNSUPPORTED_PROCESSING}。
 *
 * @since 12
 */
extern const int32_t VIDEO_PROCESSING_TYPE_COLOR_SPACE_CONVERSION;

/**
 * @brief 表示创建元数据生成视频处理实例。
 *
 * 调用{@link OH_VideoProcessing_Create}创建元数据生成视频处理实例，如果不支持该能力返回{@link VIDEO_PROCESSING_ERROR_UNSUPPORTED_PROCESSING}。
 *
 * @since 12
 */
extern const int32_t VIDEO_PROCESSING_TYPE_METADATA_GENERATION;

/**
 * @brief 表示创建细节增强视频处理实例。
 *
 * 调用{@link OH_VideoProcessing_Create}创建细节增强视频处理实例，如果不支持该能力返回{@link VIDEO_PROCESSING_ERROR_UNSUPPORTED_PROCESSING}。
 *
 * @since 12
 */
extern const int32_t VIDEO_PROCESSING_TYPE_DETAIL_ENHANCER;

/**
 * @brief 指定视频细节增强的质量等级。
 *
 * 参阅 {@link VideoDetailEnhancer_QualityLevel}查看具体取值。
 * 调用 {@link OH_VideoProcessing_SetParameter}设置质量等级。
 * 调用 {@link OH_VideoProcessing_GetParameter}获取当前质量等级。
 *
 * @since 12
 */
extern const char* VIDEO_DETAIL_ENHANCER_PARAMETER_KEY_QUALITY_LEVEL;

/**
 * @brief 视频颜色空间信息数据结构。
 *
 * @参阅 OH_VideoProcessing_IsColorSpaceConversionSupported
 * @since 12
 */
typedef struct VideoProcessing_ColorSpaceInfo {
    /** 视频元数据类型，参阅{@link enum OH_NativeBuffer_MetadataType}。 */
    int32_t metadataType;
    /** 视频颜色空间类型，参阅{@link enum OH_NativeBuffer_ColorSpace}。 */
    int32_t colorSpace;
    /** 视频像素格式，参阅{@link enum OH_NativeBuffer_Format}。 */
    int32_t pixelFormat;
} VideoProcessing_ColorSpaceInfo;

/**
 * @brief 用于细节增强的质量等级。
 *
 * 参数{@link VIDEO_DETAIL_ENHANCER_PARAMETER_KEY_QUALITY_LEVEL}的具体取值，设置方法详见开发指南。
 *
 * @参阅 OH_VideoProcessing_SetParameter
 * @参阅 OH_VideoProcessing_GetParameter
 * @since 12
 */
typedef enum VideoDetailEnhancer_QualityLevel {
    /** 无细节增强。 */
    VIDEO_DETAIL_ENHANCER_QUALITY_LEVEL_NONE,
    /** 低质量等级细节增强，速度较快，默认设置。 */
    VIDEO_DETAIL_ENHANCER_QUALITY_LEVEL_LOW,
    /** 中等质量等级细节增强，速度适中。 */
    VIDEO_DETAIL_ENHANCER_QUALITY_LEVEL_MEDIUM,
    /** 高质量等级细节增强，速度相对较慢。 */
    VIDEO_DETAIL_ENHANCER_QUALITY_LEVEL_HIGH,
} VideoDetailEnhancer_QualityLevel;

/**
 * @brief 视频处理错误码。
 *
 * @since 12
 */
typedef enum VideoProcessing_ErrorCode {
    /** 处理成功 */
    VIDEO_PROCESSING_SUCCESS,
    /** 输入参数无效。以下情况都会返回该错误码：
     *  1 - 无效的输入或输出视频buffer，视频buffer为空。
     *  2 - 无效的参数，参数为空。
     *  3 - 无效的处理类型。
     */
    VIDEO_PROCESSING_ERROR_INVALID_PARAMETER = 401,
    /** 未知错误，比如GPU计算失败或memcpy失败 */
    VIDEO_PROCESSING_ERROR_UNKNOWN = 29210001,
    /** 视频处理全局环境初始化失败，比如初始化GPU环境失败。*/
    VIDEO_PROCESSING_ERROR_INITIALIZE_FAILED,
    /** 创建视频处理实例失败，比如实例总数超出上限。*/
    VIDEO_PROCESSING_ERROR_CREATE_FAILED,
    /** 处理过程失败，比如处理时间超时 */
    VIDEO_PROCESSING_ERROR_PROCESS_FAILED,
    /** 不支持的处理类型，可以调用 OH_VideoProcessing_IsXXXSupported来检查是否支持这种处理 */
    VIDEO_PROCESSING_ERROR_UNSUPPORTED_PROCESSING,
    /** 不允许的操作，比如不满足调用接口所需的运行状态下调用该接口 */
    VIDEO_PROCESSING_ERROR_OPERATION_NOT_PERMITTED,
    /** 内存不足 */
    VIDEO_PROCESSING_ERROR_NO_MEMORY,
    /** 视频处理实例无效，比如视频处理实例为空实例 */
    VIDEO_PROCESSING_ERROR_INVALID_INSTANCE,
    /** 输入值无效，以下情况都会造成这种错误：
     *  1 - 视频buffer宽高太大或者颜色空间错误。
     *  2 - 参数包含无效的值，比如细节增强的质量等级错误。
     */
    VIDEO_PROCESSING_ERROR_INVALID_VALUE
} VideoProcessing_ErrorCode;

/**
 * @brief 视频处理状态。
 *
 * 视频处理状态通过回调函数{@link OH_VideoProcessing_OnState}进行报告。
 *
 * @since 12
 */
typedef enum VideoProcessing_State {
    /** 视频处理进行中 */
    VIDEO_PROCESSING_STATE_RUNNING,
    /** 视频处理已停止 */
    VIDEO_PROCESSING_STATE_STOPPED
} VideoProcessing_State;

/**
 * @brief 视频处理回调对象类型。
 *
 * 定义一个VideoProcessing_Callback空指针，调用{@link OH_VideoProcessingCallback_Create}来创建一个回调对象。创建之前该指针必须为空。
 * 通过调用{@link OH_VideoProcessing_RegisterCallback}来向视频处理实例注册回调对象。
 *
 * @since 12
 */
typedef struct VideoProcessing_Callback VideoProcessing_Callback;

/**
 * @brief 视频处理过程中报告错误的回调函数指针。
 *
 * 错误码:
 * {@link VIDEO_PROCESSING_ERROR_UNSUPPORTED_PROCESSING}，不支持的处理，比如不支持输入输出的颜色空间类型转换。
 * {@link VIDEO_PROCESSING_ERROR_INVALID_VALUE}，无效的视频属性，比如视频的颜色空间无效。
 * {@link VIDEO_PROCESSING_ERROR_NO_MEMORY}, 内存不足。
 * {@link VIDEO_PROCESSING_ERROR_PROCESS_FAILED}，处理过程中出错，参阅{@link VideoProcessing_ErrorCode}。
 *
 * @param videoProcessor 视频处理实例。
 * @param error 报告给用户的错误码。
 * @param userData 用户的自定义数据。
 * @since 12
 */
typedef void (*OH_VideoProcessingCallback_OnError)(OH_VideoProcessing* videoProcessor,
    VideoProcessing_ErrorCode error, void* userData);

/**
 * @brief 报告视频处理状态的回调函数指针。
 *
 * {@link OH_VideoProcessing_Start}成功调用之后状态会变为{@link VIDEO_PROCESSING_STATE_RUNNING}。
 * 调用{@link OH_VideoProcessing_Stop}，所有的缓存buffer处理完成后，状态会变为{@link VIDEO_PROCESSING_STATE_STOPPED}。
 *
 * @param videoProcessor 视频处理实例。
 * @param state 参阅 {@link VideoProcessing_State}。
 * @param userData 用户的自定义数据。
 * @since 12
 */
typedef void (*OH_VideoProcessingCallback_OnState)(OH_VideoProcessing* videoProcessor, VideoProcessing_State state,
    void* userData);

/**
 * @brief 报告输出buffer已填充好数据的回调函数指针。
 *
 * 每个新输出buffer填充好数据之后该buffer的索引就会报告给用户。调用{@link OH_VideoProcessing_RenderOutputBuffer}根据索引来处理渲染并输出该buffer。
 * 如果未注册该函数，则输出buffer填充好数据后不会报告用户，而是直接进行处理渲染并输出。
 *
 * @param videoProcessor 视频处理实例。
 * @param index 新输出buffer的索引。
 * @param userData 用户自定义的数据。
 * @since 12
 */
typedef void (*OH_VideoProcessingCallback_OnNewOutputBuffer)(OH_VideoProcessing* videoProcessor, uint32_t index,
    void* userData);

#ifdef __cplusplus
}
#endif

#endif // VIDEO_PROCESSING_ENGINE_C_API_VIDEO_PROCESSING_TYPES_H
/** @} */
