/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OH_Camera
 * @{
 *
 * @brief 为相机模块提供C接口的定义。
 *
 * @syscap SystemCapability.Multimedia.Camera.Core
 *
 * @since 11
 * @version 1.0
 */

/**
 * @file camera.h
 *
 * @brief 声明相机的基本概念。
 *
 * @library libohcamera.so
 * @syscap SystemCapability.Multimedia.Camera.Core
 * @since 11
 * @version 1.0
 */

#ifndef NATIVE_INCLUDE_CAMERA_CAMERA_H
#define NATIVE_INCLUDE_CAMERA_CAMERA_H

#include <stdint.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 相机管理器对象。
 *
 * 可以使用{@link OH_Camera_GetCameraManager}方法创建指针。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_Manager Camera_Manager;

/**
 * @brief 相机错误代码的枚举。
 *
 * @since 11
 * @version 1.0
 */
typedef enum Camera_ErrorCode {
    /**
     * 相机结果正常。
     */
    CAMERA_OK = 0,

    /**
     * 参数丢失或参数类型不正确。
     */
    CAMERA_INVALID_ARGUMENT = 7400101,

    /**
     * 不允许操作。
     */
    CAMERA_OPERATION_NOT_ALLOWED = 7400102,

    /**
     * 会话未配置。
     */
    CAMERA_SESSION_NOT_CONFIG = 7400103,

    /**
     * 会话未运行。
     */
    CAMERA_SESSION_NOT_RUNNING = 7400104,

    /**
     * 会话配置已锁定。
     */
    CAMERA_SESSION_CONFIG_LOCKED = 7400105,

    /**
     * 设备设置已锁定。
     */
    CAMERA_DEVICE_SETTING_LOCKED = 7400106,

    /**
     * 因冲突而无法使用相机。
     */
    CAMERA_CONFLICT_CAMERA = 7400107,

    /**
     * 由于安全原因，相机已禁用。
     */
    CAMERA_DEVICE_DISABLED = 7400108,

    /**
     * 因被抢占而无法使用相机。
     */
    CAMERA_DEVICE_PREEMPTED = 7400109,

    /**
     * 与当前配置存在冲突。
     * @since 12
     */
    CAMERA_UNRESOLVED_CONFLICTS_WITH_CURRENT_CONFIGURATIONS = 7400110,

	/**
     * 相机服务致命错误。
     */
    CAMERA_SERVICE_FATAL_ERROR = 7400201
} Camera_ErrorCode;

/**
 * @brief 相机状态的枚举。
 *
 * @since 11
 * @version 1.0
 */
typedef enum Camera_Status {
    /**
     * 显示状态。
     */
    CAMERA_STATUS_APPEAR = 0,

    /**
     * 消失状态。
     */
    CAMERA_STATUS_DISAPPEAR = 1,

    /**
     * 可用状态。
     */
    CAMERA_STATUS_AVAILABLE = 2,

    /**
     * 不可用状态。
     */
    CAMERA_STATUS_UNAVAILABLE = 3
} Camera_Status;

/**
 * @brief 相机模式的枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum Camera_SceneMode {
    /**
     * 普通相机模式。
     */
    NORMAL_PHOTO = 1,

    /**
     * 普通视频模式。
     */
    NORMAL_VIDEO = 2,
	
	/**
     * 安全相机模式。
     * @since 12
     */
    SECURE_PHOTO = 12
} Camera_SceneMode;
 
/**
 * @brief 相机位置的枚举。
 *
 * @since 11
 * @version 1.0
 */
typedef enum Camera_Position {
    /**
     * 未指定位置。
     */
    CAMERA_POSITION_UNSPECIFIED = 0,

    /**
     * 后置。
     */
    CAMERA_POSITION_BACK = 1,

    /**
     * 前置。
     */
    CAMERA_POSITION_FRONT = 2
} Camera_Position;

/**
 * @brief 相机类型的枚举。
 *
 * @since 11
 * @version 1.0
 */
typedef enum Camera_Type {
    /**
     * 默认相机类型。
     */
    CAMERA_TYPE_DEFAULT = 0,

    /**
     * 广角相机。
     */
    CAMERA_TYPE_WIDE_ANGLE = 1,

    /**
     * 超广角相机。
     */
    CAMERA_TYPE_ULTRA_WIDE = 2,

    /**
     * 电话相机。
     */
    CAMERA_TYPE_TELEPHOTO = 3,

    /**
     * 景深相机。
     */
    CAMERA_TYPE_TRUE_DEPTH = 4
} Camera_Type;

/**
 * @brief 相机连接类型的枚举。
 *
 * @since 11
 * @version 1.0
 */
typedef enum Camera_Connection {
    /**
     * 内置摄像头。
     */
    CAMERA_CONNECTION_BUILT_IN = 0,

    /**
     * 使用USB连接的摄像头。
     */
    CAMERA_CONNECTION_USB_PLUGIN = 1,

    /**
     * 远程摄像头。
     */
    CAMERA_CONNECTION_REMOTE = 2
} Camera_Connection;

/**
 * @brief 相机格式类型的枚举。
 *
 * @since 11
 * @version 1.0
 */
typedef enum Camera_Format {
    /**
     * RGBA 8888格式。
     */
    CAMERA_FORMAT_RGBA_8888 = 3,

    /**
     * YUV 420格式。
     */
    CAMERA_FORMAT_YUV_420_SP = 1003,

    /**
     * JPEG格式。
     */
    CAMERA_FORMAT_JPEG = 2000,

    /**
     * YCBCR P010 格式。
     * @since 12
     */
    CAMERA_FORMAT_YCBCR_P010 = 2001,

    /**
     * YCRCB P010 格式。
     * @since 12
     */
    CAMERA_FORMAT_YCRCB_P010 = 2002
} Camera_Format;

/**
 * @brief 闪光模式的枚举。
 *
 * @since 11
 * @version 1.0
 */
typedef enum Camera_FlashMode {
    /**
     * 关闭模式。
     */
    FLASH_MODE_CLOSE = 0,

    /**
     * 打开模式。
     */
    FLASH_MODE_OPEN = 1,

    /**
     * 自动模式。
     */
    FLASH_MODE_AUTO = 2,

    /**
     * 始终打开模式。
     */
    FLASH_MODE_ALWAYS_OPEN = 3
} Camera_FlashMode;

/**
 * @brief 曝光模式的枚举。
 *
 * @since 11
 * @version 1.0
 */
typedef enum Camera_ExposureMode {
    /**
     * 锁定曝光模式。
     */
    EXPOSURE_MODE_LOCKED = 0,

    /**
     * 自动曝光模式。
     */
    EXPOSURE_MODE_AUTO = 1,

    /**
     * 连续自动曝光。
     */
    EXPOSURE_MODE_CONTINUOUS_AUTO = 2
} Camera_ExposureMode;

/**
 * @brief 聚焦模式的枚举。
 *
 * @since 11
 * @version 1.0
 */
typedef enum Camera_FocusMode {
    /**
     * 手动模式。
     */
    FOCUS_MODE_MANUAL = 0,

    /**
     * 连续自动模式。
     */
    FOCUS_MODE_CONTINUOUS_AUTO = 1,

    /**
     * 自动模式。
     */
    FOCUS_MODE_AUTO = 2,

    /**
     * 锁定模式。
     */
    FOCUS_MODE_LOCKED = 3
} Camera_FocusMode;

/**
 * @brief 焦点状态的枚举。
 *
 * @since 11
 * @version 1.0
 */
typedef enum Camera_FocusState {
    /**
     * 扫描状态。
     */
    FOCUS_STATE_SCAN = 0,

    /**
     * 聚焦状态。
     */
    FOCUS_STATE_FOCUSED = 1,

    /**
     * 非聚焦状态。
     */
    FOCUS_STATE_UNFOCUSED = 2
} Camera_FocusState;

/**
 * @brief 录像防抖模式的枚举。
 *
 * @since 11
 * @version 1.0
 */
typedef enum Camera_VideoStabilizationMode {
    /**
     * 关闭录像防抖。
     */
    STABILIZATION_MODE_OFF = 0,

    /**
     * LOW模式提供基本的防抖效果。
     */
    STABILIZATION_MODE_LOW = 1,

    /**
     * MIDDLE模式意味着通过算法可以获得比LOW模式更好的效果。
     */
    STABILIZATION_MODE_MIDDLE = 2,

    /**
     * HIGH模式意味着通过算法可以获得比MIDDLE模式更好的效果。
     */
    STABILIZATION_MODE_HIGH = 3,

    /**
     * 自动选择模式，HDF相机可用。
     */
    STABILIZATION_MODE_AUTO = 4
} Camera_VideoStabilizationMode;

/**
 * @brief 图像旋转角度的枚举。
 *
 * @since 11
 * @version 1.0
 */
typedef enum Camera_ImageRotation {
    /**
     * 捕获图像旋转0度。
     */
    IAMGE_ROTATION_0 = 0,

    /**
     * 捕获图像旋转90度。
     */
    IAMGE_ROTATION_90 = 90,

    /**
     * 捕获图像旋转180度。
     */
    IAMGE_ROTATION_180 = 180,

    /**
     * 捕获图像旋转270度。
     */
    IAMGE_ROTATION_270 = 270
} Camera_ImageRotation;

/**
 * @brief 图像质量等级的枚举。
 *
 * @since 11
 * @version 1.0
 */
typedef enum Camera_QualityLevel {
    /**
     * 高图像质量。
     */
    QUALITY_LEVEL_HIGH = 0,

    /**
     * 中等图像质量。
     */
    QUALITY_LEVEL_MEDIUM = 1,

    /**
     * 低图像质量。
     */
    QUALITY_LEVEL_LOW = 2
} Camera_QualityLevel;

/**
 * @brief 元数据对象类型的枚举。
 *
 * @since 11
 * @version 1.0
 */
typedef enum Camera_MetadataObjectType {
    /**
     * 人脸检测。
     */
    FACE_DETECTION = 0
} Camera_MetadataObjectType;

/**
 * @brief 手电筒模式的枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum Camera_TorchMode {
    /**
     * 设备手电筒常关。
     */
    OFF = 0,

    /**
     * 设备手电筒常开。
     */
    ON = 1,

    /**
     * 设备手电筒自动模式，将根据环境光照水平打开手电筒。
     */
    AUTO = 2
} Camera_TorchMode;

/**
 * @brief 平滑变焦模式的枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum Camera_SmoothZoomMode {
    /**
     * 贝塞尔曲线模式。
     */
    NORMAL = 0
} Camera_SmoothZoomMode;

/**
 * @brief 预配置照片分辨率的枚举
 *
 * @since 12
 * @version 1.0
 */
typedef enum Camera_PreconfigType {
    /**
     * 预配置照片分辨率为720P。
     */
    PRECONFIG_720P = 0,

    /**
     * 预配置照片分辨率为1080P。
     */
    PRECONFIG_1080P = 1,

    /**
     * 预配置照片分辨率为4K。
     */
    PRECONFIG_4K = 2,

    /**
     * 预配置照片为高质量。
     */
    PRECONFIG_HIGH_QUALITY = 3
} Camera_PreconfigType;

/**
 * @brief 预配置照片比例的枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum Camera_PreconfigRatio {
    /**
     * 预配置照片比例为1:1。
     */
    PRECONFIG_RATIO_1_1 = 0,

    /**
     * 预配置照片比例为4:3。
     */
    PRECONFIG_RATIO_4_3 = 1,

    /**
     * 预配置照片比例为16:9。
     */
    PRECONFIG_RATIO_16_9 = 2
} Camera_PreconfigRatio;

/**
 * @brief 大小参数。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_Size {
    /**
     * 宽度，单位为像素。
     */
    uint32_t width;

    /**
     * 高度，单位为像素。
     */
    uint32_t height;
} Camera_Size;

/**
 * @brief 相机流的配置文件。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_Profile {
    /**
     * 相机格式。
     */
    Camera_Format format;

    /**
     * 图片大小。
     */
    Camera_Size size;
} Camera_Profile;

/**
 * @brief 帧速率范围。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_FrameRateRange {
    /**
     * 最小帧速率。
     */
    uint32_t min;

    /**
     * 最大帧速率。
     */
    uint32_t max;
} Camera_FrameRateRange;

/**
 * @brief 录像配置文件。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_VideoProfile {
    /**
     * 相机格式。
     */
    Camera_Format format;

    /**
     * 图片大小。
     */
    Camera_Size size;

    /**
     * 帧速率，单位为fps（每秒帧数）。
     */
    Camera_FrameRateRange range;
} Camera_VideoProfile;

/**
 * @brief 相机输出能力。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_OutputCapability {
    /**
     * 预览配置文件列表。
     */
    Camera_Profile** previewProfiles;

    /**
     * 预览配置文件列表的大小。
     */
    uint32_t previewProfilesSize;

    /**
     * 拍照配置文件列表。
     */
    Camera_Profile** photoProfiles;

    /**
     * 拍照配置文件列表的大小。
     */
    uint32_t photoProfilesSize;

    /**
     * 录像配置文件列表。
     */
    Camera_VideoProfile** videoProfiles;

    /**
     * 录像配置文件列表的大小。
     */
    uint32_t videoProfilesSize;

    /**
     * 元数据对象类型列表。
     */
    Camera_MetadataObjectType** supportedMetadataObjectTypes;

    /**
     * 元数据对象类型列表的大小。
     */
    uint32_t metadataProfilesSize;
} Camera_OutputCapability;

/**
 * @brief 相机设备对象。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_Device {
    /**
     * 相机id属性。
     */
    char* cameraId;

    /**
     * 相机位置属性。
     */
    Camera_Position cameraPosition;

    /**
     * 相机类型属性。
     */
    Camera_Type cameraType;

    /**
     * 相机连接类型属性。
     */
    Camera_Connection connectionType;
} Camera_Device;

/**
 * @brief 相机状态信息。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_StatusInfo {
    /**
     * 相机实例。
     */
    Camera_Device* camera;

    /**
     * 当前相机状态。
     */
    Camera_Status status;
} Camera_StatusInfo;

/**
 * @brief 点参数。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_Point {
    /**
     * X坐标。
     */
    double x;

    /**
     * Y坐标。
     */
    double y;
} Camera_Point;

/**
 * @brief 拍照位置。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_Location {
    /**
     * 纬度。
     */
    double latitude;

    /**
     * 经度。
     */
    double longitude;

    /**
     * 海拔高度，单位为像素。
     */
    double altitude;
} Camera_Location;

/**
 * @brief 要设置的拍照捕获选项。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_PhotoCaptureSetting {
    /**
     * 拍照图像质量。
     */
    Camera_QualityLevel quality;

    /**
     * 拍照旋转角度。
     */
    Camera_ImageRotation rotation;

    /**
     * 拍照位置。
     */
    Camera_Location* location;

    /**
     * 设置镜像拍照功能开关，默认为false。
     */
    bool mirror;
} Camera_PhotoCaptureSetting;

/**
 * @brief 帧快门回调信息。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_FrameShutterInfo {
    /**
     * 捕获id。
     */
    int32_t captureId;

    /**
     * 帧的时间戳。
     */
    uint64_t timestamp;
} Camera_FrameShutterInfo;

/**
 * @brief 捕获结束信息。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_CaptureEndInfo {
    /**
     * 捕获id。
     */
    int32_t captureId;

    /**
     * 帧数。
     */
    int64_t frameCount;
} Camera_CaptureEndInfo;

/**
 * @brief 矩形定义。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_Rect {
    /**
     * 左上角的X坐标。
     */
    int32_t topLeftX;

    /**
     * 左上角的Y坐标。
     */
    int32_t topLeftY;

    /**
     * 矩形宽度，单位为像素。
     */
    int32_t width;

    /**
     * 矩形高度，单位为像素。
     */
    int32_t height;
} Camera_Rect;

/**
 * @brief 元数据对象基础。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_MetadataObject {
    /**
     * 元数据对象类型。
     */
    Camera_MetadataObjectType type;

    /**
     * 元数据对象时间戳（以毫秒为单位）。
     */
    int64_t timestamp;

    /**
     * 检测到的元数据对象的轴对齐边界框。
     */
    Camera_Rect* boundingBox;
} Camera_MetadataObject;

/**
 * @brief 手电筒状态信息。
 *
 * @since 12
 * @version 1.0
 */
typedef struct Camera_TorchStatusInfo {
    /**
     * 手电筒是否可用。
     */
    bool isTorchAvailable;

    /**
     * 手电筒是否激活。
     */
    bool isTorchActive;

    /**
     * 手电筒亮度等级。取值范围为[0,1]，越靠近1，亮度越大。
     */
    float torchLevel;
} Camera_TorchStatusInfo;

/**
 * @brief 平滑变焦参数信息。
 *
 * @since 12
 * @version 1.0
 */
typedef struct Camera_SmoothZoomInfo {
    /**
     * 平滑变焦总时长（以毫秒为单位）。
     */
    int32_t duration;
} Camera_SmoothZoomInfo;

/**
 * @brief 拍照开始信息。
 *
 * @since 12
 * @version 1.0
 */
typedef struct Camera_CaptureStartInfo {
    /**
     * 拍照id。
     */
    int32_t captureId;

    /**
     * 预估的单次拍照底层出sensor采集帧时间，如果上报-1，代表没有预估时间。
     */
    int64_t time;
} Camera_CaptureStartInfo;

/**
 * @brief 拍照曝光结束信息。
 *
 * @since 12
 * @version 1.0
 */
typedef struct Camera_FrameShutterEndInfo {
    /**
     * 拍照id。
     */
    int32_t captureId;
} Camera_FrameShutterEndInfo;

/**
 * @brief 创建CameraManager实例。
 *
 * @param cameraManager 如果方法调用成功，将创建输出{@linkCamera_Manager}cameraManager。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_Camera_GetCameraManager(Camera_Manager** cameraManager);

/**
 * @brief 删除CameraManager实例。
 *
 * @param cameraManager 要删除的{@link Camera_Manager}cameraManager实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_Camera_DeleteCameraManager(Camera_Manager* cameraManager);


#ifdef __cplusplus
}
#endif

#endif // NATIVE_INCLUDE_CAMERA_CAMERA_H
/** @} */