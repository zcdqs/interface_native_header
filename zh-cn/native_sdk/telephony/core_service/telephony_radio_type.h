/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NATIVE_TELEPHONY_RADIO_TYPE_H
#define NATIVE_TELEPHONY_RADIO_TYPE_H

/**
 * @file telephony_radio_type.h
 *
 * @brief 定义网络搜索模块的C接口需要的数据结构.
 *
 * @kit TelephonyKit
 * @syscap SystemCapability.Telephony.CoreService
 * @library libtelephony_radio.so
 * @since 13
 */

#ifdef __cplusplus
extern "C" {
#endif

#define TELEPHONY_MAX_OPERATOR_LEN 64
#define TELEPHONY_MAX_PLMN_NUMERIC_LEN 6

/**
 * @brief 错误码类型枚举.
 *
 * @since 13
 */
typedef enum {
    /* @error 成功 */
    TEL_RADIO_SUCCESS = 0,
    /* @error 权限错误 */
    TEL_RADIO_PERMISSION_DENIED = 201,
    /* @error 参数错误 */
    TEL_RADIO_ERR_INVALID_PARAM = 401,
    /* @error 编组错误 */
    TEL_RADIO_ERR_MARSHALLING_FAILED = 8300001,
    /* @error 连接电话服务错误 */
    TEL_RADIO_ERR_SERVICE_CONNECTION_FAILED = 8300002,
    /* @error 操作电话服务错误 */
    TEL_RADIO_ERR_OPERATION_FAILED = 8300003,
} Telephony_RadioResult;

/**
 * @brief 设备的网络注册状态类型.
 *
 * @since 13
 */
typedef enum {
    /* 设备不能使用任何服务，包括数据业务、短信、通话等 */
    TEL_REG_STATE_NO_SERVICE = 0,
    /* 设备可以正常使用服务，包括数据业务、短信、通话等 */
    TEL_REG_STATE_IN_SERVICE = 1,
    /* 设备只能使用紧急呼叫业务 */
    TEL_REG_STATE_EMERGENCY_CALL_ONLY = 2,
    /* 蜂窝无线电已关闭，modem下电，无法和网侧进行通信 */
    TEL_REG_STATE_POWER_OFF = 3,
} Telephony_RegState;

/**
 * @brief 设备的无线接入技术类型.
 *
 * @since 13
 */
typedef enum {
    /* 未知无线接入技术（RAT） */
    TEL_RADIO_TECHNOLOGY_UNKNOWN = 0,
    /* 无线接入技术GSM（Global System For Mobile Communication） */
    TEL_RADIO_TECHNOLOGY_GSM = 1,
    /* 	无线接入技术1XRTT（Single-Carrier Radio Transmission Technology） */
    TEL_RADIO_TECHNOLOGY_1XRTT = 2,
    /* 无线接入技术WCDMA（Wideband Code Division Multiple Access） */
    TEL_RADIO_TECHNOLOGY_WCDMA = 3,
    /* 无线接入技术HSPA（High Speed Packet Access） */
    TEL_RADIO_TECHNOLOGY_HSPA = 4,
    /* 无线接入技术HSPAP（High Speed packet access (HSPA+) ） */
    TEL_RADIO_TECHNOLOGY_HSPAP = 5,
    /* 无线接入技术TDSCDMA（TimeDivision-Synchronous Code Division Multiple Access） */
    TEL_RADIO_TECHNOLOGY_TD_SCDMA = 6,
    /* 无线接入技术EVDO（Evolution Data Only） */
    TEL_RADIO_TECHNOLOGY_EVDO = 7,
    /* 无线接入技术EHRPD（Evolved High Rate Package Data） */
    TEL_RADIO_TECHNOLOGY_EHRPD = 8,
    /* 无线接入技术LTE（Long Term Evolution） */
    TEL_RADIO_TECHNOLOGY_LTE = 9,
    /* 无线接入技术LTE_CA（Long Term Evolution_Carrier Aggregation） */
    TEL_RADIO_TECHNOLOGY_LTE_CA = 10,
    /* 无线接入技术IWLAN（Industrial Wireless LAN） */
    TEL_RADIO_TECHNOLOGY_IWLAN = 11,
    /* 无线接入技术NR（New Radio） */
    TEL_RADIO_TECHNOLOGY_NR = 12,
} Telephony_RadioTechnology;

/**
 * @brief 设备的NSA网络注册状态类型.
 *
 * @since 13
 */
typedef enum {
    /* 设备在不支持NSA的LTE小区下处于空闲状态或连接状态 */
    TEL_NSA_STATE_NOT_SUPPORTED = 1,
    /* 在支持NSA但不支持NR覆盖检测的LTE小区下，设备处于空闲状态 */
    TEL_NSA_STATE_NO_DETECTED = 2,
    /* 设备在LTE小区下连接到LTE网络支持NSA和NR覆盖检测 */
    TEL_NSA_STATE_CONNECTED_DETECTED = 3,
    /* 支持NSA和NR覆盖检测的LTE小区下设备处于空闲状态 */
    TEL_NSA_STATE_IDLE_DETECTED = 4,
    /* 设备在支持NSA的LTE小区下连接到LTE + NR网络 */
    TEL_NSA_STATE_DUAL_CONNECTED = 5,
    /* 设备在5GC附着时在NG-RAN小区下空闲或连接到NG-RAN小区 */
    TEL_NSA_STATE_SA_ATTACHED = 6,
} Telephony_NsaState;

/**
 * @brief 网络状态信息.
 *
 * @since 13
 */
typedef struct {
    /* 注册网络的长运营商名称 */
    char longOperatorName_[TELEPHONY_MAX_OPERATOR_LEN];
    /* 注册网络的短运营商名称 */
    char shortOperatorName_[TELEPHONY_MAX_OPERATOR_LEN];
    /* 注册网络的PLMN码 */
    char plmnNumeric_[TELEPHONY_MAX_PLMN_NUMERIC_LEN];
    /* 是否处于漫游状态 */
    bool isRoaming_;
    /* 设备的网络注册状态 */
    Telephony_RegState regState_;
    /* 设备的无线接入技术 */
    Telephony_RadioTechnology cfgTech_;
    /* 设备的NSA网络注册状态 */
    Telephony_NsaState nsaState_;
    /* CA的状态 */
    bool isCaActive_;
    /* 此设备是否只允许拨打紧急呼叫 */
    bool isEmergency_;
} Telephony_NetworkState;

#ifdef __cplusplus
}
#endif

#endif // NATIVE_TELEPHONY_RADIO_TYPE_H
