/*
 * Copyright (c) 2021-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_BRUSH_H
#define C_INCLUDE_DRAWING_BRUSH_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_brush.h
 *
 * @brief 文件中定义了与画刷相关的功能函数。
 *
 * 引用文件"native_drawing/drawing_brush.h"
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 用于创建一个画刷对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 函数会返回一个指针，指针指向创建的画刷对象。
 * @since 8
 * @version 1.0
 */
OH_Drawing_Brush* OH_Drawing_BrushCreate(void);

/**
 * @brief 创建一个画刷对象副本{@link OH_Drawing_Brush}，用于拷贝一个已有画刷对象。
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * brush为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param brush 指向画刷对象的指针。
 * @return 函数会返回一个指针，指针指向创建的画刷对象副本{@link OH_Drawing_Brush}。如果对象返回NULL，表示创建失败；
 *         可能的原因是可用内存为空，或者是brush为NULL。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Brush* OH_Drawing_BrushCopy(OH_Drawing_Brush* brush);

/**
 * @brief 用于销毁画刷对象并回收该对象占有的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Brush 指向画刷对象的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_BrushDestroy(OH_Drawing_Brush*);

/**
 * @brief 用于获取画刷是否设置抗锯齿属性，如果为真则说明画刷会启用抗锯齿功能，在绘制图形时会对图形的边缘像素进行半透明的模糊处理。
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Brush为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Brush 指向画刷对象的指针。
 * @return 函数返回画刷对象是否设置抗锯齿属性，返回真则设置了抗锯齿，返回假则没有设置抗锯齿。
 * @since 8
 * @version 1.0
 */
bool OH_Drawing_BrushIsAntiAlias(const OH_Drawing_Brush*);

/**
 * @brief 用于设置画刷的抗锯齿属性，设置为真则画刷在绘制图形时会对图形的边缘像素进行半透明的模糊处理。
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Brush为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Brush 指向画刷对象的指针。
 * @param bool 真为抗锯齿，假则不做抗锯齿处理。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_BrushSetAntiAlias(OH_Drawing_Brush*, bool);

/**
 * @brief 用于获取画刷的颜色属性，颜色属性描述了画刷填充图形时使用的颜色，用一个32位（ARGB）的变量表示。
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Brush为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Brush 指向画刷对象的指针。
 * @return 函数返回一个描述颜色的32位（ARGB）变量。
 * @since 8
 * @version 1.0
 */
uint32_t OH_Drawing_BrushGetColor(const OH_Drawing_Brush*);

/**
 * @brief 用于设置画刷的颜色属性，颜色属性描述了画刷填充图形时使用的颜色，用一个32位（ARGB）的变量表示。
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Brush为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Brush 指向画刷对象的指针。
 * @param color 描述颜色的32位（ARGB）变量。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_BrushSetColor(OH_Drawing_Brush*, uint32_t color);

/**
 * @brief 获取画刷的透明度值。画刷在填充形状时透明通道会使用该值。
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Brush为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Brush 表示指向画刷对象的指针。
 * @return 返回一个8位变量，用于表示透明度值。
 * @since 11
 * @version 1.0
 */
uint8_t OH_Drawing_BrushGetAlpha(const OH_Drawing_Brush*);

/**
 * @brief 为画刷设置透明度值。画刷在填充形状时透明通道会使用该值。
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Brush为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Brush 指向画刷对象的指针。
 * @param alpha 表示要设置的透明度值，是一个8位变量。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_BrushSetAlpha(OH_Drawing_Brush*, uint8_t alpha);

/**
 * @brief 为画刷设置着色器效果。
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Brush为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Brush 指向画刷对象的指针。
 * @param OH_Drawing_ShaderEffect 表示指向着色器对象的指针，为NULL表示清空画刷的着色器效果。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_BrushSetShaderEffect(OH_Drawing_Brush*, OH_Drawing_ShaderEffect*);

/**
 * @brief 为画刷设置阴影层，设置的阴影层效果当前仅在绘制文字时生效。
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Brush为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Brush 指向画刷对象的指针。
 * @param OH_Drawing_ShadowLayer 表示指向阴影层的指针，为NULL表示清空画刷的阴影层效果。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_BrushSetShadowLayer(OH_Drawing_Brush*, OH_Drawing_ShadowLayer*);

/**
 * @brief 为画刷设置滤波器{@link OH_Drawing_Filter}。滤波器是一个容器，可以承载蒙版滤波器和颜色滤波器。
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Brush为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Brush 指向画刷对象的指针。
 * @param OH_Drawing_Filter 表示指向滤波器对象的指针，为NULL表示清空画刷滤波器。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_BrushSetFilter(OH_Drawing_Brush*, OH_Drawing_Filter*);

/**
 * @brief 从画刷获取滤波器{@link OH_Drawing_Filter}。滤波器是一个容器，可以承载蒙版滤波器和颜色滤波器。
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Brush、OH_Drawing_Filter任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Brush 指向画刷对象{@link OH_Drawing_Brush}的指针。
 * @param OH_Drawing_Filter 表示指向滤波器对象{@link OH_Drawing_Filter}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_BrushGetFilter(OH_Drawing_Brush*, OH_Drawing_Filter*);

/**
 * @brief 为画刷设置一个混合器，该混合器实现了指定的混合模式枚举。
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Brush为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER；
 * OH_Drawing_BlendMode不在枚举范围内时返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Brush 指向画刷对象{@link OH_Drawing_Brush}的指针。
 * @param OH_Drawing_BlendMode 混合模式枚举类型{@link OH_Drawing_BlendMode}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_BrushSetBlendMode(OH_Drawing_Brush*, OH_Drawing_BlendMode);

/**
 * @brief 将画刷重置至初始状态，清空所有已设置的属性。
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Brush为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Brush 指向画刷对象{@link OH_Drawing_Brush}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_BrushReset(OH_Drawing_Brush*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif