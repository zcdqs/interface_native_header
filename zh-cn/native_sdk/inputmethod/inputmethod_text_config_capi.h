/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef OHOS_INPUTMETHOD_TEXT_CONFIG_CAPI_H
#define OHOS_INPUTMETHOD_TEXT_CONFIG_CAPI_H
/**
 * @addtogroup InputMethod
 * @{
 *
 * @brief InputMethod模块提供方法来使用输入法和开发输入法。
 *
 * @since 12
 */

/**
 * @file inputmethod_text_config_capi.h
 *
 * @brief 提供输入框配置信息对象的创建、销毁与读写方法。
 *
 * @library libohinputmethod.so
 * @kit IMEKit
 * @syscap SystemCapability.MiscServices.InputMethodFramework
 * @since 12
 * @version 1.0
 */
#include <stdint.h>

#include "inputmethod_cursor_info_capi.h"
#include "inputmethod_text_avoid_info_capi.h"
#include "inputmethod_types_capi.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
/**
 * @brief 输入框配置。
 *
 * 输入框的配置信息。
 *
 * @since 12
 */
typedef struct InputMethod_TextConfig InputMethod_TextConfig;

/**
 * @brief 创建一个新的{@link InputMethod_TextConfig}实例。
 *
 * @return 如果创建成功，返回一个指向新创建的{@link InputMethod_TextConfig}实例的指针。
 * 如果创建失败，对象返回NULL，可能的失败原因有应用地址空间满。
 * @since 12
 */
InputMethod_TextConfig *OH_TextConfig_Create();
/**
 * @brief 销毁一个{@link InputMethod_TextConfig}实例。
 *
 * @param config 表示指向即将被销毁的{@link InputMethod_TextConfig}实例的指针。
 * @since 12
 */
void OH_TextConfig_Destroy(InputMethod_TextConfig *config);

/**
 * @brief 设置{@link InputMethod_TextConfig}实例的输入框类型。
 *
 * @param config 指向即将被设置值的{@link InputMethod_TextConfig}实例的指针。
 * @param inputType 输入框的输入类型。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextConfig_SetInputType(InputMethod_TextConfig *config, InputMethod_TextInputType inputType);
/**
 * @brief 设置{@link InputMethod_TextConfig}实例的回车键功能类型。
 *
 * @param config 指向即将被设置值的{@link InputMethod_TextConfig}实例的指针。
 * @param enterKeyType 回车键功能类型。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextConfig_SetEnterKeyType(
        InputMethod_TextConfig *config, InputMethod_EnterKeyType enterKeyType);
/**
 * @brief 将预上屏支持情况设置到{@link InputMethod_TextConfig}实例。
 *
 * @param config 指向即将被设置值的{@link InputMethod_TextConfig}实例的指针。
 * @param supported 表示输入框是否支持预上屏。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextConfig_SetPreviewTextSupport(InputMethod_TextConfig *config, bool supported);
/**
 * @brief 设置{@link InputMethod_TextConfig}实例的选中文本范围。
 *
 * @param config 指向即将被设置值的{@link InputMethod_TextConfig}实例的指针。
 * @param start 所选文本的起始位置。
 * @param end 所选文本的结束位置。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextConfig_SetSelection(InputMethod_TextConfig *config, int32_t start, int32_t end);
/**
 * @brief 设置{@link InputMethod_TextConfig}实例的所属窗口的窗口id。
 *
 * @param config 指向即将被设置值的{@link InputMethod_TextConfig}实例的指针。
 * @param windowId 绑定输入法的应用所属窗口的窗口id。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextConfig_SetWindowId(InputMethod_TextConfig *config, int32_t windowId);

/**
 * @brief 获取{@link InputMethod_TextConfig}实例的输入框类型。
 *
 * @param config 指向即将被获取值的{@link InputMethod_TextConfig}实例的指针。
 * @param inputType 输入框的输入类型。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextConfig_GetInputType(InputMethod_TextConfig *config, InputMethod_TextInputType *inputType);
/**
 * @brief 获取{@link InputMethod_TextConfig}实例的回车键功能类型。
 *
 * @param config 指向即将被获取值的{@link InputMethod_TextConfig}实例的指针。
 * @param enterKeyType 输入框的回车键功能类型。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextConfig_GetEnterKeyType(
    InputMethod_TextConfig *config, InputMethod_EnterKeyType *enterKeyType);
/**
 * @brief 获取{@link InputMethod_TextConfig}实例的是否支持预上屏。
 *
 * @param config 指向即将被获取值的{@link InputMethod_TextConfig}实例的指针。
 * @param supported 表示输入框是否支持预上屏。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextConfig_IsPreviewTextSupported(InputMethod_TextConfig *config, bool *supported);
/**
 * @brief 获取{@link InputMethod_TextConfig}实例的光标信息。
 *
 * @param config 指向即将被获取值的{@link InputMethod_TextConfig}实例的指针。
 * @param cursorInfo 光标信息。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextConfig_GetCursorInfo(InputMethod_TextConfig *config, InputMethod_CursorInfo **cursorInfo);

/**
 * @brief 获取{@link InputMethod_TextConfig}实例的避让信息。
 *
 * @param config 指向即将被获取值的{@link InputMethod_TextConfig}实例的指针。
 * @param avoidInfo 输入框避让信息。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 *@since 12
 */
InputMethod_ErrorCode OH_TextConfig_GetTextAvoidInfo(
    InputMethod_TextConfig *config, InputMethod_TextAvoidInfo **avoidInfo);

/**
 * @brief 获取{@link InputMethod_TextConfig}实例的选区范围信息。
 *
 * @param config 指向即将被获取值的{@link InputMethod_TextConfig}实例的指针。
 * @param start 所选文本的起始位置。
 * @param end 所选文本的结束位置。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextConfig_GetSelection(InputMethod_TextConfig *config, int32_t *start, int32_t *end);
/**
 * @brief 获取{@link InputMethod_TextConfig}实例所属窗口的窗口id。
 *
 * @param config 指向即将被获取值的{@link InputMethod_TextConfig}实例的指针。
 * @param windowId 绑定输入法的应用所属窗口的窗口id。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextConfig_GetWindowId(InputMethod_TextConfig *config, int32_t *windowId);
#ifdef __cplusplus
}
#endif /* __cplusplus */
/** @} */
#endif // OHOS_INPUTMETHOD_TEXT_CONFIG_CAPI_H