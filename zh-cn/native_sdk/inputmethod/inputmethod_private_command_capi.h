/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef OHOS_INPUTMETHOD_PRIVATE_COMMAND_CAPI_H
#define OHOS_INPUTMETHOD_PRIVATE_COMMAND_CAPI_H
/**
 * @addtogroup InputMethod
 * @{
 *
 * @brief InputMethod模块提供方法来使用输入法和开发输入法。
 *
 * @since 12
 */

/**
 * @file inputmethod_private_command_capi.h
 *
 * @brief 提供私有数据对象的创建、销毁与读写方法。
 *
 * @library libohinputmethod.so
 * @kit IMEKit
 * @syscap SystemCapability.MiscServices.InputMethodFramework
 * @since 12
 * @version 1.0
 */
#include <stddef.h>
#include <stdint.h>

#include "inputmethod_types_capi.h"
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
/**
 * @brief 私有数据。
 *
 * 输入框和输入法应用之间交互的私有数据。
 *
 * @since 12
 */
typedef struct InputMethod_PrivateCommand InputMethod_PrivateCommand;

/**
 * @brief 创建一个新的{@link InputMethod_PrivateCommand}实例。
 *
 * @param key 私有数据的key值。
 * @param keyLength key长度。
 * @return 如果创建成功，返回一个指向新创建的{@link InputMethod_PrivateCommand}实例的指针。
 * 如果创建失败，对象返回NULL，可能的失败原因有应用地址空间满。
 * @since 12
 */
InputMethod_PrivateCommand *OH_PrivateCommand_Create(char key[], size_t keyLength);
/**
 * @brief 销毁一个{@link InputMethod_PrivateCommand}实例。
 *
 * @param command 指向即将被销毁的{@link InputMethod_PrivateCommand}实例的指针。
 * @since 12
 */
void OH_PrivateCommand_Destroy(InputMethod_PrivateCommand *command);
/**
 * @brief 设置{@link InputMethod_PrivateCommand}的key值。
 *
 * @param command 指向即将被设置的{@link InputMethod_PrivateCommand}实例的指针。
 * @param key key值。
 * @param keyLength key长度。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_PrivateCommand_SetKey(InputMethod_PrivateCommand *command, char key[], size_t keyLength);
/**
 * @brief 设置{@link InputMethod_PrivateCommand}的布尔类型value值。
 *
 * @param command 指向即将被设置的{@link InputMethod_PrivateCommand}实例的指针。
 * @param value 布尔类型value值。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_PrivateCommand_SetBoolValue(InputMethod_PrivateCommand *command, bool value);
/**
 * @brief 设置{@link InputMethod_PrivateCommand}的整数类型value值。
 *
 * @param command 指向即将被设置的{@link InputMethod_PrivateCommand}实例的指针。
 * @param value 整型value值。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_PrivateCommand_SetIntValue(InputMethod_PrivateCommand *command, int32_t value);
/**
 * @brief 设置{@link InputMethod_PrivateCommand}的字符串类型value值。
 *
 * @param command 指向即将被设置的{@link InputMethod_PrivateCommand}实例的指针。
 * @param value 字符串类型value值。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_PrivateCommand_SetStrValue(
        InputMethod_PrivateCommand *command, char value[], size_t valueLength);

/**
 * @brief 从{@link InputMethod_PrivateCommand}获取key值。
 *
 * @param command 指向即将被获取key值的{@link InputMethod_PrivateCommand}实例的指针。
 * @param key key的生命周期和command一致。不要直接保存key地址，或者直接写key。建议拷贝后使用。
 * @param keyLength key长度。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_PrivateCommand_GetKey(
    InputMethod_PrivateCommand *command, const char **key, size_t *keyLength);
/**
 * @brief 从{@link InputMethod_PrivateCommand}获取value的数据类型。
 *
 * @param command 指向即将被获取value值的{@link InputMethod_PrivateCommand}实例的指针。
 * @param type value值的数据类型。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_PrivateCommand_GetValueType(
    InputMethod_PrivateCommand *command, InputMethod_CommandValueType *type);
/**
 * @brief 从{@link InputMethod_PrivateCommand}获取布尔类型的value的值。
 *
 * @param command 指向即将被获取value值的{@link InputMethod_PrivateCommand}实例的指针。
 * @param value 布尔类型的value的值。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_PrivateCommand_GetBoolValue(InputMethod_PrivateCommand *command, bool *value);
/**
 * @brief 从{@link InputMethod_PrivateCommand}获取整数类型的value的值。
 *
 * @param command 指向即将被获取value值的{@link InputMethod_PrivateCommand}实例的指针。
 * @param value 整数类型的value的值。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_PrivateCommand_GetIntValue(InputMethod_PrivateCommand *command, int32_t *value);
/**
 * @brief 从{@link InputMethod_PrivateCommand}获取字符串类型的value的值。
 *
 * @param command 指向即将被获取value值的{@link InputMethod_PrivateCommand}实例的指针。
 * @param value 字符串类型的value的值。
 * @param valueLength value的生命周期和command一致。不要直接保存value地址，或者直接写value。建议拷贝后使用。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_PrivateCommand_GetStrValue(
    InputMethod_PrivateCommand *command, const char **value, size_t *valueLength);
#ifdef __cplusplus
}
#endif /* __cplusplus */
/** @} */
#endif // OHOS_INPUTMETHOD_PRIVATE_COMMAND_CAPI_H