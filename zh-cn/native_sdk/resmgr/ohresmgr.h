/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup resourcemanager
 * @{
 *
 * @brief 提供c相关获取资源的接口。
 * @since 12
 */

/**
 * @file ohresmgr.h
 *
 * @brief 提供资源管理native侧获取资源的能力。
 * @syscap SystemCapability.Global.ResourceManager
 * @library libohresmgr.so
 * @since 12
 */
#ifndef GLOBAL_OH_RESMGR_H
#define GLOBAL_OH_RESMGR_H

#include "resmgr_common.h"
#include "../rawfile/raw_file_manager.h"
#include "../arkui/drawable_descriptor.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 获取指定资源ID，指定屏幕密度对应的media资源的Base64码。
 *
 * @param mgr 指向{@link NativeResourceManager}的指针，此指针通过{@link OH_ResourceManager_InitNativeResourceManager}方法获取。
 * @param resId 资源ID。
 * @param density 可选参数screen enddensity {@link screen enddensity}，值为0表示使用当前系统dpi的密度。
 * @param resultValue 写入resultValue的结果。
 * @param resultLen 写入resultLen的media长度。
 * @return {@link SUCCESS} 0 - 成功。
 *         {@link ERROR_CODE_INVALID_INPUT_PARAMETER} 401 - 输入参数无效。可能的原因:1。参数类型不正确;2.参数验证失败。
           {@link ERROR_CODE_RES_ID_NOT_FOUND} 9001001 - 无效的资源ID。
           {@link ERROR_CODE_RES_NOT_FOUND_BY_ID} 9001002 - 没有根据资源ID找到匹配的资源。
           {@link ERROR_CODE_OUT_OF_MEMORY} 9001100 - 内存溢出。
 * @since 12
 */
ResourceManager_ErrorCode OH_ResourceManager_GetMediaBase64(const NativeResourceManager *mgr, uint32_t resId,
    char **resultValue, uint64_t *resultLen, uint32_t density = 0);

/**
 * @brief 获取指定资源名称，指定屏幕密度对应的media资源的Base64码。
 *
 * @param mgr 指向{@link NativeResourceManager}的指针，此指针通过{@link OH_ResourceManager_InitNativeResourceManager}方法获取。
 * @param resName 资源名称。
 * @param density 可选参数screen enddensity {@link screen enddensity}，值为0表示使用当前系统dpi的密度。
 * @param resultValue 写入resultValue的结果。
 * @param resultLen 写入resultLen的media长度。
 * @return {@link SUCCESS} 0 - 成功。
 *         {@link ERROR_CODE_INVALID_INPUT_PARAMETER} 401 - 输入参数无效。可能的原因:1。参数类型不正确;2.参数验证失败。
           {@link ERROR_CODE_RES_NAME_NOT_FOUND} 9001003 - 无效的资源名称。
           {@link ERROR_CODE_RES_NOT_FOUND_BY_NAME} 9001004 - 没有根据资源名称找到匹配的资源。
           {@link ERROR_CODE_OUT_OF_MEMORY} 9001100 - 内存溢出。
 * @since 12
 */
ResourceManager_ErrorCode OH_ResourceManager_GetMediaBase64ByName(const NativeResourceManager *mgr,
    const char *resName, char **resultValue, uint64_t *resultLen, uint32_t density = 0);

/**
 * @brief 获取指定资源ID，指定屏幕密度对应的media资源的内容。
 *
 * @param mgr 指向{@link NativeResourceManager}的指针，此指针通过{@link OH_ResourceManager_InitNativeResourceManager}方法获取。
 * @param resId 资源ID。
 * @param density 可选参数screen enddensity {@link screen enddensity}，值为0表示使用当前系统dpi的密度。
 * @param resultValue 写入resultValue的结果。
 * @param resultLen 写入resultLen的media长度。
 * @return {@link SUCCESS} 0 - 成功。
 *         {@link ERROR_CODE_INVALID_INPUT_PARAMETER} 401 - 输入参数无效。可能的原因:1。参数类型不正确;2.参数验证失败。
           {@link ERROR_CODE_RES_ID_NOT_FOUND} 9001001 - 无效的资源ID。
           {@link ERROR_CODE_RES_NOT_FOUND_BY_ID} 9001002 - 没有根据资源ID找到匹配的资源。
           {@link ERROR_CODE_OUT_OF_MEMORY} 9001100 - 内存溢出。
 * @since 12
 */
ResourceManager_ErrorCode OH_ResourceManager_GetMedia(const NativeResourceManager *mgr, uint32_t resId,
    uint8_t **resultValue, uint64_t *resultLen, uint32_t density = 0);

/**
 * @brief 获取指定资源名称，指定屏幕密度对应的media资源的内容。
 *
 * @param mgr 指向{@link NativeResourceManager}的指针，此指针通过{@link OH_ResourceManager_InitNativeResourceManager}方法获取。
 * @param resName 资源名称。
 * @param density 可选参数screen enddensity {@link screen enddensity}，值为0表示使用当前系统dpi的密度。
 * @param resultValue 写入resultValue的结果。
 * @param resultLen 写入resultLen的media长度。
 * @return {@link SUCCESS} 0 - 成功。
 *         {@link ERROR_CODE_INVALID_INPUT_PARAMETER} 401 - 输入参数无效。可能的原因:1。参数类型不正确;2.参数验证失败。
           {@link ERROR_CODE_RES_NAME_NOT_FOUND} 9001003 - 无效的资源名称。
           {@link ERROR_CODE_RES_NOT_FOUND_BY_NAME} 9001004 - 没有根据资源名称找到匹配的资源。
           {@link ERROR_CODE_OUT_OF_MEMORY} 9001100 - 内存溢出。
 * @since 12
 */
ResourceManager_ErrorCode OH_ResourceManager_GetMediaByName(const NativeResourceManager *mgr, const char *resName,
    uint8_t **resultValue, uint64_t *resultLen, uint32_t density = 0);

/**
 * @brief 获取指定资源Id，指定屏幕密度对应的图标资源的DrawableDescriptor。
 *
 * @param mgr 指向{@link NativeResourceManager}的指针，此指针通过{@link OH_ResourceManager_InitNativeResourceManager}方法获取。
 * @param resId 资源ID。
 * @param density 可选参数screen enddensity {@link screen enddensity}，值为0表示使用当前系统dpi的密度。
 * @param type 选参数表示图标类型，0表示自身图标，1表示主题图标。
 * @param drawableDescriptor 写入drawableDescriptor的结果。
 * @return {@link SUCCESS} 0 - 成功。
 *         {@link ERROR_CODE_INVALID_INPUT_PARAMETER} 401 - 输入参数无效。可能的原因:1。参数类型不正确;2.参数验证失败。
           {@link ERROR_CODE_RES_ID_NOT_FOUND} 9001001 - 无效的资源ID。
           {@link ERROR_CODE_RES_NOT_FOUND_BY_ID} 9001002 - 没有根据资源ID找到匹配的资源。
 * @since 12
 */
ResourceManager_ErrorCode OH_ResourceManager_GetDrawableDescriptor(const NativeResourceManager *mgr,
    uint32_t resId, ArkUI_DrawableDescriptor **drawableDescriptor, uint32_t density = 0, uint32_t type = 0);

/**
 * @brief 获取指定资源名称，指定屏幕密度对应的图标资源的DrawableDescriptor。
 *
 * @param mgr 指向{@link NativeResourceManager}的指针，此指针通过{@link OH_ResourceManager_InitNativeResourceManager}方法获取。
 * @param resName 资源名称。
 * @param density 可选参数screen enddensity {@link screen enddensity}，值为0表示使用当前系统dpi的密度。
 * @param type 可选参数表示图标类型，0表示自身图标，1表示主题图标，2表示动态图标。
 * @param drawableDescriptor 写入drawableDescriptor的结果。
 * @return {@link SUCCESS} 0 - 成功。
 *         {@link ERROR_CODE_INVALID_INPUT_PARAMETER} 401 - 输入参数无效。可能的原因:1。参数类型不正确;2.参数验证失败。
           {@link ERROR_CODE_RES_NAME_NOT_FOUND} 9001003 - 无效的资源名称。
           {@link ERROR_CODE_RES_NOT_FOUND_BY_NAME} 9001004 - 没有根据资源名称找到匹配的资源。
 * @since 12
 */
ResourceManager_ErrorCode OH_ResourceManager_GetDrawableDescriptorByName(const NativeResourceManager *mgr,
    const char *resName, ArkUI_DrawableDescriptor **drawableDescriptor, uint32_t density = 0, uint32_t type = 0);

#ifdef __cplusplus
};
#endif

/** @} */
#endif // GLOBAL_OH_RESMGR_H