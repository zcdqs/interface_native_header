/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Codec
 * @{
 *
 * @brief 定义图像编解码器模块的API。
 *
 * 编解码器模块提供用于图像编解码、设置编解码器参数以及控制和传输图像数据的接口。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file ICodecImage.idl
 *
 * @brief 定义图像编解码器的API。
 *
 * 您可以使用这些API来分配输入缓冲区和解码图像
 *
 * 模块包路径：hos.hdi.codec.image.v1_0
 *
 * 引用：ohos.hdi.codec.image.v1_0.CodecImageType
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.codec.image.v1_0;

import ohos.hdi.codec.image.v1_0.CodecImageType;

/**
 * @brief 图像编解码器模块接口。
 *
 * @since 4.0
 */
interface ICodecImage {

    /**
     * @brief 获得图像编解码器功能。
	 *
	 * 您可以使用此API来获得图像编解码器模块提供的编解码能力集。详见{@link CodecImageCapability}。
     *
     * @param capList 指向获得的图像编解码器能力集{@link CodecImageCapability}。
     *
     * @return 成功返回HDF_SUCCESS
     * @return 失败返回HDF_FAILURE
     *
     * @since 4.0
     */
    GetImageCapability([out] struct CodecImageCapability[] capList);

    /**
     * @brief 图像编解码器模块初始化。
	 *
	 * 您可以使用此API来初始化图像编解码器模块。
     *
     * @param role 指示获取的图像编解码器格式{@link CodecImageRole}。
     *
     * @return 成功返回HDF_SUCCESS
     * @return 成功返回HDF_SUCCESS
     * @return 如果vendor层返回失败，则返回其他值。其他错误代码详见HDF_STATUS的定义。
     *
     * @since 4.0
     */
    Init([in] enum CodecImageRole role);

    /**
     * @brief 图像编解码器模块去初始化。
     *
     * 您可以使用此API对图像编解码器模块进行去初始化。
     *
     * @param role 指示获取的图像编解码器格式{@link CodecImageRole}。
     *
     * @return 成功返回HDF_SUCCESS
     * @return 成功返回HDF_SUCCESS
     * @return 如果vendor层返回失败，则返回其他值。其他错误代码详见HDF_STATUS的定义。
     *
     * @since 4.0
     */
    DeInit([in] enum CodecImageRole role);

    /**
     * @brief 启动jpeg图像解码。
     *
     * 您可以使用此API启动jpeg图像解码。
     *
     * @param inBuffer 获得的jpeg图像解码的输入缓冲区{@link CodecImageBuffer}。
     * @param outBuffer 获得的jpeg图像解码的输出缓冲区{@link CodecImageBuffer}。
     * @param decInfo 获得的jpeg图像解码的解码信息{@link CodecJpegDecInfo}。
     *
     * @return 成功返回HDF_SUCCESS
     * @return 输入无效参数返回HDF_ERR_INVALID_PARAM
     * @return 失败返回HDF_FAILURE
     * @return 如果vendor层返回失败，则返回其他值。其他错误代码详见HDF_STATUS的定义。
     *
     * @since 4.0
     */
    DoJpegDecode([in] struct CodecImageBuffer inBuffer, [in] struct CodecImageBuffer outBuffer,
                 [in] struct CodecJpegDecInfo decInfo);

    /**
     * @brief 分配输入缓冲区。
     *
     * 您可以使用此API为图像编解码器分配输入缓冲区。
     *
     * @param inBuffer 获得的图像编解码器的输入缓冲区{@link CodecImageBuffer}。
     * @param size 获得的输入缓冲区的大小{@link CodecImageBuffer}。
     * @param role 获取的输入缓冲区的图像编解码器格式{@link CodecImageRole}。
     *
     * @return 成功返回HDF_SUCCESS
     * @return 输入无效参数返回HDF_ERR_INVALID_PARAM
     * @return 失败返回HDF_FAILURE
     * @return 如果vendor层返回失败，则返回其他值。其他错误代码详见HDF_STATUS的定义。
     *
     * @since 4.0
     */
    AllocateInBuffer([out] struct CodecImageBuffer inBuffer, [in] unsigned int size, [in] CodecImageRole role);

    /**
     * @brief 释放输入缓冲区。
     *
     * 您可以使用这个API来释放输入缓冲区用于图像解码。
     *
     * @param buffer 获得的图像编解码器的输入缓冲区{@link CodecImageBuffer}。
     *
     * @return 成功返回HDF_SUCCESS
     * @return 成功返回HDF_SUCCESS
     * @return 如果vendor层返回失败，则返回其他值。其他错误代码详见HDF_STATUS的定义。
     *
     * @since 4.0
     */
    FreeInBuffer([in] struct CodecImageBuffer inBuffer);

}
/** @} */