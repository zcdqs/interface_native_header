/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Hostapd
 * @{
 *
 * @brief 定义上层WLAN服务的API接口。
 *
 * 上层WLAN服务开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描/关联WLAN热点，WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管，电源管理等。
 *
 * @since 4.1
 * @version 1.0
 */

 /**
 * @file HostapdTypes.idl
 *
 * @brief 定义与WLAN模块相关的数据类型。
 *
 * WLAN模块数据包括{@code Feature}对象信息、站点(STA)信息等，扫描信息和网络设备信息。
 *
 * 模块包路径：ohos.hdi.wlan.hostapd.v1_0
 *
 * @since 4.1
 * @version 1.0
 */


package ohos.hdi.wlan.hostapd.v1_0;

/**
 * @brief 定义{@code Feature}对象信息。
 *
 * @since 4.1
 * @version 1.0
 */

struct HdiApCbParm {
    /** Wi-Fi Hal回调STA加入和AP状态。 */
    String content;
    /** ap id。 */
    int id;
};
/** @} */