/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Thermal
 * @{
 *
 * @brief 提供设备温度管理、控制及订阅接口。
 *
 * 热模块为热服务提供的设备温度管理、控制及订阅接口。
 * 服务获取此模块的对象或代理后，可以调用相关的接口管理、控制和订阅设备温度。
 *
 * @since 3.1
 * @version 1.1
 */

/**
 * @file IThermalInterface.idl
 *
 * @brief 设备温度管理、控制及订阅接口。
 *
 *
 * 模块包路径：ohos.hdi.thermal.v1_1
 *
 * 引用：
 * - ohos.hdi.thermal.v1_1.ThermalTypes
 * - ohos.hdi.thermal.v1_1.IThermalCallback
 * - ohos.hdi.thermal.v1_1.IFanCallback
 *
 * @since 3.1
 * @version 1.1
 */

package ohos.hdi.thermal.v1_1;

import ohos.hdi.thermal.v1_1.ThermalTypes;
import ohos.hdi.thermal.v1_1.IThermalCallback;
import ohos.hdi.thermal.v1_1.IFanCallback;

/**
 * @brief 备温度管理、控制及订阅接口。
 *
 * 
 *
 * @since 3.1
 */
interface IThermalInterface {
    /**
     * @brief 设置CPU频率。
     *
     * @param freq 输入参数，设置CPU频率的值。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    SetCpuFreq([in] int freq);

    /**
     * @brief 设置GPU频率。
     *
     * @param freq 输入参数，设置GPU频率的值。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    SetGpuFreq([in] int freq);

    /**
     * @brief 设置充电电流。
     *
     * @param current 输入参数，充电电流，单位毫安。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    SetBatteryCurrent([in] int current);

    /**
     * @brief 获取设备发热的信息。
     *
     * @param event 输出参数，设备发热信息，包括器件类型、器件温度。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     * @see HdfThermalCallbackInfo
     *
     * @since 3.1
     */
    GetThermalZoneInfo([out] struct HdfThermalCallbackInfo event);

    /**
     * @brief 隔离内核CPU。
     *
     * @param num 输入参数，CPU内核编号。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 4.0
     */
    IsolateCpu([in] int num);

    /**
     * @brief 注册设备热状态回调。
     *
     * @param callbackObj 输入参数，要注册的回调函数。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @see IThermalCallback
     *
     * @since 3.1
     */
    Register([in] IThermalCallback callbackObj);

    /**
     * @brief 注册设备热状态回调。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    Unregister();

    /**
     * @brief 注册风扇故障检测的回调。
     *
     * @param callbackObj 输入参数，要注册的回调函数。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     * @see IFanCallback
     *
     * @since 4.0
     */
    RegisterFanCallback([in] IFanCallback callbackObj);

    /**
     * @brief 取消注册风扇故障检测的回调。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 4.0
     */
    UnregisterFanCallback();
}
/** @} */
