/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给上层图形服务使用的驱动接口，包括图层管理、设备控制、显示内存管理等相关接口。
 *
 * @since 5.0
 * @version 1.2
 */

 /**
 * @file IDisplayComposer.idl
 *
 * @brief 显示合成接口声明。
 *
 * 模块包路径：ohos.hdi.display.composer.v1_2
 *
 * 引用：
 * - ohos.hdi.display.composer.v1_1.IDisplayComposer
 * - ohos.hdi.display.composer.v1_0.DisplayComposerType
 * - ohos.hdi.display.composer.v1_2.DisplayComposerType
 * - ohos.hdi.display.composer.v1_2.IVBlankIdleCallback
 *
 * @since 5.0
 * @version 1.2
 */
package ohos.hdi.display.composer.v1_2;

import ohos.hdi.display.composer.v1_1.IDisplayComposer;
import ohos.hdi.display.composer.v1_0.DisplayComposerType;
import ohos.hdi.display.composer.v1_2.DisplayComposerType;
import ohos.hdi.display.composer.v1_2.IVBlankIdleCallback;

sequenceable OHOS.HDI.Display.HdifdParcelable;

 /**
 * @brief 显示合成接口声明。
 *
 * 主要提供注册热插拔事件回调、获取显示设备能力集等功能，在v1_1.IDisplayComposer基础上新增注册更改VBlankIdle事件回调、清除缓冲区、硬光标特性等功能，具体方法使用详见函数说明。
 *
 * @since 5.0
 * @version 1.2
 */
interface IDisplayComposer extends ohos.hdi.display.composer.v1_1.IDisplayComposer {
    /**
     * @brief 注册更改VBlankIdle时调用的回调。
     *
     * @param cb 表示用于通知图形服务已准备好更改 VBlankIdle 的实例。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 5.0
     * @version 1.2
     */
     RegDisplayVBlankIdleCallback([in] IVBlankIdleCallback cb);

    /**
     * @brief 表示清除所有客户端缓冲区。
     *
     * @param devId 表示设备ID。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 5.0
     * @version 1.0
     */
     ClearClientBuffer([in] unsigned int devId);

    /**
     * @brief 表示清除所有图层缓冲区。
     *
     * @param devId 表示设备ID。
     * @param layerId 表示图层ID。
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 5.0
     * @version 1.0
     */
     ClearLayerBuffer([in] unsigned int devId, [in] unsigned int layerId);

     /**
     * @brief 设置硬光标位置。
     *
     * @param devId 表示设备ID。
     * @param x 表示硬光标的横坐标。
     * @param y 表示硬光标的纵坐标。
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 5.0
     * @version 1.0
     */
     SetHardwareCursorPosition([in] unsigned int devId, [in] int x, [in] int y);

     /**
     * @brief 启用硬光标。
     *
     * @param devId 表示设备ID。
     * @param enable 表示是否启用硬光标。
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 5.0
     * @version 1.0
     */
     EnableHardwareCursorStats([in] unsigned int devId, [in] boolean enable);

     /**
     * @brief 获取硬光标状态。
     *
     * @param devId 表示设备ID。
     * @param frameCount 表示硬光标帧数。
     * @param vsyncCount 表示硬光标同步计数。
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 5.0
     * @version 1.0
     */
     GetHardwareCursorStats([in] unsigned int devId, [out] unsigned int frameCount, [out] unsigned int vsyncCount);

     /**
     * @brief 设置显示活动区域。
     *
     * @param devId 表示设备ID。
     * @param rect 表示指向客户端缓冲区裁剪区域的指针。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 5.0
     * @version 1.2
     */
     SetDisplayActiveRegion([in] unsigned int devId, [in] struct IRect rect);

     /**
     * @brief 根据设备ID、显示参数以及缓冲区句柄快速显示。
     *
     * @param devId 表示设备ID。
     * @param param 表示当前参数的数据结构。
     * @param inHandles 表示输入缓冲区句柄。
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 5.0
     * @version 1.0
     */
     FastPresent([in] unsigned int devId, [in] struct PresentParam param, [in] NativeBuffer[] inHandles);
}
/** @} */
