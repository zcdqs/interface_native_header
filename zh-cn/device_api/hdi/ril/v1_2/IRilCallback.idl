/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Ril
 * @{
 *
 * @brief Ril模块接口定义。
 *
 * Ril模块为上层电话服务提供相关调用接口，涉及电话、短信、彩信、网络搜索、SIM卡等功能接口及各种回调等。
 *
 * @since 4.1
 * @version 1.2
 */

/**
 * @file IRilCallback.idl
 *
 * @brief Ril模块的回调接口。
 *
 * 模块包路径：ohos.hdi.ril.v1_2
 *
 * 引用：ohos.hdi.ril.v1_1.IRilCallback
 *
 * @since 4.1
 * @version 1.2
 */

package ohos.hdi.ril.v1_2;

import ohos.hdi.ril.v1_1.IRilCallback;

/**
 * @brief Ril模块的回调接口。
 *
 * 回调接口提供打电话、发短彩信、激活SIM卡、上网等回调函数，回调函数由调用者实现。
 *
 * @since 4.1
 * @version 1.2
 */
[callback] interface IRilCallback extends ohos.hdi.ril.v1_1.IRilCallback {
    /**
     * @brief 常驻网络上报。
     *
     * @param responseInfo 表示通用响应信息，例如卡槽ID，请求序列ID等，详见{@link RilRadioResponseInfo}。
     * @param plmn 表示常驻网络
     *
     * @since 4.1
     * @version 1.2
     */
    ResidentNetworkUpdated([in] struct RilRadioResponseInfo responseInfo, [in] String plmn);

    /**
     * @brief 下发随卡信息响应。
     *
     * @param responseInfo 表示通用响应信息，例如卡槽ID，请求序列ID等，详见{@link RilRadioResponseInfo}。
     *
     * @since 4.1
     * @version 1.2
     */
    SendSimMatchedOperatorInfoResponse([in] struct RilRadioResponseInfo responseInfo);

    /**
     * @brief 清除所有数据连接响应。
     *
     * @param responseInfo 表示通用响应信息，例如卡槽ID，请求序列ID等，详见{@link RilRadioResponseInfo}。
     *
     * @since 4.1
     * @version 1.2
     */
    CleanAllConnectionsResponse([in] struct RilRadioResponseInfo responseInfo);
}    
/** @} */