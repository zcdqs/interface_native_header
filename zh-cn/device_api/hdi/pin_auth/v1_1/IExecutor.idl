/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfPinAuth
 * @{
 *
 * @brief 提供口令认证驱动的标准API接口。
 *
 * 口令认证驱动为口令认证服务提供统一的访问接口。获取口令认证驱动代理后，口令认证服务可以调用相关接口获取执行器，获取口令认证执行器后，
 * 口令认证服务可以调用相关接口获取执行器信息，获取凭据模版信息，注册口令，认证口令，删除口令等。
 *
 * @since 4.0
 */

/**
 * @file IExecutor.idl
 *
 * @brief 定义执行器标准API接口。接口可用于获取执行器信息，获取凭据模版信息，注册口令，认证口令，删除口令等。
 *
 * 模块包路径：ohos.hdi.pin_auth.v1_1
 *
 * 引用：
 * - ohos.hdi.pin_auth.v1_0.IExecutor
 * - ohos.hdi.pin_auth.v1_1.PinAuthTypes
 * - ohos.hdi.pin_auth.v1_1.IExecutorCallback
 *
 * @since 4.0
 */

package ohos.hdi.pin_auth.v1_1;

import ohos.hdi.pin_auth.v1_0.IExecutor;
import ohos.hdi.pin_auth.v1_1.PinAuthTypes;
import ohos.hdi.pin_auth.v1_1.IExecutorCallback;

/**
 * @brief 定义执行器标准API接口。接口可用于获取执行器信息，获取凭据模版信息，注册口令，认证口令，删除口令等。
 *
 * @since 4.0
 * @version 1.1
 */

interface IExecutor extends ohos.hdi.pin_auth.v1_0.IExecutor {
    /**
     * @brief 获取属性。
     *
     * @param templateIdList 标识需要处理的模板。
     * @param propertyTypes 标识需要获取的属性类型{@link GetPropertyType}。
     * @param property 标识获取的属性{@link Property}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 4.0
     * @version 1.1
     */
    GetProperty([in] unsigned long[] templateIdList, [in] enum GetPropertyType[] propertyTypes, [out] struct Property property);
    /**
     * @brief 注册口令。
     *
     * @param scheduleId 调度ID，用于标识一次操作请求的调度过程。
     * @param extraInfo 其他相关信息，用于支持信息扩展。
     * @param callbackObj 回调对象{@link IExecutorCallback}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 4.0
     * @version 1.1
     */
    EnrollV1_1([in] unsigned long scheduleId, [in] unsigned char[] extraInfo, [in] IExecutorCallback callbackObj);
    /**
     * @brief 认证口令。
     *
     * @param scheduleId 调度ID，用于标识一次操作请求的调度过程。
     * @param templateId 指定要认证的模版ID。
     * @param extraInfo 其他相关信息，用于支持信息扩展。
     * @param callbackObj 回调对象{@link IExecutorCallback}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 4.0
     * @version 1.1
     */
    AuthenticateV1_1([in] unsigned long scheduleId, [in] unsigned long templateId, [in] unsigned char[] extraInfo, [in] IExecutorCallback callbackObj);
}
/** @} */